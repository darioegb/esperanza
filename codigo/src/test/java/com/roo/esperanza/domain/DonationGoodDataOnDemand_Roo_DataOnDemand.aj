// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.roo.esperanza.domain;

import com.roo.esperanza.domain.DonationGood;
import com.roo.esperanza.domain.DonationGoodDataOnDemand;
import com.roo.esperanza.domain.Participant;
import com.roo.esperanza.domain.PublicationHistoryDataOnDemand;
import com.roo.esperanza.reference.DonationTypeEnum;
import com.roo.esperanza.reference.StatusTypeEnum;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

privileged aspect DonationGoodDataOnDemand_Roo_DataOnDemand {
    
    declare @type: DonationGoodDataOnDemand: @Component;
    
    private Random DonationGoodDataOnDemand.rnd = new SecureRandom();
    
    private List<DonationGood> DonationGoodDataOnDemand.data;
    
    @Autowired
    PublicationHistoryDataOnDemand DonationGoodDataOnDemand.publicationHistoryDataOnDemand;
    
    public DonationGood DonationGoodDataOnDemand.getNewTransientDonationGood(int index) {
        DonationGood obj = new DonationGood();
        setCreator(obj, index);
        setDate(obj, index);
        setDescription(obj, index);
        setDetailedDescription(obj, index);
        setDonationType(obj, index);
        setLink(obj, index);
        setPhoto(obj, index);
        setStatusType(obj, index);
        return obj;
    }
    
    public void DonationGoodDataOnDemand.setCreator(DonationGood obj, int index) {
        Participant creator = null;
        obj.setCreator(creator);
    }
    
    public void DonationGoodDataOnDemand.setDate(DonationGood obj, int index) {
        Date date = new GregorianCalendar(Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH), Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE), Calendar.getInstance().get(Calendar.SECOND) + new Double(Math.random() * 1000).intValue()).getTime();
        obj.setDate(date);
    }
    
    public void DonationGoodDataOnDemand.setDescription(DonationGood obj, int index) {
        String description = "description_" + index;
        if (description.length() > 100) {
            description = description.substring(0, 100);
        }
        obj.setDescription(description);
    }
    
    public void DonationGoodDataOnDemand.setDetailedDescription(DonationGood obj, int index) {
        String detailedDescription = "detailedDescriptionxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_" + index;
        if (detailedDescription.length() > 300) {
            detailedDescription = detailedDescription.substring(0, 300);
        }
        obj.setDetailedDescription(detailedDescription);
    }
    
    public void DonationGoodDataOnDemand.setDonationType(DonationGood obj, int index) {
        DonationTypeEnum donationType = DonationTypeEnum.class.getEnumConstants()[0];
        obj.setDonationType(donationType);
    }
    
    public void DonationGoodDataOnDemand.setLink(DonationGood obj, int index) {
        String link = "linkxxxxxxxxxxxxxx_" + index;
        if (link.length() > 250) {
            link = link.substring(0, 250);
        }
        obj.setLink(link);
    }
    
    public void DonationGoodDataOnDemand.setPhoto(DonationGood obj, int index) {
        String photo = "photo_" + index;
        obj.setPhoto(photo);
    }
    
    public void DonationGoodDataOnDemand.setStatusType(DonationGood obj, int index) {
        StatusTypeEnum statusType = StatusTypeEnum.class.getEnumConstants()[0];
        obj.setStatusType(statusType);
    }
    
    public DonationGood DonationGoodDataOnDemand.getSpecificDonationGood(int index) {
        init();
        if (index < 0) {
            index = 0;
        }
        if (index > (data.size() - 1)) {
            index = data.size() - 1;
        }
        DonationGood obj = data.get(index);
        Long id = obj.getId();
        return DonationGood.findDonationGood(id);
    }
    
    public DonationGood DonationGoodDataOnDemand.getRandomDonationGood() {
        init();
        DonationGood obj = data.get(rnd.nextInt(data.size()));
        Long id = obj.getId();
        return DonationGood.findDonationGood(id);
    }
    
    public boolean DonationGoodDataOnDemand.modifyDonationGood(DonationGood obj) {
        return false;
    }
    
    public void DonationGoodDataOnDemand.init() {
        int from = 0;
        int to = 10;
        data = DonationGood.findDonationGoodEntries(from, to);
        if (data == null) {
            throw new IllegalStateException("Find entries implementation for 'DonationGood' illegally returned null");
        }
        if (!data.isEmpty()) {
            return;
        }
        
        data = new ArrayList<DonationGood>();
        for (int i = 0; i < 10; i++) {
            DonationGood obj = getNewTransientDonationGood(i);
            try {
                obj.persist();
            } catch (final ConstraintViolationException e) {
                final StringBuilder msg = new StringBuilder();
                for (Iterator<ConstraintViolation<?>> iter = e.getConstraintViolations().iterator(); iter.hasNext();) {
                    final ConstraintViolation<?> cv = iter.next();
                    msg.append("[").append(cv.getRootBean().getClass().getName()).append(".").append(cv.getPropertyPath()).append(": ").append(cv.getMessage()).append(" (invalid value = ").append(cv.getInvalidValue()).append(")").append("]");
                }
                throw new IllegalStateException(msg.toString(), e);
            }
            obj.flush();
            data.add(obj);
        }
    }
    
}
