// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.roo.esperanza.domain;

import com.roo.esperanza.domain.DonationRequest;
import com.roo.esperanza.domain.DonationRequestDataOnDemand;
import com.roo.esperanza.domain.DonationRequestIntegrationTest;
import java.util.Iterator;
import java.util.List;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

privileged aspect DonationRequestIntegrationTest_Roo_IntegrationTest {
    
    declare @type: DonationRequestIntegrationTest: @RunWith(SpringJUnit4ClassRunner.class);
    
    declare @type: DonationRequestIntegrationTest: @ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml");
    
    declare @type: DonationRequestIntegrationTest: @Transactional;
    
    @Autowired
    DonationRequestDataOnDemand DonationRequestIntegrationTest.dod;
    
    @Test
    public void DonationRequestIntegrationTest.testCountDonationRequests() {
        Assert.assertNotNull("Data on demand for 'DonationRequest' failed to initialize correctly", dod.getRandomDonationRequest());
        long count = DonationRequest.countDonationRequests();
        Assert.assertTrue("Counter for 'DonationRequest' incorrectly reported there were no entries", count > 0);
    }
    
    @Test
    public void DonationRequestIntegrationTest.testFindDonationRequest() {
        DonationRequest obj = dod.getRandomDonationRequest();
        Assert.assertNotNull("Data on demand for 'DonationRequest' failed to initialize correctly", obj);
        Long id = obj.getId();
        Assert.assertNotNull("Data on demand for 'DonationRequest' failed to provide an identifier", id);
        obj = DonationRequest.findDonationRequest(id);
        Assert.assertNotNull("Find method for 'DonationRequest' illegally returned null for id '" + id + "'", obj);
        Assert.assertEquals("Find method for 'DonationRequest' returned the incorrect identifier", id, obj.getId());
    }
    
    @Test
    public void DonationRequestIntegrationTest.testFindAllDonationRequests() {
        Assert.assertNotNull("Data on demand for 'DonationRequest' failed to initialize correctly", dod.getRandomDonationRequest());
        long count = DonationRequest.countDonationRequests();
        Assert.assertTrue("Too expensive to perform a find all test for 'DonationRequest', as there are " + count + " entries; set the findAllMaximum to exceed this value or set findAll=false on the integration test annotation to disable the test", count < 250);
        List<DonationRequest> result = DonationRequest.findAllDonationRequests();
        Assert.assertNotNull("Find all method for 'DonationRequest' illegally returned null", result);
        Assert.assertTrue("Find all method for 'DonationRequest' failed to return any data", result.size() > 0);
    }
    
    @Test
    public void DonationRequestIntegrationTest.testFindDonationRequestEntries() {
        Assert.assertNotNull("Data on demand for 'DonationRequest' failed to initialize correctly", dod.getRandomDonationRequest());
        long count = DonationRequest.countDonationRequests();
        if (count > 20) count = 20;
        int firstResult = 0;
        int maxResults = (int) count;
        List<DonationRequest> result = DonationRequest.findDonationRequestEntries(firstResult, maxResults);
        Assert.assertNotNull("Find entries method for 'DonationRequest' illegally returned null", result);
        Assert.assertEquals("Find entries method for 'DonationRequest' returned an incorrect number of entries", count, result.size());
    }
    
    @Test
    public void DonationRequestIntegrationTest.testFlush() {
        DonationRequest obj = dod.getRandomDonationRequest();
        Assert.assertNotNull("Data on demand for 'DonationRequest' failed to initialize correctly", obj);
        Long id = obj.getId();
        Assert.assertNotNull("Data on demand for 'DonationRequest' failed to provide an identifier", id);
        obj = DonationRequest.findDonationRequest(id);
        Assert.assertNotNull("Find method for 'DonationRequest' illegally returned null for id '" + id + "'", obj);
        boolean modified =  dod.modifyDonationRequest(obj);
        Integer currentVersion = obj.getVersion();
        obj.flush();
        Assert.assertTrue("Version for 'DonationRequest' failed to increment on flush directive", (currentVersion != null && obj.getVersion() > currentVersion) || !modified);
    }
    
    @Test
    public void DonationRequestIntegrationTest.testMergeUpdate() {
        DonationRequest obj = dod.getRandomDonationRequest();
        Assert.assertNotNull("Data on demand for 'DonationRequest' failed to initialize correctly", obj);
        Long id = obj.getId();
        Assert.assertNotNull("Data on demand for 'DonationRequest' failed to provide an identifier", id);
        obj = DonationRequest.findDonationRequest(id);
        boolean modified =  dod.modifyDonationRequest(obj);
        Integer currentVersion = obj.getVersion();
        DonationRequest merged = obj.merge();
        obj.flush();
        Assert.assertEquals("Identifier of merged object not the same as identifier of original object", merged.getId(), id);
        Assert.assertTrue("Version for 'DonationRequest' failed to increment on merge and flush directive", (currentVersion != null && obj.getVersion() > currentVersion) || !modified);
    }
    
    @Test
    public void DonationRequestIntegrationTest.testPersist() {
        Assert.assertNotNull("Data on demand for 'DonationRequest' failed to initialize correctly", dod.getRandomDonationRequest());
        DonationRequest obj = dod.getNewTransientDonationRequest(Integer.MAX_VALUE);
        Assert.assertNotNull("Data on demand for 'DonationRequest' failed to provide a new transient entity", obj);
        Assert.assertNull("Expected 'DonationRequest' identifier to be null", obj.getId());
        try {
            obj.persist();
        } catch (final ConstraintViolationException e) {
            final StringBuilder msg = new StringBuilder();
            for (Iterator<ConstraintViolation<?>> iter = e.getConstraintViolations().iterator(); iter.hasNext();) {
                final ConstraintViolation<?> cv = iter.next();
                msg.append("[").append(cv.getRootBean().getClass().getName()).append(".").append(cv.getPropertyPath()).append(": ").append(cv.getMessage()).append(" (invalid value = ").append(cv.getInvalidValue()).append(")").append("]");
            }
            throw new IllegalStateException(msg.toString(), e);
        }
        obj.flush();
        Assert.assertNotNull("Expected 'DonationRequest' identifier to no longer be null", obj.getId());
    }
    
    @Test
    public void DonationRequestIntegrationTest.testRemove() {
        DonationRequest obj = dod.getRandomDonationRequest();
        Assert.assertNotNull("Data on demand for 'DonationRequest' failed to initialize correctly", obj);
        Long id = obj.getId();
        Assert.assertNotNull("Data on demand for 'DonationRequest' failed to provide an identifier", id);
        obj = DonationRequest.findDonationRequest(id);
        obj.remove();
        obj.flush();
        Assert.assertNull("Failed to remove 'DonationRequest' with identifier '" + id + "'", DonationRequest.findDonationRequest(id));
    }
    
}
