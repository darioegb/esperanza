// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.roo.esperanza.domain;

import com.roo.esperanza.domain.ConfigNotification;
import com.roo.esperanza.domain.ConfigNotificationDataOnDemand;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.springframework.stereotype.Component;

privileged aspect ConfigNotificationDataOnDemand_Roo_DataOnDemand {
    
    declare @type: ConfigNotificationDataOnDemand: @Component;
    
    private Random ConfigNotificationDataOnDemand.rnd = new SecureRandom();
    
    private List<ConfigNotification> ConfigNotificationDataOnDemand.data;
    
    public ConfigNotification ConfigNotificationDataOnDemand.getNewTransientConfigNotification(int index) {
        ConfigNotification obj = new ConfigNotification();
        setClaimDonation(obj, index);
        setComplaintReceived(obj, index);
        setDenouncements(obj, index);
        setDonationGoods(obj, index);
        setDonationReceived(obj, index);
        setDonationRequest(obj, index);
        setNeeds(obj, index);
        setPublicactionStatus(obj, index);
        setPublications(obj, index);
        setReceiveMails(obj, index);
        setStateComplaintFiled(obj, index);
        setStatistics(obj, index);
        setSuscriptions(obj, index);
        setThanksToDonation(obj, index);
        return obj;
    }
    
    public void ConfigNotificationDataOnDemand.setClaimDonation(ConfigNotification obj, int index) {
        Boolean claimDonation = Boolean.TRUE;
        obj.setClaimDonation(claimDonation);
    }
    
    public void ConfigNotificationDataOnDemand.setComplaintReceived(ConfigNotification obj, int index) {
        Boolean complaintReceived = Boolean.TRUE;
        obj.setComplaintReceived(complaintReceived);
    }
    
    public void ConfigNotificationDataOnDemand.setDenouncements(ConfigNotification obj, int index) {
        Boolean denouncements = Boolean.TRUE;
        obj.setDenouncements(denouncements);
    }
    
    public void ConfigNotificationDataOnDemand.setDonationGoods(ConfigNotification obj, int index) {
        Boolean donationGoods = Boolean.TRUE;
        obj.setDonationGoods(donationGoods);
    }
    
    public void ConfigNotificationDataOnDemand.setDonationReceived(ConfigNotification obj, int index) {
        Boolean donationReceived = Boolean.TRUE;
        obj.setDonationReceived(donationReceived);
    }
    
    public void ConfigNotificationDataOnDemand.setDonationRequest(ConfigNotification obj, int index) {
        Boolean donationRequest = Boolean.TRUE;
        obj.setDonationRequest(donationRequest);
    }
    
    public void ConfigNotificationDataOnDemand.setNeeds(ConfigNotification obj, int index) {
        Boolean needs = Boolean.TRUE;
        obj.setNeeds(needs);
    }
    
    public void ConfigNotificationDataOnDemand.setPublicactionStatus(ConfigNotification obj, int index) {
        Boolean publicactionStatus = Boolean.TRUE;
        obj.setPublicactionStatus(publicactionStatus);
    }
    
    public void ConfigNotificationDataOnDemand.setPublications(ConfigNotification obj, int index) {
        Boolean publications = Boolean.TRUE;
        obj.setPublications(publications);
    }
    
    public void ConfigNotificationDataOnDemand.setReceiveMails(ConfigNotification obj, int index) {
        Boolean receiveMails = Boolean.TRUE;
        obj.setReceiveMails(receiveMails);
    }
    
    public void ConfigNotificationDataOnDemand.setStateComplaintFiled(ConfigNotification obj, int index) {
        Boolean stateComplaintFiled = Boolean.TRUE;
        obj.setStateComplaintFiled(stateComplaintFiled);
    }
    
    public void ConfigNotificationDataOnDemand.setStatistics(ConfigNotification obj, int index) {
        Boolean statistics = Boolean.TRUE;
        obj.setStatistics(statistics);
    }
    
    public void ConfigNotificationDataOnDemand.setSuscriptions(ConfigNotification obj, int index) {
        Boolean suscriptions = Boolean.TRUE;
        obj.setSuscriptions(suscriptions);
    }
    
    public void ConfigNotificationDataOnDemand.setThanksToDonation(ConfigNotification obj, int index) {
        Boolean thanksToDonation = Boolean.TRUE;
        obj.setThanksToDonation(thanksToDonation);
    }
    
    public ConfigNotification ConfigNotificationDataOnDemand.getSpecificConfigNotification(int index) {
        init();
        if (index < 0) {
            index = 0;
        }
        if (index > (data.size() - 1)) {
            index = data.size() - 1;
        }
        ConfigNotification obj = data.get(index);
        Long id = obj.getId();
        return ConfigNotification.findConfigNotification(id);
    }
    
    public ConfigNotification ConfigNotificationDataOnDemand.getRandomConfigNotification() {
        init();
        ConfigNotification obj = data.get(rnd.nextInt(data.size()));
        Long id = obj.getId();
        return ConfigNotification.findConfigNotification(id);
    }
    
    public boolean ConfigNotificationDataOnDemand.modifyConfigNotification(ConfigNotification obj) {
        return false;
    }
    
    public void ConfigNotificationDataOnDemand.init() {
        int from = 0;
        int to = 10;
        data = ConfigNotification.findConfigNotificationEntries(from, to);
        if (data == null) {
            throw new IllegalStateException("Find entries implementation for 'ConfigNotification' illegally returned null");
        }
        if (!data.isEmpty()) {
            return;
        }
        
        data = new ArrayList<ConfigNotification>();
        for (int i = 0; i < 10; i++) {
            ConfigNotification obj = getNewTransientConfigNotification(i);
            try {
                obj.persist();
            } catch (final ConstraintViolationException e) {
                final StringBuilder msg = new StringBuilder();
                for (Iterator<ConstraintViolation<?>> iter = e.getConstraintViolations().iterator(); iter.hasNext();) {
                    final ConstraintViolation<?> cv = iter.next();
                    msg.append("[").append(cv.getRootBean().getClass().getName()).append(".").append(cv.getPropertyPath()).append(": ").append(cv.getMessage()).append(" (invalid value = ").append(cv.getInvalidValue()).append(")").append("]");
                }
                throw new IllegalStateException(msg.toString(), e);
            }
            obj.flush();
            data.add(obj);
        }
    }
    
}
