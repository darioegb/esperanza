// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.roo.esperanza.domain;

import com.roo.esperanza.domain.PublicationHistory;
import com.roo.esperanza.domain.PublicationHistoryDataOnDemand;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.springframework.stereotype.Component;

privileged aspect PublicationHistoryDataOnDemand_Roo_DataOnDemand {
    
    declare @type: PublicationHistoryDataOnDemand: @Component;
    
    private Random PublicationHistoryDataOnDemand.rnd = new SecureRandom();
    
    private List<PublicationHistory> PublicationHistoryDataOnDemand.data;
    
    public PublicationHistory PublicationHistoryDataOnDemand.getNewTransientPublicationHistory(int index) {
        PublicationHistory obj = new PublicationHistory();
        setAdminName(obj, index);
        return obj;
    }
    
    public void PublicationHistoryDataOnDemand.setAdminName(PublicationHistory obj, int index) {
        String adminName = "adminName_" + index;
        if (adminName.length() > 60) {
            adminName = adminName.substring(0, 60);
        }
        obj.setAdminName(adminName);
    }
    
    public PublicationHistory PublicationHistoryDataOnDemand.getSpecificPublicationHistory(int index) {
        init();
        if (index < 0) {
            index = 0;
        }
        if (index > (data.size() - 1)) {
            index = data.size() - 1;
        }
        PublicationHistory obj = data.get(index);
        Long id = obj.getId();
        return PublicationHistory.findPublicationHistory(id);
    }
    
    public PublicationHistory PublicationHistoryDataOnDemand.getRandomPublicationHistory() {
        init();
        PublicationHistory obj = data.get(rnd.nextInt(data.size()));
        Long id = obj.getId();
        return PublicationHistory.findPublicationHistory(id);
    }
    
    public boolean PublicationHistoryDataOnDemand.modifyPublicationHistory(PublicationHistory obj) {
        return false;
    }
    
    public void PublicationHistoryDataOnDemand.init() {
        int from = 0;
        int to = 10;
        data = PublicationHistory.findPublicationHistoryEntries(from, to);
        if (data == null) {
            throw new IllegalStateException("Find entries implementation for 'PublicationHistory' illegally returned null");
        }
        if (!data.isEmpty()) {
            return;
        }
        
        data = new ArrayList<PublicationHistory>();
        for (int i = 0; i < 10; i++) {
            PublicationHistory obj = getNewTransientPublicationHistory(i);
            try {
                obj.persist();
            } catch (final ConstraintViolationException e) {
                final StringBuilder msg = new StringBuilder();
                for (Iterator<ConstraintViolation<?>> iter = e.getConstraintViolations().iterator(); iter.hasNext();) {
                    final ConstraintViolation<?> cv = iter.next();
                    msg.append("[").append(cv.getRootBean().getClass().getName()).append(".").append(cv.getPropertyPath()).append(": ").append(cv.getMessage()).append(" (invalid value = ").append(cv.getInvalidValue()).append(")").append("]");
                }
                throw new IllegalStateException(msg.toString(), e);
            }
            obj.flush();
            data.add(obj);
        }
    }
    
}
