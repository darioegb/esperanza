// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.roo.esperanza.domain;

import com.roo.esperanza.domain.NecessityDataOnDemand;
import org.springframework.beans.factory.annotation.Configurable;

privileged aspect NecessityDataOnDemand_Roo_Configurable {
    
    declare @type: NecessityDataOnDemand: @Configurable;
    
}
