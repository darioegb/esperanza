package com.roo.esperanza.web;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Statistics extends CommonFunctions {
	
	@BeforeClass public static void openbrowser() {
		initBrowser();
		driver.get("http://localhost:8080/Esperanza/login?lang=es_ES");
	 }
	
	@AfterClass 
	public static void closebrowser() {
		closeBrowser(); 
	 }
	
	@Test
	public void statisticsByInstitutions() throws InterruptedException{
		logIn("darioegb@gmail.com", "123123");
		driver.findElement(By.cssSelector("a[href*='find=ByDateBetweenOrUserRoleType&form']")).click();
		Select listbox = new Select(driver.findElement(By.xpath("//select[@name='roleType']"))); 
		listbox.selectByIndex(2);
		driver.findElement(By.xpath("//input[@id = 'proceed']")).click();
		WebDriverWait wait = new WebDriverWait(driver, 5); 
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@name = 'chartButton']")));
		driver.findElement(By.xpath("//button[@name = 'chartButton']")).click();
		Thread.sleep(5000);
		logOut();
	}

}
