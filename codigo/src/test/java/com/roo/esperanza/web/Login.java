/**
 * 
 */
/**
 * @author admin
 *
 */
package com.roo.esperanza.web;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class Login extends CommonFunctions{
	
	@BeforeClass public static void openbrowser() {
		initBrowser();
		driver.get("http://localhost:8080/Esperanza/login?lang=es_ES");
	 }
	
	@AfterClass 
	public static void closebrowser() {
		closeBrowser(); 
	 }

	@Test
	public void LoginAndLogout(){
		logIn("darioegb@gmail.com", "123");
		logOut();
	}
	
	@Test
	public void LoginAndLogoutFail(){
		logIn("darioegb@gmail.com", "123456");
		logOut();
	}
}