package com.roo.esperanza.web;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public abstract class CommonFunctions {
	public static WebDriver driver=null;
	public static boolean browserAlreadyOpen=false;
	public boolean isAlreadyLogIn=false;
	public static void initBrowser(){
		//Check If browser Is already opened during previous test execution. 
		if(!browserAlreadyOpen)	driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		//At last browserAlreadyOpen flag will becomes true to not open new browser when start executing next test. browserAlreadyOpen=true;
		browserAlreadyOpen=true;
	} 
	//To Close Browser 
	public static void closeBrowser(){
		driver.quit();
		browserAlreadyOpen=false;
	}
	
	//Can accept userID and password as a string 
	public void logIn(String userID, String password){
		//To check If already login previously then don't execute this function. 
		if(!isAlreadyLogIn){
			//If Not login then login In to your account. 
			driver.findElement(By.xpath("//input[@name='j_username']")).sendKeys(userID); 
			driver.findElement(By.xpath("//input[@name='j_password']")).sendKeys(password);
			driver.findElement(By.xpath("//button[@id = 'proceed']")).click(); 
			isAlreadyLogIn=true; 
		} 
	} 
	public void logOut(){
		driver.findElement(By.xpath("//li[@id='c_userconfig']")).click(); 
		driver.findElement(By.xpath("//li[@id='i_logout']")).click(); 
		isAlreadyLogIn=false; 
	}
}