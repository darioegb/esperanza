package com.roo.esperanza.reference;

public enum DonationTypeEnum {

    Food, CroopsPlants, Materials, Money, Electronics, Other, Hours
}
