package com.roo.esperanza.reference;

public enum DonationStatusTypeEnum {
    Opened, Accept, Cancel, Received, NotReceived
}
