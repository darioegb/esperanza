package com.roo.esperanza.reference;

public enum ScoreTypeEnum {
    Bad, Regular, Medium, Good, Excellent
}
