package com.roo.esperanza.reference;

public enum RankingTypeEnum {
    Weekly, Monthly, Biannual, Annual
}
