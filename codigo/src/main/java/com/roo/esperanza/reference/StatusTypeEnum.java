package com.roo.esperanza.reference;

public enum StatusTypeEnum {
    Create, Accept, Cancel, Block, Finish
}
