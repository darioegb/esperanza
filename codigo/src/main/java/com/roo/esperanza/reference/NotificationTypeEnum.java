package com.roo.esperanza.reference;

public enum NotificationTypeEnum {
	Needs, DonationGoods, ReceiveMails, DonationReceived, Statistics, PublicactionStatus, 
    StateComplaintFiled, ComplaintReceived, Suscriptions, DonationRequest, ThanksToDonation, ClaimDonation, 
    Publications, Denouncements   
}
