package com.roo.esperanza.reference;

public enum RoleTypeEnum {
	ROLE_ADMIN, ROLE_PERSON, ROLE_INSTITUTION
}
