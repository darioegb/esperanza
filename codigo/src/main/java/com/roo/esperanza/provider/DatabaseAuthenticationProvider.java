/**
 * 
 */
package com.roo.esperanza.provider;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityNotFoundException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.authentication.encoding.MessageDigestPasswordEncoder;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.roo.esperanza.domain.User;

// TODO: Auto-generated Javadoc
/**
 * The Class DatabaseAuthenticationProvider.
 *
 * @author rohit
 */
@Service("databaseAuthenticationProvider")
public class DatabaseAuthenticationProvider extends
		AbstractUserDetailsAuthenticationProvider {
	
	/** The logger. */
	private final Logger logger = Logger.getLogger(getClass());	
	
	/** The message digest password encoder. */
	@Autowired
	private MessageDigestPasswordEncoder messageDigestPasswordEncoder;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.security.authentication.dao.
	 * AbstractUserDetailsAuthenticationProvider
	 * #additionalAuthenticationChecks(org
	 * .springframework.security.core.userdetails.UserDetails,
	 * org.springframework
	 * .security.authentication.UsernamePasswordAuthenticationToken)
	 */
	@Override
	protected void additionalAuthenticationChecks(UserDetails arg0,
			UsernamePasswordAuthenticationToken arg1)
			throws AuthenticationException {
		return;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.security.authentication.dao.
	 * AbstractUserDetailsAuthenticationProvider#retrieveUser(java.lang.String,
	 * org
	 * .springframework.security.authentication.UsernamePasswordAuthenticationToken
	 * )
	 */
	@Override
	protected UserDetails retrieveUser(String username,
		      UsernamePasswordAuthenticationToken authentication)
			throws AuthenticationException {
		logger.debug("Inside retrieveUser");
		String password = (String) authentication.getCredentials();
	    if (! StringUtils.hasText(password)) {
	    	throw new BadCredentialsException("Por favor ingrese su contraseña");
	    }
	    String encryptedPassword = messageDigestPasswordEncoder.encodePassword(password, null); 
	    String expectedPassword = null;
	    List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
	    TypedQuery<User> query= User.findUsersByEmailAddress(username);
		try {
	        User targetUser = (User) query.getSingleResult();
		    expectedPassword = targetUser.getPassword();
		    if (! StringUtils.hasText(expectedPassword)) {
		    	throw new BadCredentialsException("Contraseña invalida");
		    }
		    if (! encryptedPassword.equals(expectedPassword)) {
		    	throw new BadCredentialsException("No hay clave para " + username + " seteada en la base de datos, contactactese con el administrator");
		    }
		    authorities.add(new SimpleGrantedAuthority(targetUser.getRol().getRoleType().name()));
		  } catch (EmptyResultDataAccessException e) {
		        throw new BadCredentialsException("Usuario invalido");
		  } catch (EntityNotFoundException e) {
		        throw new BadCredentialsException("Usuario invalido");
		  } catch (NonUniqueResultException e) {
		        throw new BadCredentialsException("Usuario no unico, contactese con el administrador");
		}
	    return new org.springframework.security.core.userdetails.User(
	      username,
	      password,
	      true, // enabled 
	      true, // account not expired
	      true, // credentials not expired 
	      true, // account not locked
	      authorities
	    );
	}
}
