// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.roo.esperanza.provider;

import com.roo.esperanza.domain.Address;
import com.roo.esperanza.domain.AdminAction;
import com.roo.esperanza.domain.Category;
import com.roo.esperanza.domain.Conversation;
import com.roo.esperanza.domain.Country;
import com.roo.esperanza.domain.Denouncement;
import com.roo.esperanza.domain.Donation;
import com.roo.esperanza.domain.DonationGood;
import com.roo.esperanza.domain.DonationRequest;
import com.roo.esperanza.domain.Institution;
import com.roo.esperanza.domain.Message;
import com.roo.esperanza.domain.Multimedia;
import com.roo.esperanza.domain.Necessity;
import com.roo.esperanza.domain.Notification;
import com.roo.esperanza.domain.Participant;
import com.roo.esperanza.domain.Person;
import com.roo.esperanza.domain.Province;
import com.roo.esperanza.domain.PublicationHistory;
import com.roo.esperanza.domain.Puntuation;
import com.roo.esperanza.domain.Resource;
import com.roo.esperanza.domain.Role;
import com.roo.esperanza.domain.Suscription;
import com.roo.esperanza.domain.User;
import com.roo.esperanza.domain.UserRole;
import com.roo.esperanza.provider.ApplicationConversionServiceFactoryBean;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.core.convert.converter.Converter;
import org.springframework.format.FormatterRegistry;

privileged aspect ApplicationConversionServiceFactoryBean_Roo_ConversionService {
    
    declare @type: ApplicationConversionServiceFactoryBean: @Configurable;
    
    public Converter<Address, String> ApplicationConversionServiceFactoryBean.getAddressToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.roo.esperanza.domain.Address, java.lang.String>() {
            public String convert(Address address) {
                return new StringBuilder().append(address.getAddress()).append(' ').append(address.getCity()).append(' ').append(address.getPostalCode()).toString();
            }
        };
    }
    
    public Converter<Long, Address> ApplicationConversionServiceFactoryBean.getIdToAddressConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.Long, com.roo.esperanza.domain.Address>() {
            public com.roo.esperanza.domain.Address convert(java.lang.Long id) {
                return Address.findAddress(id);
            }
        };
    }
    
    public Converter<String, Address> ApplicationConversionServiceFactoryBean.getStringToAddressConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.String, com.roo.esperanza.domain.Address>() {
            public com.roo.esperanza.domain.Address convert(String id) {
                return getObject().convert(getObject().convert(id, Long.class), Address.class);
            }
        };
    }
    
    public Converter<AdminAction, String> ApplicationConversionServiceFactoryBean.getAdminActionToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.roo.esperanza.domain.AdminAction, java.lang.String>() {
            public String convert(AdminAction adminAction) {
                return new StringBuilder().append(adminAction.getReason()).append(' ').append(adminAction.getDate()).toString();
            }
        };
    }
    
    public Converter<Long, AdminAction> ApplicationConversionServiceFactoryBean.getIdToAdminActionConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.Long, com.roo.esperanza.domain.AdminAction>() {
            public com.roo.esperanza.domain.AdminAction convert(java.lang.Long id) {
                return AdminAction.findAdminAction(id);
            }
        };
    }
    
    public Converter<String, AdminAction> ApplicationConversionServiceFactoryBean.getStringToAdminActionConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.String, com.roo.esperanza.domain.AdminAction>() {
            public com.roo.esperanza.domain.AdminAction convert(String id) {
                return getObject().convert(getObject().convert(id, Long.class), AdminAction.class);
            }
        };
    }
    
    public Converter<Category, String> ApplicationConversionServiceFactoryBean.getCategoryToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.roo.esperanza.domain.Category, java.lang.String>() {
            public String convert(Category category) {
                return new StringBuilder().append(category.getName()).toString();
            }
        };
    }
    
    public Converter<Long, Category> ApplicationConversionServiceFactoryBean.getIdToCategoryConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.Long, com.roo.esperanza.domain.Category>() {
            public com.roo.esperanza.domain.Category convert(java.lang.Long id) {
                return Category.findCategory(id);
            }
        };
    }
    
    public Converter<String, Category> ApplicationConversionServiceFactoryBean.getStringToCategoryConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.String, com.roo.esperanza.domain.Category>() {
            public com.roo.esperanza.domain.Category convert(String id) {
                return getObject().convert(getObject().convert(id, Long.class), Category.class);
            }
        };
    }
    
    public Converter<Conversation, String> ApplicationConversionServiceFactoryBean.getConversationToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.roo.esperanza.domain.Conversation, java.lang.String>() {
            public String convert(Conversation conversation) {
                return "(no displayable fields)";
            }
        };
    }
    
    public Converter<Long, Conversation> ApplicationConversionServiceFactoryBean.getIdToConversationConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.Long, com.roo.esperanza.domain.Conversation>() {
            public com.roo.esperanza.domain.Conversation convert(java.lang.Long id) {
                return Conversation.findConversation(id);
            }
        };
    }
    
    public Converter<String, Conversation> ApplicationConversionServiceFactoryBean.getStringToConversationConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.String, com.roo.esperanza.domain.Conversation>() {
            public com.roo.esperanza.domain.Conversation convert(String id) {
                return getObject().convert(getObject().convert(id, Long.class), Conversation.class);
            }
        };
    }
    
    public Converter<Country, String> ApplicationConversionServiceFactoryBean.getCountryToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.roo.esperanza.domain.Country, java.lang.String>() {
            public String convert(Country country) {
                return new StringBuilder().append(country.getCode()).append(' ').append(country.getName()).toString();
            }
        };
    }
    
    public Converter<Long, Country> ApplicationConversionServiceFactoryBean.getIdToCountryConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.Long, com.roo.esperanza.domain.Country>() {
            public com.roo.esperanza.domain.Country convert(java.lang.Long id) {
                return Country.findCountry(id);
            }
        };
    }
    
    public Converter<String, Country> ApplicationConversionServiceFactoryBean.getStringToCountryConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.String, com.roo.esperanza.domain.Country>() {
            public com.roo.esperanza.domain.Country convert(String id) {
                return getObject().convert(getObject().convert(id, Long.class), Country.class);
            }
        };
    }
    
    public Converter<Denouncement, String> ApplicationConversionServiceFactoryBean.getDenouncementToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.roo.esperanza.domain.Denouncement, java.lang.String>() {
            public String convert(Denouncement denouncement) {
                return new StringBuilder().append(denouncement.getReason()).append(' ').append(denouncement.getDate()).toString();
            }
        };
    }
    
    public Converter<Long, Denouncement> ApplicationConversionServiceFactoryBean.getIdToDenouncementConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.Long, com.roo.esperanza.domain.Denouncement>() {
            public com.roo.esperanza.domain.Denouncement convert(java.lang.Long id) {
                return Denouncement.findDenouncement(id);
            }
        };
    }
    
    public Converter<String, Denouncement> ApplicationConversionServiceFactoryBean.getStringToDenouncementConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.String, com.roo.esperanza.domain.Denouncement>() {
            public com.roo.esperanza.domain.Denouncement convert(String id) {
                return getObject().convert(getObject().convert(id, Long.class), Denouncement.class);
            }
        };
    }
    
    public Converter<Donation, String> ApplicationConversionServiceFactoryBean.getDonationToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.roo.esperanza.domain.Donation, java.lang.String>() {
            public String convert(Donation donation) {
                return new StringBuilder().append(donation.getDescription()).append(' ').append(donation.getDate()).toString();
            }
        };
    }
    
    public Converter<Long, Donation> ApplicationConversionServiceFactoryBean.getIdToDonationConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.Long, com.roo.esperanza.domain.Donation>() {
            public com.roo.esperanza.domain.Donation convert(java.lang.Long id) {
                return Donation.findDonation(id);
            }
        };
    }
    
    public Converter<String, Donation> ApplicationConversionServiceFactoryBean.getStringToDonationConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.String, com.roo.esperanza.domain.Donation>() {
            public com.roo.esperanza.domain.Donation convert(String id) {
                return getObject().convert(getObject().convert(id, Long.class), Donation.class);
            }
        };
    }
    
    public Converter<DonationGood, String> ApplicationConversionServiceFactoryBean.getDonationGoodToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.roo.esperanza.domain.DonationGood, java.lang.String>() {
            public String convert(DonationGood donationGood) {
                return new StringBuilder().append(donationGood.getDescription()).append(' ').append(donationGood.getDetailedDescription()).append(' ').append(donationGood.getDate()).append(' ').append(donationGood.getLink()).toString();
            }
        };
    }
    
    public Converter<Long, DonationGood> ApplicationConversionServiceFactoryBean.getIdToDonationGoodConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.Long, com.roo.esperanza.domain.DonationGood>() {
            public com.roo.esperanza.domain.DonationGood convert(java.lang.Long id) {
                return DonationGood.findDonationGood(id);
            }
        };
    }
    
    public Converter<String, DonationGood> ApplicationConversionServiceFactoryBean.getStringToDonationGoodConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.String, com.roo.esperanza.domain.DonationGood>() {
            public com.roo.esperanza.domain.DonationGood convert(String id) {
                return getObject().convert(getObject().convert(id, Long.class), DonationGood.class);
            }
        };
    }
    
    public Converter<DonationRequest, String> ApplicationConversionServiceFactoryBean.getDonationRequestToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.roo.esperanza.domain.DonationRequest, java.lang.String>() {
            public String convert(DonationRequest donationRequest) {
                return new StringBuilder().append(donationRequest.getDescription()).append(' ').append(donationRequest.getDate()).toString();
            }
        };
    }
    
    public Converter<Long, DonationRequest> ApplicationConversionServiceFactoryBean.getIdToDonationRequestConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.Long, com.roo.esperanza.domain.DonationRequest>() {
            public com.roo.esperanza.domain.DonationRequest convert(java.lang.Long id) {
                return DonationRequest.findDonationRequest(id);
            }
        };
    }
    
    public Converter<String, DonationRequest> ApplicationConversionServiceFactoryBean.getStringToDonationRequestConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.String, com.roo.esperanza.domain.DonationRequest>() {
            public com.roo.esperanza.domain.DonationRequest convert(String id) {
                return getObject().convert(getObject().convert(id, Long.class), DonationRequest.class);
            }
        };
    }
    
    public Converter<Institution, String> ApplicationConversionServiceFactoryBean.getInstitutionToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.roo.esperanza.domain.Institution, java.lang.String>() {
            public String convert(Institution institution) {
                return new StringBuilder().append(institution.getDate()).append(' ').append(institution.getPhone()).append(' ').append(institution.getCellPhone()).append(' ').append(institution.getProfilePicture()).toString();
            }
        };
    }
    
    public Converter<Long, Institution> ApplicationConversionServiceFactoryBean.getIdToInstitutionConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.Long, com.roo.esperanza.domain.Institution>() {
            public com.roo.esperanza.domain.Institution convert(java.lang.Long id) {
                return Institution.findInstitution(id);
            }
        };
    }
    
    public Converter<String, Institution> ApplicationConversionServiceFactoryBean.getStringToInstitutionConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.String, com.roo.esperanza.domain.Institution>() {
            public com.roo.esperanza.domain.Institution convert(String id) {
                return getObject().convert(getObject().convert(id, Long.class), Institution.class);
            }
        };
    }
    
    public Converter<Message, String> ApplicationConversionServiceFactoryBean.getMessageToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.roo.esperanza.domain.Message, java.lang.String>() {
            public String convert(Message message) {
                return new StringBuilder().append(message.getDate()).append(' ').append(message.getMessage()).toString();
            }
        };
    }
    
    public Converter<Long, Message> ApplicationConversionServiceFactoryBean.getIdToMessageConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.Long, com.roo.esperanza.domain.Message>() {
            public com.roo.esperanza.domain.Message convert(java.lang.Long id) {
                return Message.findMessage(id);
            }
        };
    }
    
    public Converter<String, Message> ApplicationConversionServiceFactoryBean.getStringToMessageConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.String, com.roo.esperanza.domain.Message>() {
            public com.roo.esperanza.domain.Message convert(String id) {
                return getObject().convert(getObject().convert(id, Long.class), Message.class);
            }
        };
    }
    
    public Converter<Multimedia, String> ApplicationConversionServiceFactoryBean.getMultimediaToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.roo.esperanza.domain.Multimedia, java.lang.String>() {
            public String convert(Multimedia multimedia) {
                return new StringBuilder().append(multimedia.getPath()).append(' ').append(multimedia.getDescription()).toString();
            }
        };
    }
    
    public Converter<Long, Multimedia> ApplicationConversionServiceFactoryBean.getIdToMultimediaConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.Long, com.roo.esperanza.domain.Multimedia>() {
            public com.roo.esperanza.domain.Multimedia convert(java.lang.Long id) {
                return Multimedia.findMultimedia(id);
            }
        };
    }
    
    public Converter<String, Multimedia> ApplicationConversionServiceFactoryBean.getStringToMultimediaConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.String, com.roo.esperanza.domain.Multimedia>() {
            public com.roo.esperanza.domain.Multimedia convert(String id) {
                return getObject().convert(getObject().convert(id, Long.class), Multimedia.class);
            }
        };
    }
    
    public Converter<Necessity, String> ApplicationConversionServiceFactoryBean.getNecessityToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.roo.esperanza.domain.Necessity, java.lang.String>() {
            public String convert(Necessity necessity) {
                return new StringBuilder().append(necessity.getDescription()).append(' ').append(necessity.getDetailedDescription()).append(' ').append(necessity.getDate()).append(' ').append(necessity.getLink()).toString();
            }
        };
    }
    
    public Converter<Long, Necessity> ApplicationConversionServiceFactoryBean.getIdToNecessityConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.Long, com.roo.esperanza.domain.Necessity>() {
            public com.roo.esperanza.domain.Necessity convert(java.lang.Long id) {
                return Necessity.findNecessity(id);
            }
        };
    }
    
    public Converter<String, Necessity> ApplicationConversionServiceFactoryBean.getStringToNecessityConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.String, com.roo.esperanza.domain.Necessity>() {
            public com.roo.esperanza.domain.Necessity convert(String id) {
                return getObject().convert(getObject().convert(id, Long.class), Necessity.class);
            }
        };
    }
    
    public Converter<Notification, String> ApplicationConversionServiceFactoryBean.getNotificationToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.roo.esperanza.domain.Notification, java.lang.String>() {
            public String convert(Notification notification) {
                return new StringBuilder().append(notification.getDate()).append(' ').append(notification.getDescription()).toString();
            }
        };
    }
    
    public Converter<Long, Notification> ApplicationConversionServiceFactoryBean.getIdToNotificationConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.Long, com.roo.esperanza.domain.Notification>() {
            public com.roo.esperanza.domain.Notification convert(java.lang.Long id) {
                return Notification.findNotification(id);
            }
        };
    }
    
    public Converter<String, Notification> ApplicationConversionServiceFactoryBean.getStringToNotificationConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.String, com.roo.esperanza.domain.Notification>() {
            public com.roo.esperanza.domain.Notification convert(String id) {
                return getObject().convert(getObject().convert(id, Long.class), Notification.class);
            }
        };
    }
    
    public Converter<Participant, String> ApplicationConversionServiceFactoryBean.getParticipantToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.roo.esperanza.domain.Participant, java.lang.String>() {
            public String convert(Participant participant) {
                return new StringBuilder().append(participant.getDate()).append(' ').append(participant.getPhone()).append(' ').append(participant.getCellPhone()).append(' ').append(participant.getProfilePicture()).toString();
            }
        };
    }
    
    public Converter<Long, Participant> ApplicationConversionServiceFactoryBean.getIdToParticipantConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.Long, com.roo.esperanza.domain.Participant>() {
            public com.roo.esperanza.domain.Participant convert(java.lang.Long id) {
                return Participant.findParticipant(id);
            }
        };
    }
    
    public Converter<String, Participant> ApplicationConversionServiceFactoryBean.getStringToParticipantConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.String, com.roo.esperanza.domain.Participant>() {
            public com.roo.esperanza.domain.Participant convert(String id) {
                return getObject().convert(getObject().convert(id, Long.class), Participant.class);
            }
        };
    }
    
    public Converter<Person, String> ApplicationConversionServiceFactoryBean.getPersonToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.roo.esperanza.domain.Person, java.lang.String>() {
            public String convert(Person person) {
                return new StringBuilder().append(person.getDate()).append(' ').append(person.getPhone()).append(' ').append(person.getCellPhone()).append(' ').append(person.getProfilePicture()).toString();
            }
        };
    }
    
    public Converter<Long, Person> ApplicationConversionServiceFactoryBean.getIdToPersonConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.Long, com.roo.esperanza.domain.Person>() {
            public com.roo.esperanza.domain.Person convert(java.lang.Long id) {
                return Person.findPerson(id);
            }
        };
    }
    
    public Converter<String, Person> ApplicationConversionServiceFactoryBean.getStringToPersonConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.String, com.roo.esperanza.domain.Person>() {
            public com.roo.esperanza.domain.Person convert(String id) {
                return getObject().convert(getObject().convert(id, Long.class), Person.class);
            }
        };
    }
    
    public Converter<Province, String> ApplicationConversionServiceFactoryBean.getProvinceToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.roo.esperanza.domain.Province, java.lang.String>() {
            public String convert(Province province) {
                return new StringBuilder().append(province.getName()).toString();
            }
        };
    }
    
    public Converter<Long, Province> ApplicationConversionServiceFactoryBean.getIdToProvinceConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.Long, com.roo.esperanza.domain.Province>() {
            public com.roo.esperanza.domain.Province convert(java.lang.Long id) {
                return Province.findProvince(id);
            }
        };
    }
    
    public Converter<String, Province> ApplicationConversionServiceFactoryBean.getStringToProvinceConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.String, com.roo.esperanza.domain.Province>() {
            public com.roo.esperanza.domain.Province convert(String id) {
                return getObject().convert(getObject().convert(id, Long.class), Province.class);
            }
        };
    }
    
    public Converter<PublicationHistory, String> ApplicationConversionServiceFactoryBean.getPublicationHistoryToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.roo.esperanza.domain.PublicationHistory, java.lang.String>() {
            public String convert(PublicationHistory publicationHistory) {
                return new StringBuilder().append(publicationHistory.getAdminName()).toString();
            }
        };
    }
    
    public Converter<Long, PublicationHistory> ApplicationConversionServiceFactoryBean.getIdToPublicationHistoryConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.Long, com.roo.esperanza.domain.PublicationHistory>() {
            public com.roo.esperanza.domain.PublicationHistory convert(java.lang.Long id) {
                return PublicationHistory.findPublicationHistory(id);
            }
        };
    }
    
    public Converter<String, PublicationHistory> ApplicationConversionServiceFactoryBean.getStringToPublicationHistoryConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.String, com.roo.esperanza.domain.PublicationHistory>() {
            public com.roo.esperanza.domain.PublicationHistory convert(String id) {
                return getObject().convert(getObject().convert(id, Long.class), PublicationHistory.class);
            }
        };
    }
    
    public Converter<Puntuation, String> ApplicationConversionServiceFactoryBean.getPuntuationToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.roo.esperanza.domain.Puntuation, java.lang.String>() {
            public String convert(Puntuation puntuation) {
                return new StringBuilder().append(puntuation.getComment()).append(' ').append(puntuation.getDate()).toString();
            }
        };
    }
    
    public Converter<Long, Puntuation> ApplicationConversionServiceFactoryBean.getIdToPuntuationConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.Long, com.roo.esperanza.domain.Puntuation>() {
            public com.roo.esperanza.domain.Puntuation convert(java.lang.Long id) {
                return Puntuation.findPuntuation(id);
            }
        };
    }
    
    public Converter<String, Puntuation> ApplicationConversionServiceFactoryBean.getStringToPuntuationConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.String, com.roo.esperanza.domain.Puntuation>() {
            public com.roo.esperanza.domain.Puntuation convert(String id) {
                return getObject().convert(getObject().convert(id, Long.class), Puntuation.class);
            }
        };
    }
    
    public Converter<Resource, String> ApplicationConversionServiceFactoryBean.getResourceToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.roo.esperanza.domain.Resource, java.lang.String>() {
            public String convert(Resource resource) {
                return new StringBuilder().append(resource.getIdResource()).toString();
            }
        };
    }
    
    public Converter<Long, Resource> ApplicationConversionServiceFactoryBean.getIdToResourceConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.Long, com.roo.esperanza.domain.Resource>() {
            public com.roo.esperanza.domain.Resource convert(java.lang.Long id) {
                return Resource.findResource(id);
            }
        };
    }
    
    public Converter<String, Resource> ApplicationConversionServiceFactoryBean.getStringToResourceConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.String, com.roo.esperanza.domain.Resource>() {
            public com.roo.esperanza.domain.Resource convert(String id) {
                return getObject().convert(getObject().convert(id, Long.class), Resource.class);
            }
        };
    }
    
    public Converter<Role, String> ApplicationConversionServiceFactoryBean.getRoleToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.roo.esperanza.domain.Role, java.lang.String>() {
            public String convert(Role role) {
                return new StringBuilder().append(role.getRoleDescription()).toString();
            }
        };
    }
    
    public Converter<Long, Role> ApplicationConversionServiceFactoryBean.getIdToRoleConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.Long, com.roo.esperanza.domain.Role>() {
            public com.roo.esperanza.domain.Role convert(java.lang.Long id) {
                return Role.findRole(id);
            }
        };
    }
    
    public Converter<String, Role> ApplicationConversionServiceFactoryBean.getStringToRoleConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.String, com.roo.esperanza.domain.Role>() {
            public com.roo.esperanza.domain.Role convert(String id) {
                return getObject().convert(getObject().convert(id, Long.class), Role.class);
            }
        };
    }
    
    public Converter<Suscription, String> ApplicationConversionServiceFactoryBean.getSuscriptionToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.roo.esperanza.domain.Suscription, java.lang.String>() {
            public String convert(Suscription suscription) {
                return new StringBuilder().append(suscription.getDate()).append(' ').append(suscription.getAmount()).toString();
            }
        };
    }
    
    public Converter<Long, Suscription> ApplicationConversionServiceFactoryBean.getIdToSuscriptionConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.Long, com.roo.esperanza.domain.Suscription>() {
            public com.roo.esperanza.domain.Suscription convert(java.lang.Long id) {
                return Suscription.findSuscription(id);
            }
        };
    }
    
    public Converter<String, Suscription> ApplicationConversionServiceFactoryBean.getStringToSuscriptionConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.String, com.roo.esperanza.domain.Suscription>() {
            public com.roo.esperanza.domain.Suscription convert(String id) {
                return getObject().convert(getObject().convert(id, Long.class), Suscription.class);
            }
        };
    }
    
    public Converter<User, String> ApplicationConversionServiceFactoryBean.getUserToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.roo.esperanza.domain.User, java.lang.String>() {
            public String convert(User user) {
                return new StringBuilder().append(user.getEmailAddress()).append(' ').append(user.getPassword()).append(' ').append(user.getActivationDate()).append(' ').append(user.getActivationKey()).toString();
            }
        };
    }
    
    public Converter<Long, User> ApplicationConversionServiceFactoryBean.getIdToUserConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.Long, com.roo.esperanza.domain.User>() {
            public com.roo.esperanza.domain.User convert(java.lang.Long id) {
                return User.findUser(id);
            }
        };
    }
    
    public Converter<String, User> ApplicationConversionServiceFactoryBean.getStringToUserConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.String, com.roo.esperanza.domain.User>() {
            public com.roo.esperanza.domain.User convert(String id) {
                return getObject().convert(getObject().convert(id, Long.class), User.class);
            }
        };
    }
    
    public Converter<UserRole, String> ApplicationConversionServiceFactoryBean.getUserRoleToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.roo.esperanza.domain.UserRole, java.lang.String>() {
            public String convert(UserRole userRole) {
                return "(no displayable fields)";
            }
        };
    }
    
    public Converter<Long, UserRole> ApplicationConversionServiceFactoryBean.getIdToUserRoleConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.Long, com.roo.esperanza.domain.UserRole>() {
            public com.roo.esperanza.domain.UserRole convert(java.lang.Long id) {
                return UserRole.findUserRole(id);
            }
        };
    }
    
    public Converter<String, UserRole> ApplicationConversionServiceFactoryBean.getStringToUserRoleConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.String, com.roo.esperanza.domain.UserRole>() {
            public com.roo.esperanza.domain.UserRole convert(String id) {
                return getObject().convert(getObject().convert(id, Long.class), UserRole.class);
            }
        };
    }
    
    public void ApplicationConversionServiceFactoryBean.installLabelConverters(FormatterRegistry registry) {
        registry.addConverter(getAddressToStringConverter());
        registry.addConverter(getIdToAddressConverter());
        registry.addConverter(getStringToAddressConverter());
        registry.addConverter(getAdminActionToStringConverter());
        registry.addConverter(getIdToAdminActionConverter());
        registry.addConverter(getStringToAdminActionConverter());
        registry.addConverter(getCategoryToStringConverter());
        registry.addConverter(getIdToCategoryConverter());
        registry.addConverter(getStringToCategoryConverter());
        registry.addConverter(getConversationToStringConverter());
        registry.addConverter(getIdToConversationConverter());
        registry.addConverter(getStringToConversationConverter());
        registry.addConverter(getCountryToStringConverter());
        registry.addConverter(getIdToCountryConverter());
        registry.addConverter(getStringToCountryConverter());
        registry.addConverter(getDenouncementToStringConverter());
        registry.addConverter(getIdToDenouncementConverter());
        registry.addConverter(getStringToDenouncementConverter());
        registry.addConverter(getDonationToStringConverter());
        registry.addConverter(getIdToDonationConverter());
        registry.addConverter(getStringToDonationConverter());
        registry.addConverter(getDonationGoodToStringConverter());
        registry.addConverter(getIdToDonationGoodConverter());
        registry.addConverter(getStringToDonationGoodConverter());
        registry.addConverter(getDonationRequestToStringConverter());
        registry.addConverter(getIdToDonationRequestConverter());
        registry.addConverter(getStringToDonationRequestConverter());
        registry.addConverter(getInstitutionToStringConverter());
        registry.addConverter(getIdToInstitutionConverter());
        registry.addConverter(getStringToInstitutionConverter());
        registry.addConverter(getMessageToStringConverter());
        registry.addConverter(getIdToMessageConverter());
        registry.addConverter(getStringToMessageConverter());
        registry.addConverter(getMultimediaToStringConverter());
        registry.addConverter(getIdToMultimediaConverter());
        registry.addConverter(getStringToMultimediaConverter());
        registry.addConverter(getNecessityToStringConverter());
        registry.addConverter(getIdToNecessityConverter());
        registry.addConverter(getStringToNecessityConverter());
        registry.addConverter(getNotificationToStringConverter());
        registry.addConverter(getIdToNotificationConverter());
        registry.addConverter(getStringToNotificationConverter());
        registry.addConverter(getParticipantToStringConverter());
        registry.addConverter(getIdToParticipantConverter());
        registry.addConverter(getStringToParticipantConverter());
        registry.addConverter(getPersonToStringConverter());
        registry.addConverter(getIdToPersonConverter());
        registry.addConverter(getStringToPersonConverter());
        registry.addConverter(getProvinceToStringConverter());
        registry.addConverter(getIdToProvinceConverter());
        registry.addConverter(getStringToProvinceConverter());
        registry.addConverter(getPublicationHistoryToStringConverter());
        registry.addConverter(getIdToPublicationHistoryConverter());
        registry.addConverter(getStringToPublicationHistoryConverter());
        registry.addConverter(getPuntuationToStringConverter());
        registry.addConverter(getIdToPuntuationConverter());
        registry.addConverter(getStringToPuntuationConverter());
        registry.addConverter(getResourceToStringConverter());
        registry.addConverter(getIdToResourceConverter());
        registry.addConverter(getStringToResourceConverter());
        registry.addConverter(getRoleToStringConverter());
        registry.addConverter(getIdToRoleConverter());
        registry.addConverter(getStringToRoleConverter());
        registry.addConverter(getSuscriptionToStringConverter());
        registry.addConverter(getIdToSuscriptionConverter());
        registry.addConverter(getStringToSuscriptionConverter());
        registry.addConverter(getUserToStringConverter());
        registry.addConverter(getIdToUserConverter());
        registry.addConverter(getStringToUserConverter());
        registry.addConverter(getUserRoleToStringConverter());
        registry.addConverter(getIdToUserRoleConverter());
        registry.addConverter(getStringToUserRoleConverter());
    }
    
    public void ApplicationConversionServiceFactoryBean.afterPropertiesSet() {
        super.afterPropertiesSet();
        installLabelConverters(getObject());
    }
    
}
