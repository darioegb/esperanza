package com.roo.esperanza.provider;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

// TODO: Auto-generated Javadoc
/**
 * The Class ScriptAddService.
 */
@Service
public class ScriptAddService {
	
	/** The lista js. */
	private List<String> listaJS;
    
    /** The lista css. */
    private List<String> listaCSS;
	
	/**
	 * Agregar js.
	 *
	 * @param url the url
	 */
	public void agregarJS(String url) {
		if(listaJS == null) {
			listaJS = new ArrayList<String>();
		}
		listaJS.add(url);
	}
	
	/**
	 * Agregar css.
	 *
	 * @param url the url
	 */
	public void agregarCSS(String url) {
		if(listaCSS == null) {
			listaCSS = new ArrayList<String>();
		}
		listaCSS.add(url);
	}
	
	/**
	 * Clean js css.
	 */
	public void cleanJsCss() {
		listaJS = null;
		listaCSS = null;
	}
	
	/**
	 * Publicar js css.
	 *
	 * @param model the model
	 */
	public void publicarJsCss(Model model) {
		model.addAttribute("JSs", listaJS);
		model.addAttribute("CSSs", listaCSS);
	}
}
