package com.roo.esperanza.provider;
import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

// TODO: Auto-generated Javadoc
/**
 * The Class ImageService.
 */
@Service
public class ImageService {
	
	/** The relative path image. */
	private String relativePathImage;

	/** The save directory. */
	private String saveDirectory;
	
	/** The save directory db. */
	private String saveDirectoryDB;
		
	/**
	 * Gets the relative path image.
	 *
	 * @return the relative path image
	 */
	public String getRelativePathImage() {
		return relativePathImage;
	}

	/**
	 * Sets the relative path image.
	 *
	 * @param relativePathImage the new relative path image
	 */
	public void setRelativePathImage(String relativePathImage) {
		this.relativePathImage = relativePathImage;
	}
	
	/**
	 * Gets the default person image.
	 *
	 * @return the default person image
	 */
	public String getDefaultPersonImage(){
		return "images/profileUser/person.png";
		
	}
	
	/**
	 * Gets the default institution image.
	 *
	 * @return the default institution image
	 */
	public String getDefaultInstitutionImage(){
		return "images/profileUser/institution.png";
		
	}

	/**
	 * Upload file.
	 *
	 * @param multimediaFile the multimedia file
	 * @param id the id
	 * @param type the type
	 * @return the boolean
	 */
	public Boolean UploadFile(MultipartFile multimediaFile, final Long id, int type) {
		String rootPath = System.getProperty("catalina.base");
		if(type == 0){
			saveDirectory = rootPath + File.separator + "images/profileUser/";
			saveDirectoryDB = "images/profileUser/";
		}else if(type == 1){
			saveDirectory = rootPath + File.separator + "images/gallery"+ File.separator + id + "/";
			saveDirectoryDB = "images/gallery/" + id + "/";
		}else{
			saveDirectory = rootPath + File.separator + "images/donationGood"+ File.separator + id + "/";
			saveDirectoryDB = "images/donationGood/" + id + "/";
		}
		File file = new File(saveDirectory);
		if(!file.exists()){
			if(file.mkdirs()){
			  System.out.println("directory is created!");
			}else{
				System.out.println("failed to create directory!");  
			}
		}
		String ext = ".jpg";
		String newName = id.toString();
		long actualdate = new Date().getTime();
		try {
			if (multimediaFile.getOriginalFilename().toLowerCase().contains("png")) {
	        	ext = ".png";
	        }
	        if (multimediaFile.getOriginalFilename().toLowerCase().contains("jpg")) {
	            ext = ".jpg";
	        }
	        if (multimediaFile.getOriginalFilename().toLowerCase().contains("gif")) {
	            ext = ".gif";
	        }
	        if (multimediaFile.getOriginalFilename().toLowerCase().contains("jpeg")) {
	            ext = ".jpeg";
	        }
			multimediaFile.transferTo(new File(saveDirectory + newName + "-" + actualdate + ext));
		    setRelativePathImage(saveDirectoryDB + newName + "-" + actualdate + ext);
			return true;
		} catch (IllegalStateException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Delete file.
	 *
	 * @param oldName the old name
	 */
	public void deleteFile(String oldName){
		int index=oldName.lastIndexOf('/');
		oldName = oldName.substring(index+1);
		try{
			File photo = new File(saveDirectory + oldName);
		    System.out.println(photo.isFile());
		  		if(photo.delete()){
		  			System.out.println(photo.getName() + " is deleted!");
		}else{
			System.out.println("Delete operation is failed.");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
