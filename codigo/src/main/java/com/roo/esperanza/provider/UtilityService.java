package com.roo.esperanza.provider;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Service;

// TODO: Auto-generated Javadoc
/**
 * The Class UtilityService.
 */
@Service
public class UtilityService {
	
	/**
	 * Hash sha256.
	 *
	 * @param data the data
	 * @return the string
	 */
	public String hashSha256(String data){
		 MessageDigest sha256 = null;
		try {
			sha256 = MessageDigest.getInstance("SHA-256");
		} catch (NoSuchAlgorithmException e1) {
			e1.printStackTrace();
		}
         try {
			sha256.update(data.getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
         byte[] digest = sha256.digest();
         StringBuffer sb=new StringBuffer();
         for(int i=0;i<digest.length;i++){
             sb.append(String.format("%02x", digest[i]));
         }
		return sb.toString();
		
	}
	
	/**
	 * Url embed you tube.
	 *
	 * @param url the url
	 * @return the string
	 */
	public String urlEmbedYouTube(String url){
		String pattern = "(?<=watch\\?v=|/videos/)[^#\\&\\?]*";

	    Pattern compiledPattern = Pattern.compile(pattern);
	    Matcher matcher = compiledPattern.matcher(url);

	    if(matcher.find() && matcher.group().length() == 11){
	        return "http://www.youtube.com/embed/"+matcher.group();
	    }else{
	    	return "error";
	    }	
	}
	
	/**
	 * check if exist internet connexion.
	 *
	 * @return the boolean
	 */
	public Boolean checkInternetConnexion()
		throws UnknownHostException,
		IOException {
	try {
		try {
			URL url = new URL("http://www.google.com");
			System.out.println(url.getHost());
			HttpURLConnection con = (HttpURLConnection) url
					.openConnection();
			con.connect();
			if (con.getResponseCode() == 200){
				return true;
			}
			System.out.println(con.getResponseCode());
		} catch (Exception exception) {
			return false;
		}
	} catch (Exception e) {
		e.printStackTrace();
	}
	return null;

	}
}
