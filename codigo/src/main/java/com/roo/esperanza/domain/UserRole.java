package com.roo.esperanza.domain;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

// TODO: Auto-generated Javadoc
/**
 * The Class UserRole.
 */
@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findUserRolesByUserEntry" })
public class UserRole {

    /** The user entry. */
    @NotNull
    @ManyToOne
    private User userEntry;

    /** The role entry. */
    @NotNull
    @ManyToOne
    private Role roleEntry;
}
