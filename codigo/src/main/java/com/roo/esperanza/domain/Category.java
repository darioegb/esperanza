package com.roo.esperanza.domain;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import javax.persistence.ManyToOne;
import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

// TODO: Auto-generated Javadoc
/**
 * The Class Category.
 */
@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class Category {

    /** The parent. */
    @ManyToOne
    private Category parent;

    /** The name. */
    @NotNull
    @Column(name = "name")
    @Size(min = 5, max = 65)
    private String name;
}
