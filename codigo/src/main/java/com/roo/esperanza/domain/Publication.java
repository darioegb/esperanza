package com.roo.esperanza.domain;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.Query;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

import com.roo.esperanza.reference.DonationTypeEnum;
import com.roo.esperanza.reference.StatusTypeEnum;

// TODO: Auto-generated Javadoc
/**
 * The Class Publication.
 */
@RooJavaBean
@RooToString
@RooJpaActiveRecord
public abstract class Publication {

    /** The description. */
    @NotNull
    @Column(name = "description")
    @Size(min = 5, max = 100)
    private String description;
    
    /** The detailed description. */
    @Column(name = "detailed_description")
    @Size(min = 100, max = 300)
    private String detailedDescription;
    
    /** The creator. */
    @ManyToOne
    private Participant creator;

    /** The donation type. */
    @NotNull
    @Enumerated(EnumType.STRING)
    private DonationTypeEnum donationType;

    /** The date. */
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Date date;

    /** The link. */
    @Column(name = "link")
    @Size(min = 5, max = 150)
    private String link;

    /** The status type. */
    @NotNull
    @Enumerated(EnumType.STRING)
    private StatusTypeEnum statusType;

    /** The history. */
    @ManyToOne
    private PublicationHistory history;
    
    /**
     * Find publication by hash.
     *
     * @param hash the hash
     * @return the publication
     */
    public static Publication findPublicationByHash(String hash) {
        EntityManager em = Publication.entityManager();
        Query q = em.createNativeQuery("SELECT * FROM publication AS p WHERE SHA2(p.id,256)=:hash", Publication.class);
        q.setParameter("hash", hash);
        return (Publication) q.getSingleResult();
    }

	/**
	 * Find publications by date between or description like or status type or donation type or creator role type.
	 *
	 * @param minDate the min date
	 * @param maxDate the max date
	 * @param description the description
	 * @param statusType the status type
	 * @param donationType the donation type
	 * @param role the role
	 * @return the typed query
	 */
	public static TypedQuery<Publication> findPublicationsByDateBetweenOrDescriptionLikeOrStatusTypeOrDonationTypeOrCreatorRoleType(Date minDate, Date maxDate, String description, StatusTypeEnum statusType, DonationTypeEnum donationType, Role role) {
		EntityManager em = Publication.entityManager();
		if (description.length() > 0){
	        description = description.replace('*', '%');
	        if (description.charAt(0) != '%')	description = "%" + description;
	        if (description.charAt(description.length() - 1) != '%')	description = description + "%";	       
        }
		
		StringBuilder queryBuilder = new StringBuilder("SELECT o FROM Publication AS o WHERE o.date BETWEEN :minDate AND :maxDate  OR LOWER(o.description) LIKE LOWER(:description)  OR o.statusType = :statusType OR o.donationType = :donationType");
		if(role != null)	queryBuilder.append(" OR o.creator.user.role = :role ");
		queryBuilder.append(" AND o.creator.user.enabled=1 ORDER BY o.id DESC");
        TypedQuery<Publication> q = em.createQuery(queryBuilder.toString(), Publication.class);
        q.setParameter("minDate", minDate);
        q.setParameter("maxDate", maxDate);
        q.setParameter("description", description);
        q.setParameter("statusType", statusType);
        q.setParameter("donationType", donationType);
        if(role != null)	q.setParameter("role", role);
        return q;
    }

	/**
	 * Count find publications by date between or description like or status type or donation type or creator role type.
	 *
	 * @param minDate the min date
	 * @param maxDate the max date
	 * @param description the description
	 * @param statusType the status type
	 * @param donationType the donation type
	 * @param role the role
	 * @return the long
	 */
	public static Long countFindPublicationsByDateBetweenOrDescriptionLikeOrStatusTypeOrDonationTypeOrCreatorRoleType(Date minDate, Date maxDate, String description, StatusTypeEnum statusType, DonationTypeEnum donationType, Role role) {
		EntityManager em = Publication.entityManager();
		if (description.length() > 0){
	        description = description.replace('*', '%');
	        if (description.charAt(0) != '%')	description = "%" + description;
	        if (description.charAt(description.length() - 1) != '%')	description = description + "%";	       
        }
		StringBuilder queryBuilder = new StringBuilder("SELECT COUNT(o) FROM Publication AS o WHERE o.date BETWEEN :minDate AND :maxDate  OR LOWER(o.description) LIKE LOWER(:description)  OR o.statusType = :statusType OR o.donationType = :donationType");
		if(role != null)	queryBuilder.append(" OR o.creator.user.role = :role ");
		queryBuilder.append(" AND o.creator.user.enabled=1 ORDER BY o.id DESC");
        TypedQuery<Long> q = em.createQuery(queryBuilder.toString(), Long.class);
        q.setParameter("minDate", minDate);
        q.setParameter("maxDate", maxDate);
        q.setParameter("description", description);
        q.setParameter("statusType", statusType);
        q.setParameter("donationType", donationType);
        if(role != null)	q.setParameter("role", role);
        return ((Long) q.getSingleResult());
    }

	/**
	 * Find publications by date between or description like or status type or donation type or creator role type.
	 *
	 * @param minDate the min date
	 * @param maxDate the max date
	 * @param description the description
	 * @param statusType the status type
	 * @param donationType the donation type
	 * @param role the role
	 * @param sortFieldName the sort field name
	 * @param sortOrder the sort order
	 * @return the typed query
	 */
	public static TypedQuery<Publication> findPublicationsByDateBetweenOrDescriptionLikeOrStatusTypeOrDonationTypeOrCreatorRoleType(Date minDate, Date maxDate, String description, StatusTypeEnum statusType, DonationTypeEnum donationType, Role role, String sortFieldName, String sortOrder) {
		EntityManager em = Publication.entityManager();
		if (description.length() > 0){
	        description = description.replace('*', '%');
	        if (description.charAt(0) != '%')	description = "%" + description;
	        if (description.charAt(description.length() - 1) != '%')	description = description + "%";	       
        }
        StringBuilder queryBuilder = new StringBuilder("SELECT o FROM Publication AS o WHERE o.date BETWEEN :minDate AND :maxDate  OR LOWER(o.description) LIKE LOWER(:description)  OR o.statusType = :statusType OR o.donationType = :donationType");
		if(role != null)	queryBuilder.append(" OR o.creator.user.role = :role ");
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            queryBuilder.append(" ORDER BY ").append(sortFieldName);
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder))	queryBuilder.append(" ").append(sortOrder);
        }
        TypedQuery<Publication> q = em.createQuery(queryBuilder.toString(), Publication.class);
        q.setParameter("minDate", minDate);
        q.setParameter("maxDate", maxDate);
        q.setParameter("description", description);
        q.setParameter("statusType", statusType);
        q.setParameter("donationType", donationType);
        if(role != null)	q.setParameter("role", role);
        return q;
    }

	/**
	 * Find all publications by status type.
	 *
	 * @param sortFieldName the sort field name
	 * @param sortOrder the sort order
	 * @return the list
	 */
	public static List<Publication> findAllPublicationsByStatusType(String sortFieldName, String sortOrder) {
		EntityManager em = Publication.entityManager();
		StringBuilder queryBuilder = new StringBuilder("SELECT o FROM Publication AS o WHERE o.creator.user.enabled=1 AND o.statusType='Create' ORDER BY o.id DESC");
        TypedQuery<Publication> q = em.createQuery(queryBuilder.toString(), Publication.class);
        return q.getResultList();
    }
	
	/**
	 * Find all publications by .
	 *
	 * @param sortFieldName the sort field name
	 * @param sortOrder the sort order
	 * @return the list
	 */
	public static List<Publication> findAllPublicationsByCreator(Participant creator) {
		EntityManager em = Publication.entityManager();
		StringBuilder queryBuilder = new StringBuilder("SELECT o FROM Publication AS o WHERE o.creator.user.enabled=1 AND o.creator = :creator AND (o.statusType = 'Accept' OR o.statusType = 'Finish') ORDER BY o.id DESC");
        TypedQuery<Publication> q = em.createQuery(queryBuilder.toString(), Publication.class);
        q.setParameter("creator", creator);
        return q.getResultList();
    }
}
