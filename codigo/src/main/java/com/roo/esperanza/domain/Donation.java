package com.roo.esperanza.domain;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.Query;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import com.roo.esperanza.reference.DonationStatusTypeEnum;

// TODO: Auto-generated Javadoc
/**
 * The Class Donation.
 */
@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findDonationsByReceiverOrDateBetweenOrDescriptionLikeOrPublication" })
public class Donation {

    /** The description. */
    @Column(name = "description")
    @Size(min = 5, max = 100)
    private String description;

    /** The donation status type. */
    @NotNull
    @Enumerated(EnumType.STRING)
    private DonationStatusTypeEnum donationStatusType;

    /** The donor. */
    @ManyToOne
    private Person donor;

    /** The date. */
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Date date;

    /** The receiver. */
    @ManyToOne
    private Participant receiver;
    
    /** The publication. */
    @ManyToOne
    private Publication publication;
    
    /**
     * Find donation by hash.
     *
     * @param hash the hash
     * @return the donation
     */
    public static Donation findDonationByHash(String hash) {
        EntityManager em = Donation.entityManager();
        Query q = em.createNativeQuery("SELECT * FROM donation AS d WHERE SHA2(d.id,256)=:hash", Donation.class);
        q.setParameter("hash", hash);
        return (Donation) q.getSingleResult();
    }

    /**
     * Find donations by receiver or date between or description like or donation type.
     *
     * @param receiver the receiver
     * @param minDate the min date
     * @param maxDate the max date
     * @param description the description
     * @param personFilter the person filter
     * @param sortFieldName the sort field name
     * @param sortOrder the sort order
     * @return the typed query
     */
    public static TypedQuery<Donation> findDonationsByReceiverOrDateBetweenOrDescriptionLikeOrPersonLikeOrDonationTypeOrPublication(Participant receiver, Date minDate, Date maxDate, String description, String personFilter, Long publicationId, String sortFieldName, String sortOrder) {
        EntityManager em = Donation.entityManager();
        if (description.length() > 0) {
            description = description.replace('*', '%');
            if (description.charAt(0) != '%') description = "%" + description;
            if (description.charAt(description.length() - 1) != '%') description = description + "%";
        }
        StringBuilder queryBuilder = new StringBuilder("SELECT o FROM Donation AS o WHERE o.date BETWEEN :minDate AND :maxDate  OR LOWER(o.description) LIKE LOWER(:description) OR o.publication.id = :publicationId");
        String[] output = null;
        if (personFilter.length() > 0){
            String pattern = " OR (";
        	output = personFilter.split(" ");
        	for (int i = 0; i < output.length; i++) {
        		output[i] = output[i].replace('*', '%');
    	        if (output[i].charAt(0) != '%')	output[i] = "%" + output[i];
    	        if (output[i].charAt(output[i].length() - 1) != '%')	output[i] = output[i] + "%";
    	        if(i>0) pattern += " OR ";
    	        pattern += "LOWER(o.donor.firstName) LIKE LOWER(:filter"+i+") OR LOWER(o.donor.lastName) LIKE LOWER(:filter"+i+")";
			}
        	pattern += ")";
        	queryBuilder.append(pattern);
        }
        queryBuilder.append(" AND o.receiver = :receiver");
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            queryBuilder.append(" ORDER BY ").append(sortFieldName);
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder)) {
                queryBuilder.append(" ").append(sortOrder);
            }
        }
        TypedQuery<Donation> q = em.createQuery(queryBuilder.toString(), Donation.class);
        q.setParameter("receiver", receiver);
        q.setParameter("minDate", minDate);
        q.setParameter("maxDate", maxDate);
        q.setParameter("description", description);
        q.setParameter("publicationId", publicationId);
        if(output != null){
	        for (int i = 0; i < output.length; i++) {
	        	q.setParameter("filter"+i, output[i]);
	        }
        }
        return q;
    }

    /**
     * Count find donations by receiver or date between or description like or donation type.
     *
     * @param receiver the receiver
     * @param minDate the min date
     * @param maxDate the max date
     * @param description the description
     * @param personFilter the person filter
     * @return the long
     */
    public static Long countFindDonationsByReceiverOrDateBetweenOrDescriptionLikeOrPersonLikeOrDonationTypeOrPublication(Participant receiver, Date minDate, Date maxDate, String description, String personFilter, Long publicationId) {
        EntityManager em = Donation.entityManager();
        if (description.length() > 0) {
            description = description.replace('*', '%');
            if (description.charAt(0) != '%') description = "%" + description;
            if (description.charAt(description.length() - 1) != '%') description = description + "%";
        }
        StringBuilder queryBuilder = new StringBuilder("SELECT COUNT(o) FROM Donation AS o WHERE o.date BETWEEN :minDate AND :maxDate  OR LOWER(o.description) LIKE LOWER(:description) OR o.publication.id = :publicationId");
        String[] output = null;
        if (personFilter.length() > 0){
            String pattern = " OR (";
        	output = personFilter.split(" ");
        	for (int i = 0; i < output.length; i++) {
        		output[i] = output[i].replace('*', '%');
    	        if (output[i].charAt(0) != '%')	output[i] = "%" + output[i];
    	        if (output[i].charAt(output[i].length() - 1) != '%')	output[i] = output[i] + "%";
    	        if(i>0) pattern += " OR ";
    	        pattern += "LOWER(o.donor.firstName) LIKE LOWER(:filter"+i+") OR LOWER(o.donor.lastName) LIKE LOWER(:filter"+i+")";
			}
        	pattern += ")";
        	queryBuilder.append(pattern);
        }
        queryBuilder.append(" AND o.receiver = :receiver");
        TypedQuery<Long> q = em.createQuery(queryBuilder.toString(), Long.class);
        q.setParameter("receiver", receiver);
        q.setParameter("minDate", minDate);
        q.setParameter("maxDate", maxDate);
        q.setParameter("description", description);
        q.setParameter("publicationId", publicationId);
        if(output != null){
	        for (int i = 0; i < output.length; i++) {
	        	q.setParameter("filter"+i, output[i]);
	        }
        }
        return ((Long) q.getSingleResult());
    }
    
    public static Long countFindDonationsByPublication(Long publicationId) {
        EntityManager em = Donation.entityManager();
        StringBuilder queryBuilder = new StringBuilder("SELECT COUNT(o) FROM Donation AS o WHERE o.publication.id = :publicationId AND o.donationStatusType!='Cancel'");
        TypedQuery<Long> q = em.createQuery(queryBuilder.toString(), Long.class);
        q.setParameter("publicationId", publicationId);
        return ((Long) q.getSingleResult());
    }
    
    public static List<Donation> findDonationsByPublication(Long publicationId) {
        EntityManager em = Donation.entityManager();
        StringBuilder queryBuilder = new StringBuilder("SELECT o FROM Donation AS o WHERE o.publication.id = :publicationId AND o.donationStatusType!='Cancel' ORDER BY o.id DESC");
        TypedQuery<Donation> q = em.createQuery(queryBuilder.toString(), Donation.class);
        q.setParameter("publicationId", publicationId);
        return  q.getResultList();
    }

    /**
     * Find all donations by receiver.
     *
     * @param receiver the receiver
     * @param sortFieldName the sort field name
     * @param sortOrder the sort order
     * @return the list
     */
    public static List<Donation> findAllDonationsByReceiver(Participant receiver, String sortFieldName, String sortOrder) {
        EntityManager em = Donation.entityManager();
        StringBuilder queryBuilder = new StringBuilder("SELECT o FROM Donation AS o WHERE o.receiver=:receiver");
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            queryBuilder.append(" ORDER BY ").append(sortFieldName);
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder)) queryBuilder.append(" " + sortOrder);
        }
        TypedQuery<Donation> q = em.createQuery(queryBuilder.toString(), Donation.class);
        q.setParameter("receiver", receiver);
        return q.getResultList();
    }
    
    /**
     * Find last donations by receiver.
     *
     * @param receiver the receiver
     * @param count the size
     * @return the list
     */
    public static List<Donation> findLastDonationsByReceiver(Participant receiver, int count) {
        EntityManager em = Donation.entityManager();
        StringBuilder queryBuilder = new StringBuilder("SELECT o FROM Donation AS o WHERE o.receiver=:receiver ORDER BY o.date DESC");
        TypedQuery<Donation> q = em.createQuery(queryBuilder.toString(), Donation.class);
        q.setParameter("receiver", receiver);
        return q.setMaxResults(count).getResultList();
    }

    /**
     * Find donation by receiver and date.
     *
     * @param receiver the receiver
     * @param date the date
     * @return the typed query
     */
    public static TypedQuery<Donation> findDonationByReceiverAndDate(Participant receiver, Date date) {
        EntityManager em = Donation.entityManager();
        StringBuilder queryBuilder = new StringBuilder("SELECT o FROM Donation AS o WHERE o.receiver = :receiver AND o.date = :date");
        TypedQuery<Donation> q = em.createQuery(queryBuilder.toString(), Donation.class);
        q.setParameter("receiver", receiver);
        q.setParameter("date", date);
        return q;
    }
}
