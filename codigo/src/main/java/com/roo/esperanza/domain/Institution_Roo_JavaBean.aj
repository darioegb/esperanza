// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.roo.esperanza.domain;

import com.roo.esperanza.domain.Category;
import com.roo.esperanza.domain.Institution;
import com.roo.esperanza.domain.Multimedia;
import java.util.Set;

privileged aspect Institution_Roo_JavaBean {
    
    public String Institution.getSocialReason() {
        return this.socialReason;
    }
    
    public void Institution.setSocialReason(String socialReason) {
        this.socialReason = socialReason;
    }
    
    public Integer Institution.getCbu() {
        return this.cbu;
    }
    
    public void Institution.setCbu(Integer cbu) {
        this.cbu = cbu;
    }
    
    public String Institution.getShortDescription() {
        return this.shortDescription;
    }
    
    public void Institution.setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }
    
    public String Institution.getLongDescription() {
        return this.longDescription;
    }
    
    public void Institution.setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }
    
    public Set<Multimedia> Institution.getArrayMutimedia() {
        return this.arrayMutimedia;
    }
    
    public void Institution.setArrayMutimedia(Set<Multimedia> arrayMutimedia) {
        this.arrayMutimedia = arrayMutimedia;
    }
    
    public Category Institution.getCategory() {
        return this.category;
    }
    
    public void Institution.setCategory(Category category) {
        this.category = category;
    }
    
}
