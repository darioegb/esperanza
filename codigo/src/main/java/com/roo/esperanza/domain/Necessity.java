package com.roo.esperanza.domain;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import com.roo.esperanza.reference.DonationTypeEnum;
import com.roo.esperanza.reference.PriorityTypeEnum;

// TODO: Auto-generated Javadoc
/**
 * The Class Necessity.
 */
@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findNecessitysByDescriptionLikeOrDonationTypeOrPriorityTypeOrDateBetween" })
public class Necessity extends Publication {
    
    /** The priority type. */
    @NotNull
    @Enumerated(EnumType.STRING)
    private PriorityTypeEnum priorityType;

	/**
	 * Find all necessitys by creator.
	 *
	 * @param sortFieldName the sort field name
	 * @param sortOrder the sort order
	 * @param creator the creator
	 * @return the list
	 */
	public static List<Necessity> findAllNecessitysByCreator(String sortFieldName, String sortOrder, Participant creator) {
        EntityManager em = Necessity.entityManager();
        TypedQuery<Necessity> q = em.createQuery("SELECT o FROM Necessity AS o WHERE o.creator = :creator AND o.statusType!='Block'", Necessity.class);
        q.setParameter("creator",creator);
        return q.getResultList();
    }
	
	/**
	 * Find all necessitys by creator distinct.
	 *
	 * @param creator the creator
	 * @return the list
	 */
	public static List<Necessity> findAllNecessitysByCreatorDistinct(Participant creator) {
        EntityManager em = Necessity.entityManager();
        TypedQuery<Necessity> q = em.createQuery("SELECT o FROM Necessity AS o WHERE o.creator != :creator AND o.statusType='Accept' AND o.creator.user.enabled=1", Necessity.class);
        q.setParameter("creator",creator);
        return q.getResultList();
    }
	
	/**
	 * Find all necessity by creator.
	 *
	 * @param firstResult the first result
	 * @param maxResults the max results
	 * @param sortFieldName the sort field name
	 * @param sortOrder the sort order
	 * @param creator the creator
	 * @return the list
	 */
	public static List<Necessity> findAllNecessityByCreator(int firstResult, int maxResults, String sortFieldName, String sortOrder, Participant creator) {
    	EntityManager em = Necessity.entityManager();
    	StringBuilder queryBuilder = new StringBuilder("SELECT o FROM Necessity o WHERE o.creator = :creator AND o.statusType='Accept'");
    	if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            queryBuilder.append(" ORDER BY ").append(sortFieldName);
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder))	queryBuilder.append(" ").append(sortOrder);
        }
        TypedQuery<Necessity> q = em.createQuery(queryBuilder.toString(), Necessity.class);
        q.setParameter("creator",creator);
        return q.setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }

	/**
	 * Find necessitys by description like or donation type or priority type or date between.
	 *
	 * @param description the description
	 * @param participantFilter the participant filter
	 * @param donationType the donation type
	 * @param priorityType the priority type
	 * @param minDate the min date
	 * @param maxDate the max date
	 * @param sortFieldName the sort field name
	 * @param sortOrder the sort order
	 * @return the typed query
	 */
	public static TypedQuery<Necessity> findNecessitysByDescriptionLikeOrParticipantLikeOrDonationTypeOrPriorityTypeOrDateBetween(String description, String participantFilter, DonationTypeEnum donationType, PriorityTypeEnum priorityType, Date minDate, Date maxDate, String sortFieldName, String sortOrder) {
		EntityManager em = Necessity.entityManager();
		if (description.length() > 0){
	        description = description.replace('*', '%');
	        if (description.charAt(0) != '%')	description = "%" + description;
	        if (description.charAt(description.length() - 1) != '%')	description = description + "%";	       
        }
		if (participantFilter.length() > 0){
			participantFilter = participantFilter.replace('*', '%');
	        if (participantFilter.charAt(0) != '%')	participantFilter = "%" + participantFilter;
	        if (participantFilter.charAt(participantFilter.length() - 1) != '%')	participantFilter = participantFilter + "%";	       
        }
		UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = User.findUsersByEmailAddress(userDetails.getUsername()).getSingleResult();
        StringBuilder queryBuilder = new StringBuilder("SELECT o FROM Necessity AS o WHERE LOWER(o.description) LIKE LOWER(:description) OR ((o.creator.class=Person AND (LOWER(o.creator.firstName) LIKE LOWER(:participantFilter) OR LOWER(o.creator.lastName) LIKE LOWER(:participantFilter))) OR (o.creator.class=Institution AND LOWER(o.creator.socialReason) LIKE LOWER(:participantFilter))) OR o.donationType = :donationType OR o.priorityType = :priorityType OR o.date BETWEEN :minDate AND :maxDate AND o.creator.user.enabled=1 AND o.creator.user!=:user");
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            queryBuilder.append(" ORDER BY ").append(sortFieldName);
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder))	queryBuilder.append(" ").append(sortOrder);
        }
        TypedQuery<Necessity> q = em.createQuery(queryBuilder.toString(), Necessity.class);
        q.setParameter("description", description);
        q.setParameter("participantFilter", participantFilter);
        q.setParameter("donationType", donationType);
        q.setParameter("priorityType", priorityType);
        q.setParameter("minDate", minDate);
        q.setParameter("maxDate", maxDate);
        q.setParameter("user", user); 
        return q;
    }

	/**
	 * Count find necessitys by description like or donation type or priority type or date between.
	 *
	 * @param description the description
	 * @param participantFilter the participant filter
	 * @param donationType the donation type
	 * @param priorityType the priority type
	 * @param minDate the min date
	 * @param maxDate the max date
	 * @return the long
	 */
	public static Long countFindNecessitysByDescriptionLikeOrParticipantLikeOrDonationTypeOrPriorityTypeOrDateBetween(String description, String participantFilter, DonationTypeEnum donationType, PriorityTypeEnum priorityType, Date minDate, Date maxDate) {
		EntityManager em = Necessity.entityManager();
		if (description.length() > 0){
	        description = description.replace('*', '%');
	        if (description.charAt(0) != '%')	description = "%" + description;
	        if (description.charAt(description.length() - 1) != '%')	description = description + "%";	       
        }
		if (participantFilter.length() > 0){
			participantFilter = participantFilter.replace('*', '%');
	        if (participantFilter.charAt(0) != '%')	participantFilter = "%" + participantFilter;
	        if (participantFilter.charAt(participantFilter.length() - 1) != '%')	participantFilter = participantFilter + "%";	       
        }
		UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = User.findUsersByEmailAddress(userDetails.getUsername()).getSingleResult();
        TypedQuery<Long> q = em.createQuery("SELECT COUNT(o) FROM Necessity AS o WHERE LOWER(o.description) LIKE LOWER(:description) OR ((o.creator.class=Person AND (LOWER(o.creator.firstName) LIKE LOWER(:participantFilter) OR LOWER(o.creator.lastName) LIKE LOWER(:participantFilter))) OR (o.creator.class=Institution AND LOWER(o.creator.socialReason) LIKE LOWER(:participantFilter))) OR o.donationType = :donationType OR o.priorityType = :priorityType OR o.date BETWEEN :minDate AND :maxDate  AND o.creator.user.enabled=1 AND o.creator.user!=:user", Long.class);
        q.setParameter("description", description);
        q.setParameter("participantFilter", participantFilter);
        q.setParameter("donationType", donationType);
        q.setParameter("priorityType", priorityType);
        q.setParameter("minDate", minDate);
        q.setParameter("maxDate", maxDate);
        q.setParameter("user", user);
       
        return ((Long) q.getSingleResult());
    }
	
	/**
	 * Find all necessitys distinct user.
	 *
	 * @param sortFieldName the sort field name
	 * @param sortOrder the sort order
	 * @return the list
	 */
	public static List<Necessity> findAllNecessitysDistinctUser(String sortFieldName, String sortOrder) {
        EntityManager em = Necessity.entityManager();
        String query = "SELECT o FROM Necessity AS o";
        UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = User.findUsersByEmailAddress(userDetails.getUsername()).getSingleResult();
        StringBuilder queryBuilder = new StringBuilder(query+" WHERE o.creator.user.enabled=1 AND o.creator.user!=:user");
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            queryBuilder.append(" ORDER BY ").append(sortFieldName);
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder))	queryBuilder.append(" ").append(sortOrder);
        }
        TypedQuery<Necessity> q = em.createQuery(queryBuilder.toString(), Necessity.class);
        q.setParameter("user", user);
        return q.getResultList();
    }
}
