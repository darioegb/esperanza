// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.roo.esperanza.domain;

import com.roo.esperanza.domain.Conversation;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

privileged aspect Conversation_Roo_Jpa_ActiveRecord {
    
    @PersistenceContext
    transient EntityManager Conversation.entityManager;
    
    public static final List<String> Conversation.fieldNames4OrderClauseFilter = java.util.Arrays.asList("sender", "receiver");
    
    public static final EntityManager Conversation.entityManager() {
        EntityManager em = new Conversation().entityManager;
        if (em == null) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
        return em;
    }
    
    public static long Conversation.countConversations() {
        return entityManager().createQuery("SELECT COUNT(o) FROM Conversation o", Long.class).getSingleResult();
    }
    
    public static List<Conversation> Conversation.findAllConversations() {
        return entityManager().createQuery("SELECT o FROM Conversation o", Conversation.class).getResultList();
    }
    
    public static List<Conversation> Conversation.findAllConversations(String sortFieldName, String sortOrder) {
        String jpaQuery = "SELECT o FROM Conversation o";
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            jpaQuery = jpaQuery + " ORDER BY " + sortFieldName;
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder)) {
                jpaQuery = jpaQuery + " " + sortOrder;
            }
        }
        return entityManager().createQuery(jpaQuery, Conversation.class).getResultList();
    }
    
    public static Conversation Conversation.findConversation(Long id) {
        if (id == null) return null;
        return entityManager().find(Conversation.class, id);
    }
    
    public static List<Conversation> Conversation.findConversationEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM Conversation o", Conversation.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    public static List<Conversation> Conversation.findConversationEntries(int firstResult, int maxResults, String sortFieldName, String sortOrder) {
        String jpaQuery = "SELECT o FROM Conversation o";
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            jpaQuery = jpaQuery + " ORDER BY " + sortFieldName;
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder)) {
                jpaQuery = jpaQuery + " " + sortOrder;
            }
        }
        return entityManager().createQuery(jpaQuery, Conversation.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    @Transactional
    public void Conversation.persist() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.persist(this);
    }
    
    @Transactional
    public void Conversation.remove() {
        if (this.entityManager == null) this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            Conversation attached = Conversation.findConversation(this.id);
            this.entityManager.remove(attached);
        }
    }
    
    @Transactional
    public void Conversation.flush() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.flush();
    }
    
    @Transactional
    public void Conversation.clear() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.clear();
    }
    
    @Transactional
    public Conversation Conversation.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        Conversation merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
}
