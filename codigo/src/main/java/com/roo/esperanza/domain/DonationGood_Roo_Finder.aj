// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.roo.esperanza.domain;

import com.roo.esperanza.domain.DonationGood;
import com.roo.esperanza.reference.DonationTypeEnum;
import java.util.Date;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

privileged aspect DonationGood_Roo_Finder {
    
    public static TypedQuery<DonationGood> DonationGood.findDonationGoodsByDescriptionLikeOrDonationTypeOrDateBetween(String description, DonationTypeEnum donationType, Date minDate, Date maxDate) {
        if (description == null || description.length() == 0) throw new IllegalArgumentException("The description argument is required");
        description = description.replace('*', '%');
        if (description.charAt(0) != '%') {
            description = "%" + description;
        }
        if (description.charAt(description.length() - 1) != '%') {
            description = description + "%";
        }
        if (donationType == null) throw new IllegalArgumentException("The donationType argument is required");
        if (minDate == null) throw new IllegalArgumentException("The minDate argument is required");
        if (maxDate == null) throw new IllegalArgumentException("The maxDate argument is required");
        EntityManager em = DonationGood.entityManager();
        TypedQuery<DonationGood> q = em.createQuery("SELECT o FROM DonationGood AS o WHERE LOWER(o.description) LIKE LOWER(:description)  OR o.donationType = :donationType OR o.date BETWEEN :minDate AND :maxDate", DonationGood.class);
        q.setParameter("description", description);
        q.setParameter("donationType", donationType);
        q.setParameter("minDate", minDate);
        q.setParameter("maxDate", maxDate);
        return q;
    }
    
}
