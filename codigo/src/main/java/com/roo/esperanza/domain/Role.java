package com.roo.esperanza.domain;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import com.roo.esperanza.reference.RoleTypeEnum;

// TODO: Auto-generated Javadoc
/**
 * The Class Role.
 */
@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findRolesByRoleType" })
public class Role {

    /** The role type. */
    @NotNull
    @Enumerated(EnumType.STRING)
    private RoleTypeEnum roleType;

    /** The role description. */
    @NotNull
    @Size(max = 200)
    private String roleDescription;
}
