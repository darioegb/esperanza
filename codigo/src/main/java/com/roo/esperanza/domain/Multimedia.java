package com.roo.esperanza.domain;
import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

import com.roo.esperanza.reference.MultimediaTypeEnum;

// TODO: Auto-generated Javadoc
/**
 * The Class Multimedia.
 */
@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class Multimedia {

    /** The path. */
    @Column(name = "path")
    private String path;

    /** The description. */
    @NotNull
    @Column(name = "description")
    @Size(min = 10, max = 250)
    private String description;

    /** The multimedia type. */
    @NotNull
    @Enumerated(EnumType.STRING)
    private MultimediaTypeEnum multimediaType;
    
    /**
     * Find multimedias by multimedia type and institution.
     *
     * @param multimediaType the multimedia type
     * @param id the id
     * @return the typed query
     */
    public static TypedQuery<Multimedia> findMultimediasByMultimediaTypeAndInstitution(MultimediaTypeEnum multimediaType, Long id) {
        if (multimediaType == null) throw new IllegalArgumentException("The multimediaType argument is required");
        EntityManager em = Multimedia.entityManager();
        TypedQuery<Multimedia> q = em.createQuery("SELECT m FROM Institution AS i LEFT JOIN i.arrayMutimedia AS m WHERE m.multimediaType = :multimediaType AND i.id = :id", Multimedia.class);
        q.setParameter("multimediaType", multimediaType);
        q.setParameter("id", id);
        return q;
    }
}
