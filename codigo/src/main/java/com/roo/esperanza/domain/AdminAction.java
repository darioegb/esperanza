package com.roo.esperanza.domain;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.OneToMany;
import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.springframework.format.annotation.DateTimeFormat;

// TODO: Auto-generated Javadoc
/**
 * The Class AdminAction.
 */
@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class AdminAction {

    /** The denoucements. */
    @OneToMany(cascade = CascadeType.ALL)
    private Set<User> denoucements = new HashSet<User>();

    /** The reason. */
    @NotNull
    @Column(name = "reason")
    @Size(min = 20, max = 200)
    private String reason;

    /** The date. */
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Date date;

    /** The status. */
    @NotNull
    private Boolean status;
}
