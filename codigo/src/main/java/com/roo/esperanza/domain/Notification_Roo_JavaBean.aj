// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.roo.esperanza.domain;

import com.roo.esperanza.domain.Notification;
import com.roo.esperanza.domain.User;
import com.roo.esperanza.reference.NotificationTypeEnum;
import java.util.Date;

privileged aspect Notification_Roo_JavaBean {
    
    public User Notification.getUser() {
        return this.user;
    }
    
    public void Notification.setUser(User user) {
        this.user = user;
    }
    
    public Date Notification.getDate() {
        return this.date;
    }
    
    public void Notification.setDate(Date date) {
        this.date = date;
    }
    
    public Boolean Notification.getStatus() {
        return this.status;
    }
    
    public void Notification.setStatus(Boolean status) {
        this.status = status;
    }
    
    public NotificationTypeEnum Notification.getNotificationType() {
        return this.notificationType;
    }
    
    public void Notification.setNotificationType(NotificationTypeEnum notificationType) {
        this.notificationType = notificationType;
    }
    
    public String Notification.getDescription() {
        return this.description;
    }
    
    public void Notification.setDescription(String description) {
        this.description = description;
    }
    
}
