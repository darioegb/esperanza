package com.roo.esperanza.domain;

import java.util.Date;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import com.roo.esperanza.reference.GenreTypeEnum;

// TODO: Auto-generated Javadoc
/**
 * The Class PersonRegistrationForm.
 */
public class PersonRegistrationForm {
	
	/** The email address. */
	private String emailAddress;

	/** The first name. */
    @NotNull
    @Size(min = 3, max=65)
    private String firstName;
    
    /** The last name. */
    @NotNull
    @Size(min = 3, max=65)
    private String lastName;
    
    /** The personal description. */
    @NotNull
    @Size(min = 20, max = 100)
    private String personalDescription;
    
    /** The genre type. */
    @NotNull
    @Enumerated(EnumType.STRING)
    private GenreTypeEnum genreType;

    /** The date. */
    @DateTimeFormat(pattern="dd/MM/yyyy")
    private Date date;

    /** The identification. */
    private Integer identification;
	
    /** The phone. */
    private Integer phone;
    
	/** The cell phone. */
    private Integer cellPhone;
        
    /** The city. */
    @NotNull
    private String city;
    
    /** The address. */
    @NotNull
    private String address;
    
    /** The province. */
    @NotNull
    private  Province province;
    
    /** The country. */
    @NotNull
    private  Country country;
    
    /** The postal code. */
    @NotNull
    private String postalCode;
    
	/**
	 * Gets the email address.
	 *
	 * @return the email address
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * Sets the email address.
	 *
	 * @param emailAddress the new email address
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
    
    /**
     * Gets the first name.
     *
     * @return the first name
     */
    public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets the first name.
	 *
	 * @param firstName the new first name
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Gets the last name.
	 *
	 * @return the last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets the last name.
	 *
	 * @param lastName the new last name
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Gets the personal description.
	 *
	 * @return the personal description
	 */
	public String getPersonalDescription() {
		return personalDescription;
	}

	/**
	 * Sets the personal description.
	 *
	 * @param personalDescription the new personal description
	 */
	public void setPersonalDescription(String personalDescription) {
		this.personalDescription = personalDescription;
	}

	/**
	 * Gets the genre type.
	 *
	 * @return the genre type
	 */
	public GenreTypeEnum getGenreType() {
		return genreType;
	}

	/**
	 * Sets the genre type.
	 *
	 * @param genreType the new genre type
	 */
	public void setGenreType(GenreTypeEnum genreType) {
		this.genreType = genreType;
	}

	/**
	 * Gets the date.
	 *
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * Sets the date.
	 *
	 * @param date the new date
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * Gets the identification.
	 *
	 * @return the identification
	 */
	public Integer getIdentification() {
		return identification;
	}

	/**
	 * Sets the identification.
	 *
	 * @param identification the new identification
	 */
	public void setIdentification(Integer identification) {
		this.identification = identification;
	}

	/**
	 * Gets the phone.
	 *
	 * @return the phone
	 */
	public Integer getPhone() {
		return phone;
	}

	/**
	 * Sets the phone.
	 *
	 * @param phone the new phone
	 */
	public void setPhone(Integer phone) {
		this.phone = phone;
	}

	/**
	 * Gets the cell phone.
	 *
	 * @return the cell phone
	 */
	public Integer getCellPhone() {
		return cellPhone;
	}

	/**
	 * Sets the cell phone.
	 *
	 * @param cellPhone the new cell phone
	 */
	public void setCellPhone(Integer cellPhone) {
		this.cellPhone = cellPhone;
	}

	/**
	 * Gets the city.
	 *
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Sets the city.
	 *
	 * @param city the new city
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * Gets the address.
	 *
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Sets the address.
	 *
	 * @param address the new address
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Gets the province.
	 *
	 * @return the province
	 */
	public Province getProvince() {
		return province;
	}

	/**
	 * Sets the province.
	 *
	 * @param province the new province
	 */
	public void setProvince(Province province) {
		this.province = province;
	}

	/**
	 * Gets the country.
	 *
	 * @return the country
	 */
	public Country getCountry() {
		return country;
	}

	/**
	 * Sets the country.
	 *
	 * @param country the new country
	 */
	public void setCountry(Country country) {
		this.country = country;
	}

	/**
	 * Gets the postal code.
	 *
	 * @return the postal code
	 */
	public String getPostalCode() {
		return postalCode;
	}

	/**
	 * Sets the postal code.
	 *
	 * @param postalCode the new postal code
	 */
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

}
