// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.roo.esperanza.domain;

import com.roo.esperanza.domain.Multimedia;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

privileged aspect Multimedia_Roo_Jpa_ActiveRecord {
    
    @PersistenceContext
    transient EntityManager Multimedia.entityManager;
    
    public static final List<String> Multimedia.fieldNames4OrderClauseFilter = java.util.Arrays.asList("path", "description", "multimediaType");
    
    public static final EntityManager Multimedia.entityManager() {
        EntityManager em = new Multimedia().entityManager;
        if (em == null) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
        return em;
    }
    
    public static long Multimedia.countMultimedias() {
        return entityManager().createQuery("SELECT COUNT(o) FROM Multimedia o", Long.class).getSingleResult();
    }
    
    public static List<Multimedia> Multimedia.findAllMultimedias() {
        return entityManager().createQuery("SELECT o FROM Multimedia o", Multimedia.class).getResultList();
    }
    
    public static List<Multimedia> Multimedia.findAllMultimedias(String sortFieldName, String sortOrder) {
        String jpaQuery = "SELECT o FROM Multimedia o";
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            jpaQuery = jpaQuery + " ORDER BY " + sortFieldName;
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder)) {
                jpaQuery = jpaQuery + " " + sortOrder;
            }
        }
        return entityManager().createQuery(jpaQuery, Multimedia.class).getResultList();
    }
    
    public static Multimedia Multimedia.findMultimedia(Long id) {
        if (id == null) return null;
        return entityManager().find(Multimedia.class, id);
    }
    
    public static List<Multimedia> Multimedia.findMultimediaEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM Multimedia o", Multimedia.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    public static List<Multimedia> Multimedia.findMultimediaEntries(int firstResult, int maxResults, String sortFieldName, String sortOrder) {
        String jpaQuery = "SELECT o FROM Multimedia o";
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            jpaQuery = jpaQuery + " ORDER BY " + sortFieldName;
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder)) {
                jpaQuery = jpaQuery + " " + sortOrder;
            }
        }
        return entityManager().createQuery(jpaQuery, Multimedia.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    @Transactional
    public void Multimedia.persist() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.persist(this);
    }
    
    @Transactional
    public void Multimedia.remove() {
        if (this.entityManager == null) this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            Multimedia attached = Multimedia.findMultimedia(this.id);
            this.entityManager.remove(attached);
        }
    }
    
    @Transactional
    public void Multimedia.flush() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.flush();
    }
    
    @Transactional
    public void Multimedia.clear() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.clear();
    }
    
    @Transactional
    public Multimedia Multimedia.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        Multimedia merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
}
