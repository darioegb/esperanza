package com.roo.esperanza.domain;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Size;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import org.springframework.roo.addon.json.RooJson;

// TODO: Auto-generated Javadoc
/**
 * The Class Province.
 */
@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findProvincesByCountry" })
@RooJson
public class Province {

    /** The name. */
    @Column(name = "name")
    @Size(min = 10, max = 65)
    private String name;

    /** The country. */
    @ManyToOne
    private Country country;
}
