package com.roo.esperanza.domain;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import com.roo.esperanza.reference.SuscriptionTypeEnum;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.springframework.format.annotation.DateTimeFormat;
import javax.persistence.ManyToOne;

// TODO: Auto-generated Javadoc
/**
 * The Class Suscription.
 */
@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findSuscriptionsByStateNot" })
public class Suscription {

    /** The type. */
    @NotNull
    @Enumerated(EnumType.STRING)
    private SuscriptionTypeEnum type;

    /** The state. */
    @NotNull
    private Boolean state;

    /** The date. */
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Date date;

    /** The amount. */
    @NotNull
    @Column(name = "amount")
    private Float amount;

    /** The suscriptor. */
    @ManyToOne
    private Person suscriptor;

    /** The receiver. */
    @ManyToOne
    private Institution receiver;
}
