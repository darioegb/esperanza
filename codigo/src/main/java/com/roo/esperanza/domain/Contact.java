package com.roo.esperanza.domain;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;

// TODO: Auto-generated Javadoc
/**
 * The Class Contact.
 */
public class Contact {

	/** The name. */
	@Size(min=3, max=65)
    private String name;

    /** The email address. */
    @Email
    @Size(min=3, max=65)
    private String emailAddress;

    /** The description. */
    @Size(min = 10, max=200)
    private String description;

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the email address.
	 *
	 * @return the email address
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * Sets the email address.
	 *
	 * @param emailAddress the new email address
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
}
