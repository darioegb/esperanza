package com.roo.esperanza.domain;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import javax.persistence.ManyToOne;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import org.springframework.format.annotation.DateTimeFormat;
import javax.validation.constraints.NotNull;
import com.roo.esperanza.reference.NotificationTypeEnum;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Size;

// TODO: Auto-generated Javadoc
/**
 * The Class Notification.
 */
@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class Notification {

    /** The user. */
    @ManyToOne
    private User user;

    /** The date. */
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Date date;

    /** The status. */
    @NotNull
    private Boolean status;

    /** The notification type. */
    @NotNull
    @Enumerated(EnumType.STRING)
    private NotificationTypeEnum notificationType;

    /** The description. */
    @NotNull
    @Column(name = "description")
    @Size(min = 20, max = 250)
    private String description;

    /**
     * Count find notifications by status not and user.
     *
     * @param user the user
     * @return the long
     */
    public static Long countFindNotificationsByStatusNotAndUser(User user) {
        EntityManager em = Notification.entityManager();
        TypedQuery<Long> q = em.createQuery("SELECT COUNT(o) FROM Notification AS o WHERE o.status=false AND o.user=:user AND o.notificationType='DonationReceived'", Long.class);
        q.setParameter("user", user);
        return ((Long) q.getSingleResult());
    }
    
    /**
     * Count find notifications by notification type and user.
     *
     * @param notificationType the notification type
     * @param user the user
     * @return the long
     */
    public static Long countFindNotificationsByNotificationTypeAndUser(NotificationTypeEnum notificationType, User user) {
        EntityManager em = Notification.entityManager();
        TypedQuery<Long> q = em.createQuery("SELECT COUNT(o) FROM Notification AS o WHERE o.notificationType = :notificationType AND o.user=:user", Long.class);
        q.setParameter("notificationType", notificationType);
        q.setParameter("user", user);
        return ((Long) q.getSingleResult());
    }

    /**
     * Find notifications by notification type and user.
     *
     * @param notificationType the notification type
     * @param user the user
     * @return the typed query
     */
    public static TypedQuery<Notification> findNotificationsByNotificationTypeAndUser(NotificationTypeEnum notificationType, User user) {
        EntityManager em = Notification.entityManager();
        TypedQuery<Notification> q = em.createQuery("SELECT o FROM Notification AS o WHERE o.notificationType = :notificationType AND o.user=:user", Notification.class);
        q.setParameter("notificationType", notificationType);
        q.setParameter("user", user);
        return q;
    }

    /**
     * Find notifications by notification type and user.
     *
     * @param notificationType the notification type
     * @param user the user
     * @param sortFieldName the sort field name
     * @param sortOrder the sort order
     * @return the typed query
     */
    public static TypedQuery<Notification> findNotificationsByNotificationTypeAndUser(NotificationTypeEnum notificationType, User user, String sortFieldName, String sortOrder) {
        EntityManager em = Notification.entityManager();
        StringBuilder queryBuilder = new StringBuilder("SELECT o FROM Notification AS o WHERE o.notificationType = :notificationType AND o.user=:user");
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            queryBuilder.append(" ORDER BY ").append(sortFieldName);
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder)) {
                queryBuilder.append(" ").append(sortOrder);
            }
        }
        TypedQuery<Notification> q = em.createQuery(queryBuilder.toString(), Notification.class);
        q.setParameter("notificationType", notificationType);
        q.setParameter("user", user);
        return q;
    }
    
    /**
     * Find all notifications by user.
     *
     * @param user the user
     * @param sortFieldName the sort field name
     * @param sortOrder the sort order
     * @return the list
     */
    public static List<Notification> findAllNotificationsByUser(User user, String sortFieldName, String sortOrder) {
        EntityManager em = Donation.entityManager();
        StringBuilder queryBuilder = new StringBuilder("SELECT o FROM Notification AS o WHERE o.user=:user");
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            queryBuilder.append(" ORDER BY ").append(sortFieldName);
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder))	queryBuilder.append(" " + sortOrder);
        }
        TypedQuery<Notification> q = em.createQuery(queryBuilder.toString(), Notification.class);
        q.setParameter("user", user);
        return q.getResultList();
    }
}
