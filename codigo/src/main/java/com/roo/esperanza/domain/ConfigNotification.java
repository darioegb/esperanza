package com.roo.esperanza.domain;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import javax.validation.constraints.NotNull;

// TODO: Auto-generated Javadoc
/**
 * The Class ConfigNotification.
 */
@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class ConfigNotification {

    /** The donation goods. */
    private Boolean donationGoods;
    
    /** The receive mails. */
    @NotNull
    private Boolean receiveMails;

    /** The donation received. */
    private Boolean donationReceived;

    /** The statistics. */
    private Boolean statistics;

    /** The publicaction status. */
    private Boolean publicactionStatus;

    /** The state complaint filed. */
    private Boolean stateComplaintFiled;

    /** The complaint received. */
    private Boolean complaintReceived;
    
	/** The publications. */
	private Boolean publications;

    /** The denouncements. */
    private Boolean denouncements;

    /** The suscriptions. */
    private Boolean suscriptions;

    /** The needs. */
    private Boolean needs;

    /** The donation request. */
    private Boolean donationRequest;

    /** The thanks to donation. */
    private Boolean thanksToDonation;

    /** The claim donation. */
    private Boolean claimDonation;
}
