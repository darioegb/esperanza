// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.roo.esperanza.domain;

import com.roo.esperanza.domain.DonationGood;
import com.roo.esperanza.domain.Person;
import com.roo.esperanza.domain.Suscription;
import com.roo.esperanza.reference.GenreTypeEnum;
import java.util.Set;

privileged aspect Person_Roo_JavaBean {
    
    public String Person.getFirstName() {
        return this.firstName;
    }
    
    public void Person.setFirstName(String firstName) {
        this.firstName = firstName;
    }
    
    public String Person.getLastName() {
        return this.lastName;
    }
    
    public void Person.setLastName(String lastName) {
        this.lastName = lastName;
    }
    
    public String Person.getPersonalDescription() {
        return this.personalDescription;
    }
    
    public void Person.setPersonalDescription(String personalDescription) {
        this.personalDescription = personalDescription;
    }
    
    public GenreTypeEnum Person.getGenreType() {
        return this.genreType;
    }
    
    public void Person.setGenreType(GenreTypeEnum genreType) {
        this.genreType = genreType;
    }
    
    public Integer Person.getIdentification() {
        return this.identification;
    }
    
    public void Person.setIdentification(Integer identification) {
        this.identification = identification;
    }
    
    public Set<Suscription> Person.getSuscriptions() {
        return this.suscriptions;
    }
    
    public void Person.setSuscriptions(Set<Suscription> suscriptions) {
        this.suscriptions = suscriptions;
    }
    
    public Set<DonationGood> Person.getDonationGoods() {
        return this.donationGoods;
    }
    
    public void Person.setDonationGoods(Set<DonationGood> donationGoods) {
        this.donationGoods = donationGoods;
    }
    
}
