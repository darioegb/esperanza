package com.roo.esperanza.domain;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

// TODO: Auto-generated Javadoc
/**
 * The Class Institution.
 */
@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findInstitutionsByNecessitysOrCategory", "findInstitutionsByUser" })
public class Institution extends Participant {

    /** The social reason. */
    @NotNull
    @Column(name = "social_reason")
    @Size(min = 3)
    private String socialReason;

    /** The cbu. */
    @NotNull
    @Column(name = "cbu")
    private Integer cbu;

    /** The short description. */
    @NotNull
    @Column(name = "short_description")
    @Size(min = 30, max = 100)
    private String shortDescription;

    /** The long description. */
    @NotNull
    @Column(name = "long_description")
    @Size(min = 100, max = 1000)
    private String longDescription;

    /** The array mutimedia. */
    @OneToMany(cascade = CascadeType.ALL)
    private Set<Multimedia> arrayMutimedia = new HashSet<Multimedia>();

    /** The category. */
    @ManyToOne
    private Category category;
    
    /**
     * Count find institutions by necessity description or category.
     *
     * @param description the description
     * @param category the category
     * @return the long
     */
    public static Long countFindInstitutionsByNecessityDescriptionOrCategory(String description, Category category) {
        EntityManager em = Institution.entityManager();
        if(description.length() > 0){
        	description = description.replace('*', '%');
            if (description.charAt(0) != '%')	description = "%" + description;
            if (description.charAt(description.length() - 1) != '%')	description = description + "%";    
        }
        String query = "SELECT COUNT(i) FROM Institution AS i";
        if(description.length() > 0 && category == null)	query += " LEFT JOIN i.necessitys AS n WHERE LOWER(n.description) LIKE LOWER(:description)";
        else if(category != null && description.length() == 0)	query += " WHERE LOWER(i.category) LIKE LOWER(:category)";
        else query += " LEFT JOIN i.necessitys AS n WHERE LOWER(i.category) LIKE LOWER(:category) OR LOWER(n.description) LIKE LOWER(:description)";
        UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = User.findUsersByEmailAddress(userDetails.getUsername()).getSingleResult();
        StringBuilder queryBuilder = new StringBuilder(query+" AND i.user.enabled=1 AND i.user!=:user");
        TypedQuery<Long> q = em.createQuery(queryBuilder.toString(), Long.class);
        q.setParameter("description", description);
        q.setParameter("user", user);
        if(category != null)	q.setParameter("category", category);
        
        return ((Long) q.getSingleResult());
    }

	/**
	 * Find institutions by necessity description or category.
	 *
	 * @param description the description
	 * @param category the category
	 * @return the typed query
	 */
	public static TypedQuery<Institution> findInstitutionsByNecessityDescriptionOrCategory(String description, Category category) {
        EntityManager em = Institution.entityManager();
        if(description.length() > 0){
        	description = description.replace('*', '%');
            if (description.charAt(0) != '%')	description = "%" + description;
            if (description.charAt(description.length() - 1) != '%')	description = description + "%";    
        }
        String query = "SELECT i FROM Institution AS i";
        if(description.length() > 0 && category == null)	query += " LEFT JOIN i.necessitys AS n WHERE LOWER(n.description) LIKE LOWER(:description)";
        else query += " LEFT JOIN i.necessitys AS n WHERE LOWER(i.category) LIKE LOWER(:category) OR LOWER(n.description) LIKE LOWER(:description)";
        UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = User.findUsersByEmailAddress(userDetails.getUsername()).getSingleResult();
        StringBuilder queryBuilder = new StringBuilder(query+" AND i.user.enabled=1 AND i.user!=:user");
        TypedQuery<Institution> q = em.createQuery(queryBuilder.toString(), Institution.class);
        q.setParameter("description", description);
        q.setParameter("user", user);
        if(category != null)	q.setParameter("category", category);
        return q;
    }

	/**
	 * Find institutions by necessity description or category.
	 *
	 * @param description the description
	 * @param category the category
	 * @param sortFieldName the sort field name
	 * @param sortOrder the sort order
	 * @return the typed query
	 */
	public static TypedQuery<Institution> findInstitutionsByNecessityDescriptionOrCategory(String description, Category category, String sortFieldName, String sortOrder) {
        EntityManager em = Institution.entityManager();
        if(description.length() > 0){
        	description = description.replace('*', '%');
            if (description.charAt(0) != '%')	description = "%" + description;
            if (description.charAt(description.length() - 1) != '%')	description = description + "%";    
        }
        String query = "SELECT i FROM Institution AS i";
        if(description.length() > 0 && category == null)	query += " LEFT JOIN i.necessitys AS n WHERE LOWER(n.description) LIKE LOWER(:description)";
        else query += " LEFT JOIN i.necessitys AS n WHERE LOWER(i.category) LIKE LOWER(:category) OR LOWER(n.description) LIKE LOWER(:description)";
        UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = User.findUsersByEmailAddress(userDetails.getUsername()).getSingleResult();
        StringBuilder queryBuilder = new StringBuilder(query+" AND i.user.enabled=1 AND i.user!=:user");
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            queryBuilder.append(" ORDER BY ").append(sortFieldName);
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder))	queryBuilder.append(" " + sortOrder);
        }
        TypedQuery<Institution> q = em.createQuery(queryBuilder.toString(), Institution.class);
        q.setParameter("description", description);
        q.setParameter("user", user);
        if(category != null)	q.setParameter("category", category);
        return q;
    }
	
	/**
	 * Find all institutions distinct user.
	 *
	 * @param sortFieldName the sort field name
	 * @param sortOrder the sort order
	 * @return the list
	 */
	public static List<Institution> findAllInstitutionsDistinctUser(String sortFieldName, String sortOrder) {
        EntityManager em = Institution.entityManager();
        String query = "SELECT i FROM Institution AS i";
        UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = User.findUsersByEmailAddress(userDetails.getUsername()).getSingleResult();
        StringBuilder queryBuilder = new StringBuilder(query+" WHERE i.user.enabled=1 AND i.user!=:user");
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            queryBuilder.append(" ORDER BY ").append(sortFieldName);
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder))	queryBuilder.append(" " + sortOrder);
        }
        TypedQuery<Institution> q = em.createQuery(queryBuilder.toString(), Institution.class);
        q.setParameter("user", user);
        return q.getResultList();
    }
}
