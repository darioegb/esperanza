package com.roo.esperanza.domain;

import javax.validation.constraints.NotNull;

// TODO: Auto-generated Javadoc
/**
 * The Class ProfileImageDataUpdateForm.
 */
public class ProfileImageDataUpdateForm {

	/** The id user. */
	@NotNull
    private Long idUser;
	
	/** The old name. */
	@NotNull
    private String oldName;

    /**
     * Gets the id user.
     *
     * @return the id user
     */
    public Long getIdUser() {
		return idUser;
	}

	/**
	 * Sets the id user.
	 *
	 * @param idUser the new id user
	 */
	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}
    
	/**
	 * Gets the old name.
	 *
	 * @return the old name
	 */
	public String getOldName() {
		return oldName;
	}

	/**
	 * Sets the old name.
	 *
	 * @param oldName the new old name
	 */
	public void setOldName(String oldName) {
		this.oldName = oldName;
	}
}
