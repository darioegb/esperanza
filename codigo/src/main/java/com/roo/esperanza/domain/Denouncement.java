package com.roo.esperanza.domain;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.springframework.format.annotation.DateTimeFormat;
import com.roo.esperanza.reference.StatusTypeEnum;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.Query;

// TODO: Auto-generated Javadoc
/**
 * The Class Denouncement.
 */
@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class Denouncement {

    /** The reason. */
    @NotNull
    @Column(name = "reason")
    @Size(min = 10, max = 200)
    private String reason;

    /** The date. */
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Date date;

    /** The status type. */
    @NotNull
    @Enumerated(EnumType.STRING)
    private StatusTypeEnum statusType;

    /** The complainant. */
    @ManyToOne
    private User complainant;

    /** The denouced. */
    @ManyToOne
    private User denouced;

    /** The admin resolution. */
    @ManyToOne
    private AdminAction adminResolution;
    
    /**
     * Find denouncement by hash.
     *
     * @param hash the hash
     * @return the denouncement
     */
    public static Denouncement findDenouncementByHash(String hash) {
        EntityManager em = Denouncement.entityManager();
        Query q = em.createNativeQuery("SELECT * FROM denouncement AS d WHERE SHA2(d.id,256)=:hash", Denouncement.class);
        q.setParameter("hash", hash);
        return (Denouncement) q.getSingleResult();
    }
}
