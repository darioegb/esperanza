package com.roo.esperanza.domain;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

import com.roo.esperanza.reference.ScoreTypeEnum;

// TODO: Auto-generated Javadoc
/**
 * The Class Puntuation.
 */
@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class Puntuation {

    /** The score type. */
    @NotNull
    @Enumerated(EnumType.STRING)
    private ScoreTypeEnum scoreType;

    /** The comment. */
    @NotNull
    @Column(name = "comment")
    @Size(min = 10, max = 250)
    private String comment;

    /** The date. */
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Date date;

	/**
	 * Find puntuation entries.
	 *
	 * @param participant the participant
	 * @param firstResult the first result
	 * @param maxResults the max results
	 * @param sortFieldName the sort field name
	 * @param sortOrder the sort order
	 * @return the list
	 */
	public static List<Puntuation> findPuntuationEntries(Participant participant, int firstResult, int maxResults, String sortFieldName, String sortOrder) {
		StringBuilder queryBuilder = new StringBuilder("SELECT a FROM Participant o LEFT JOIN o.arrayScore as a WHERE o = :participant");
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
        	queryBuilder.append(" ORDER BY "+sortFieldName);
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder))	queryBuilder.append(" "+sortOrder);
        }
        TypedQuery<Puntuation> q =  entityManager().createQuery(queryBuilder.toString(), Puntuation.class);
        q.setParameter("participant", participant);
        return q.setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }

	/**
	 * Find all puntuations.
	 *
	 * @param participant the participant
	 * @param sortFieldName the sort field name
	 * @param sortOrder the sort order
	 * @return the list
	 */
	public static List<Puntuation> findAllPuntuations(Participant participant, String sortFieldName, String sortOrder) {
		StringBuilder queryBuilder = new StringBuilder("SELECT a FROM Participant o LEFT JOIN o.arrayScore as a WHERE o = :participant");
		if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
        	queryBuilder.append(" ORDER BY "+sortFieldName);
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder))	queryBuilder.append(" "+sortOrder);
        }
        TypedQuery<Puntuation> q =  entityManager().createQuery(queryBuilder.toString(), Puntuation.class);
		q.setParameter("participant", participant);
        return q.getResultList();
    }

	/**
	 * Count puntuations.
	 *
	 * @param participant the participant
	 * @return the long
	 */
	public static long countPuntuations(Participant participant) {
		StringBuilder queryBuilder = new StringBuilder("SELECT COUNT(a) FROM Participant o LEFT JOIN o.arrayScore as a WHERE o = :participant");
        TypedQuery<Long> q = entityManager().createQuery(queryBuilder.toString(), Long.class);
        q.setParameter("participant", participant);
        return q.getSingleResult();
    }
}
