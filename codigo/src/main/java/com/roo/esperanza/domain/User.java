package com.roo.esperanza.domain;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EntityManager;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import org.hibernate.validator.constraints.Email;
import org.springframework.format.annotation.DateTimeFormat;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Query;

// TODO: Auto-generated Javadoc
/**
 * The Class User.
 */
@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findUsersByEmailAddress", "findUsersByActivationKeyAndEmailAddress" })
public class User {

    /** The email address. */
    @NotNull
    @Column(unique = true)
    @Size(min = 10)
    @Email
    private String emailAddress;

    /** The password. */
    @NotNull
    @Size(min = 1)
    private String password;

    /** The activation date. */
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Date activationDate;

    /** The activation key. */
    private String activationKey;

    /** The enabled. */
    private Boolean enabled;

    /** The locked. */
    private Boolean locked;

    /** The rol. */
    @ManyToOne
    private Role rol;
    
    /** The config notification. */
    @ManyToOne
    private ConfigNotification configNotification;
    
    /** The notifications. */
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    private Set<Notification> notifications = new HashSet<Notification>();
    
    /**
     * Find all users distinct admin.
     *
     * @param sortFieldName the sort field name
     * @param sortOrder the sort order
     * @return the list
     */
    public static List<User> findAllUsersDistinctAdmin(String sortFieldName, String sortOrder) {
    	EntityManager em = User.entityManager();
        StringBuilder queryBuilder = new StringBuilder("SELECT o FROM User o WHERE o.enabled=1 AND o.rol!=1");
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            queryBuilder.append(" ORDER BY ").append(sortFieldName);
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder))	queryBuilder.append(" " + sortOrder);
        }
        TypedQuery<User> q = em.createQuery(queryBuilder.toString(), User.class);
        return q.getResultList();
    }
    
    /**
     * Find user by hash.
     *
     * @param hash the hash
     * @return the user
     */
    public static User findUserByHash(String hash) {
        EntityManager em = User.entityManager();
        Query q = em.createNativeQuery("SELECT * FROM user AS p WHERE SHA2(p.id,256)=:hash", User.class);
        q.setParameter("hash", hash);
        return (User) q.getSingleResult();
    }
    
    /**
     * Find user entries distinct admin.
     *
     * @param firstResult the first result
     * @param maxResults the max results
     * @param sortFieldName the sort field name
     * @param sortOrder the sort order
     * @return the list
     */
    public static List<User> findUserEntriesDistinctAdmin(int firstResult, int maxResults, String sortFieldName, String sortOrder) {
    	EntityManager em = User.entityManager();
        StringBuilder queryBuilder = new StringBuilder("SELECT o FROM User o WHERE o.enabled=1 AND o.rol!=1");
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            queryBuilder.append(" ORDER BY ").append(sortFieldName);
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder))	queryBuilder.append(" " + sortOrder);
        }
        TypedQuery<User> q = em.createQuery(queryBuilder.toString(), User.class);
        return q.setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
}
