package com.roo.esperanza.domain;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

import javax.persistence.Column;
import javax.validation.constraints.Size;

// TODO: Auto-generated Javadoc
/**
 * The Class Country.
 */
@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class Country {

	/** The code. */
	@Column(name = "code")
	private String code;
	
    /** The name. */
	@Column(name = "name")
    @Size(min = 10, max = 65)
    private String name;
	
}
