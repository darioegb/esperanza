package com.roo.esperanza.domain;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

import com.roo.esperanza.reference.StatusTypeEnum;

import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import org.springframework.format.annotation.DateTimeFormat;
import javax.persistence.ManyToOne;
import javax.persistence.Query;

// TODO: Auto-generated Javadoc
/**
 * The Class DonationRequest.
 */
@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findDonationRequestsByDonationGood" })
public class DonationRequest {

    /** The description. */
    @NotNull
    @Column(name = "description")
    @Size(min = 10, max = 100)
    private String description;

    /** The date. */
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Date date;

    /** The donation good. */
    @ManyToOne
    private DonationGood donationGood;

    /** The statusType. */
    @NotNull
    @Enumerated(EnumType.STRING)
    private StatusTypeEnum statusType;

    /** The applicant. */
    @ManyToOne
    private Participant applicant;

    /**
     * Find donation request by hash.
     *
     * @param hash the hash
     * @return the donation request
     */
    public static DonationRequest findDonationRequestByHash(String hash) {
        EntityManager em = DonationRequest.entityManager();
        Query q = em.createNativeQuery("SELECT * FROM donation_request AS p WHERE SHA2(p.id,256)=:hash", DonationRequest.class);
        q.setParameter("hash", hash);
        return (DonationRequest) q.getSingleResult();
    }

	/**
	 * Count find donation requests by donation good hash.
	 *
	 * @param hash the hash
	 * @return the long
	 */
	public static Long countFindDonationRequestsByDonationGoodHash(String hash) {
        EntityManager em = DonationRequest.entityManager();
        TypedQuery<Long> q = em.createQuery("SELECT COUNT(o) FROM DonationRequest AS o WHERE SHA2(o.donationGood.id,256) = :hash ORDER BY o.id DESC", Long.class);
        q.setParameter("hash", hash);
        return ((Long) q.getSingleResult());
    }

	/**
	 * Find donation requests by donation good hash.
	 *
	 * @param hash the hash
	 * @return the typed query
	 */
	public static TypedQuery<DonationRequest> findDonationRequestsByDonationGoodHash(String hash) {
        EntityManager em = DonationRequest.entityManager();
        TypedQuery<DonationRequest> q = em.createQuery("SELECT o FROM DonationRequest AS o WHERE SHA2(o.donationGood.id,256) = :hash AND o.statusType='Create' ORDER BY o.id DESC", DonationRequest.class);
        q.setParameter("hash", hash);
        return q;
    }
}
