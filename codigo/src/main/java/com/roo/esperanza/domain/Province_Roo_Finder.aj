// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.roo.esperanza.domain;

import com.roo.esperanza.domain.Country;
import com.roo.esperanza.domain.Province;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

privileged aspect Province_Roo_Finder {
    
    public static Long Province.countFindProvincesByCountry(Country country) {
        if (country == null) throw new IllegalArgumentException("The country argument is required");
        EntityManager em = Province.entityManager();
        TypedQuery q = em.createQuery("SELECT COUNT(o) FROM Province AS o WHERE o.country = :country", Long.class);
        q.setParameter("country", country);
        return ((Long) q.getSingleResult());
    }
    
    public static TypedQuery<Province> Province.findProvincesByCountry(Country country) {
        if (country == null) throw new IllegalArgumentException("The country argument is required");
        EntityManager em = Province.entityManager();
        TypedQuery<Province> q = em.createQuery("SELECT o FROM Province AS o WHERE o.country = :country", Province.class);
        q.setParameter("country", country);
        return q;
    }
    
    public static TypedQuery<Province> Province.findProvincesByCountry(Country country, String sortFieldName, String sortOrder) {
        if (country == null) throw new IllegalArgumentException("The country argument is required");
        EntityManager em = Province.entityManager();
        StringBuilder queryBuilder = new StringBuilder("SELECT o FROM Province AS o WHERE o.country = :country");
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            queryBuilder.append(" ORDER BY ").append(sortFieldName);
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder)) {
                queryBuilder.append(" ").append(sortOrder);
            }
        }
        TypedQuery<Province> q = em.createQuery(queryBuilder.toString(), Province.class);
        q.setParameter("country", country);
        return q;
    }
    
}
