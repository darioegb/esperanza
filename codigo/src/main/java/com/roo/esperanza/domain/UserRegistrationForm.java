/**
 * 
 */
package com.roo.esperanza.domain;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.roo.esperanza.reference.RoleTypeEnum;

import net.tanesha.recaptcha.ReCaptcha;
import net.tanesha.recaptcha.ReCaptchaFactory;
import net.tanesha.recaptcha.ReCaptchaResponse;

// TODO: Auto-generated Javadoc
/**
 * The Class UserRegistrationForm.
 *
 * @author rohit
 */
public class UserRegistrationForm {

	/** The email address. */
	@NotNull
	@Size(min = 10, max = 65)
	private String emailAddress;
	
	/** The password_confirmation. */
	@NotNull
	@Size(min = 3, max = 15)
	private String password_confirmation;
	
	/** The password. */
	@NotNull
	@Size(min = 3, max = 15)
	private String password;
	
	/** The role type. */
	@NotNull
    @Enumerated(EnumType.STRING)
    private RoleTypeEnum roleType;

	/** The recaptcha_challenge_field. */
	private String recaptcha_challenge_field;

	/** The recaptcha_response_field. */
	private String recaptcha_response_field;
	
	/** The re catcpha. */
	private ReCaptcha reCatcpha = ReCaptchaFactory.newReCaptcha("6LdfmL8SAAAAAFnT0l3UNPOV8mkpHIown-ysSR1g", "6LdfmL8SAAAAAHKPqUQV5SxrRX9Id6a8cQo-mgpE", false);
		
	/**
	 * Gets the email address.
	 *
	 * @return the emailAddress
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * Sets the email address.
	 *
	 * @param emailAddress            the emailAddress to set
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password.
	 *
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Gets the password_confirmation.
	 *
	 * @return the repeatPassword
	 */
	public String getPassword_confirmation() {
		return password_confirmation;
	}

	/**
	 * Sets the password_confirmation.
	 *
	 * @param password_confirmation the new password_confirmation
	 */
	public void setPassword_confirmation(String password_confirmation) {
		this.password_confirmation = password_confirmation;
	}
	
	/**
	 * Gets the role type.
	 *
	 * @return the rol
	 */
	public RoleTypeEnum getRoleType() {
		return roleType;
	}

	/**
	 * Sets the role type.
	 *
	 * @param roleType the new role type
	 */
	public void setRoleType(RoleTypeEnum roleType) {
		this.roleType = roleType;
	}
	
	/**
	 * Gets the recaptcha_challenge_field.
	 *
	 * @return the recaptcha_challenge_field
	 */
	public String getRecaptcha_challenge_field() {

		return recaptcha_challenge_field;

	}

	/**
	 * Sets the recaptcha_challenge_field.
	 *
	 * @param recaptcha_challenge_field the new recaptcha_challenge_field
	 */
	public void setRecaptcha_challenge_field(String recaptcha_challenge_field) {

		this.recaptcha_challenge_field = recaptcha_challenge_field;

	}

	/**
	 * Gets the recaptcha_response_field.
	 *
	 * @return the recaptcha_response_field
	 */
	public String getRecaptcha_response_field() {

		return recaptcha_response_field;

	}

	/**
	 * Sets the recaptcha_response_field.
	 *
	 * @param recaptcha_response_field the new recaptcha_response_field
	 */
	public void setRecaptcha_response_field(String recaptcha_response_field) {

		this.recaptcha_response_field = recaptcha_response_field;

	}
	
	/**
	 * Gets the re captcha html.
	 *
	 * @return the re captcha html
	 */
	public String getReCaptchaHtml(){
		
		return reCatcpha.createRecaptchaHtml(null, null);
	}
	
	/**
	 * Checks if is valid captcha.
	 *
	 * @return true, if is valid captcha
	 */
	public boolean isValidCaptcha(){
        ReCaptchaResponse reCaptchaResponse = reCatcpha.checkAnswer("localhost", getRecaptcha_challenge_field(), getRecaptcha_response_field());
        return reCaptchaResponse.isValid();
	}
}
