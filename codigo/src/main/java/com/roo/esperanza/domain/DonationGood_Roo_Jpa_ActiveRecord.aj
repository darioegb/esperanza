// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.roo.esperanza.domain;

import com.roo.esperanza.domain.DonationGood;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

privileged aspect DonationGood_Roo_Jpa_ActiveRecord {
    
    public static final List<String> DonationGood.fieldNames4OrderClauseFilter = java.util.Arrays.asList("requestDonations", "photo");
    
    public static long DonationGood.countDonationGoods() {
        return entityManager().createQuery("SELECT COUNT(o) FROM DonationGood o", Long.class).getSingleResult();
    }
    
    public static List<DonationGood> DonationGood.findAllDonationGoods() {
        return entityManager().createQuery("SELECT o FROM DonationGood o", DonationGood.class).getResultList();
    }
    
    public static List<DonationGood> DonationGood.findAllDonationGoods(String sortFieldName, String sortOrder) {
        String jpaQuery = "SELECT o FROM DonationGood o";
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            jpaQuery = jpaQuery + " ORDER BY " + sortFieldName;
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder)) {
                jpaQuery = jpaQuery + " " + sortOrder;
            }
        }
        return entityManager().createQuery(jpaQuery, DonationGood.class).getResultList();
    }
    
    public static DonationGood DonationGood.findDonationGood(Long id) {
        if (id == null) return null;
        return entityManager().find(DonationGood.class, id);
    }
    
    public static List<DonationGood> DonationGood.findDonationGoodEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM DonationGood o", DonationGood.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    public static List<DonationGood> DonationGood.findDonationGoodEntries(int firstResult, int maxResults, String sortFieldName, String sortOrder) {
        String jpaQuery = "SELECT o FROM DonationGood o";
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            jpaQuery = jpaQuery + " ORDER BY " + sortFieldName;
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder)) {
                jpaQuery = jpaQuery + " " + sortOrder;
            }
        }
        return entityManager().createQuery(jpaQuery, DonationGood.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    @Transactional
    public DonationGood DonationGood.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        DonationGood merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
}
