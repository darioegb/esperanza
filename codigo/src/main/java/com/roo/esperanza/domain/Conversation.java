package com.roo.esperanza.domain;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import javax.persistence.EntityManager;
import javax.persistence.ManyToOne;
import javax.persistence.TypedQuery;

// TODO: Auto-generated Javadoc
/**
 * The Class Conversation.
 */
@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findConversationsBySenderAndReceiver", "findConversationsBySenderOrReceiver" })
public class Conversation {

    /** The sender. */
    @ManyToOne
    private Participant sender;

    /** The receiver. */
    @ManyToOne
    private Participant receiver;

    /**
     * Check conversation by participants.
     *
     * @param sender the sender
     * @param receiver the receiver
     * @return the typed query
     */
    public static TypedQuery<Conversation> checkConversationByParticipants(Participant sender, Participant receiver) {
        EntityManager em = Message.entityManager();
        TypedQuery<Conversation> q = em.createQuery("SELECT c FROM Conversation as c WHERE (receiver = :receiver AND sender = :sender) OR (receiver = :sender AND sender = :receiver)", Conversation.class);
        q.setParameter("sender", sender);
        q.setParameter("receiver", receiver);
        return q;
    }

	/**
	 * Find conversations by sender or receiver.
	 *
	 * @param sender the sender
	 * @param receiver the receiver
	 * @return the typed query
	 */
	public static TypedQuery<Conversation> findConversationsBySenderOrReceiver(Participant sender, Participant receiver) {
        if (sender == null) throw new IllegalArgumentException("The sender argument is required");
        if (receiver == null) throw new IllegalArgumentException("The receiver argument is required");
        EntityManager em = Conversation.entityManager();
        TypedQuery<Conversation> q = em.createQuery("SELECT o FROM Conversation AS o WHERE o.sender = :sender OR o.receiver = :receiver ORDER BY o.id DESC", Conversation.class);
        q.setParameter("sender", sender);
        q.setParameter("receiver", receiver);
        return q;
    }
}
