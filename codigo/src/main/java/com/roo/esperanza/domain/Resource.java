package com.roo.esperanza.domain;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import com.roo.esperanza.reference.ResourceTypeEnum;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.EntityManager;
import javax.persistence.OneToMany;
import javax.persistence.TypedQuery;
import javax.persistence.ManyToOne;

// TODO: Auto-generated Javadoc
/**
 * The Class Resource.
 */
@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findResourcesByDenouncements", "findResourcesByParticipantOwner" })
public class Resource {

    /** The id resource. */
    private Integer idResource;

    /** The resource type. */
    @NotNull
    @Enumerated(EnumType.STRING)
    private ResourceTypeEnum resourceType;

    /** The denouncements. */
    @OneToMany(cascade = CascadeType.ALL)
    private Set<Denouncement> denouncements = new HashSet<Denouncement>();

    /** The participant owner. */
    @ManyToOne
    private Participant participantOwner;
    
    /**
     * Find resources by denouncement.
     *
     * @param denouncement the denouncement
     * @return the typed query
     */
    public static TypedQuery<Resource> findResourcesByDenouncement(Denouncement denouncement) {
        EntityManager em = Resource.entityManager();
        StringBuilder queryBuilder = new StringBuilder("SELECT o FROM Resource AS o LEFT JOIN o.denouncements d WHERE LOWER(d) LIKE LOWER(:denouncement)");
        TypedQuery<Resource> q = em.createQuery(queryBuilder.toString(), Resource.class);
        q.setParameter("denouncement", denouncement);

        return q;
    }
}
