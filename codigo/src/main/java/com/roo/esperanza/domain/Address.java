package com.roo.esperanza.domain;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Size;

// TODO: Auto-generated Javadoc
/**
 * The Class Address.
 */
@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class Address {

    /** The address. */
    @Column(name = "address")
    @Size(min = 10, max = 100)
    private String address;

    /** The city. */
    @Column(name = "city")
    @Size(min = 3, max = 65)
    private String city;

    /** The province. */
    @ManyToOne
    private Province province;

    /** The country. */
    @ManyToOne
    private Country country;

    /** The postal code. */
    @Column(name = "postal_code")
    private String postalCode;
}
