// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.roo.esperanza.domain;

import com.roo.esperanza.domain.Conversation;
import com.roo.esperanza.domain.Participant;

privileged aspect Conversation_Roo_JavaBean {
    
    public Participant Conversation.getSender() {
        return this.sender;
    }
    
    public void Conversation.setSender(Participant sender) {
        this.sender = sender;
    }
    
    public Participant Conversation.getReceiver() {
        return this.receiver;
    }
    
    public void Conversation.setReceiver(Participant receiver) {
        this.receiver = receiver;
    }
    
}
