package com.roo.esperanza.domain;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

// TODO: Auto-generated Javadoc
/**
 * The Class PublicationHistory.
 */
@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class PublicationHistory {

    /** The admin name. */
    @NotNull
    @Column(name = "admin_name")
    @Size(min = 5, max = 60)
    private String adminName;
    
    /** The array status. */
    @Column(name = "array_status")
    private ArrayList<String> arrayStatus = new ArrayList<String>();
}
