package com.roo.esperanza.domain;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Query;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import com.roo.esperanza.reference.RoleTypeEnum;

// TODO: Auto-generated Javadoc
/**
 * The Class Participant.
 */
@RooJavaBean
@RooToString
@RooJpaActiveRecord
public abstract class Participant {

    /** The user. */
    @ManyToOne
    private User user;

    /** The address. */
    @ManyToOne
    private Address address;

    /** The date. */
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Date date;

    /** The phone. */
    @Column(name = "phone")
    private Integer phone;

    /** The cell phone. */
    @Column(name = "cellPhone")
    private Integer cellPhone;

    /** The profile picture. */
    @Column(name = "profile_picture")
    private String profilePicture;

    /** The donations. */
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "donor")
    private Set<Donation> donations = new HashSet<Donation>();

    /** The necessitys. */
    @OneToMany(cascade = CascadeType.ALL)
    private Set<Necessity> necessitys = new HashSet<Necessity>();

    /** The array score. */
    @OneToMany(cascade = CascadeType.ALL)
    private Set<Puntuation> arrayScore = new HashSet<Puntuation>();

    /** The resources. */
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "participantOwner")
    private Set<Resource> resources = new HashSet<Resource>();
    
    /**
	 * Find person by hash.
	 *
	 * @param hash the hash
	 * @return the person
	 */
	public static Participant findParticipantByHash(String hash) {
        EntityManager em = Participant.entityManager();
        Query q = em.createNativeQuery("SELECT * FROM participant AS p WHERE SHA2(p.id,256)=:hash", Participant.class);
        q.setParameter("hash", hash);
        return (Participant) q.getSingleResult();
    }

    /**
     * Find statistics by date between or user role type.
     *
     * @param minDate the min date
     * @param maxDate the max date
     * @param roleType the role type
     * @return the typed query
     */
    public static TypedQuery<Participant> findStatisticsByDateBetweenOrUserRoleType(Date minDate, Date maxDate, RoleTypeEnum roleType) {
        EntityManager em = Participant.entityManager();
        StringBuilder queryBuilder = new StringBuilder("SELECT o FROM Participant AS o LEFT JOIN o.necessitys as n WHERE o.date BETWEEN :minDate AND :maxDate OR o.user.rol.roleType = :roleType AND o.user.enabled=1 AND n.statusType='Accept' GROUP BY o ORDER BY COUNT(n) DESC");
        TypedQuery<Participant> q = em.createQuery(queryBuilder.toString(), Participant.class);
        q.setParameter("minDate", minDate);
        q.setParameter("maxDate", maxDate);
        q.setParameter("roleType", roleType);
        return q;
    }

    /**
     * Count find statistics by date between or user role type.
     *
     * @param minDate the min date
     * @param maxDate the max date
     * @param roleType the role type
     * @return the float
     */
    public static float countFindStatisticsByDateBetweenOrUserRoleType(Date minDate, Date maxDate, RoleTypeEnum roleType) {
        EntityManager em = Participant.entityManager();
        StringBuilder queryBuilder = new StringBuilder("SELECT COUNT(o) FROM Participant AS o LEFT JOIN o.necessitys as n WHERE o.date BETWEEN :minDate AND :maxDate OR o.user.rol.roleType = :roleType AND o.user.enabled=1 AND n.statusType='Accept' GROUP BY o ORDER BY COUNT(n) DESC");
        TypedQuery<Long> q = em.createQuery(queryBuilder.toString(), Long.class);
        q.setParameter("minDate", minDate);
        q.setParameter("maxDate", maxDate);
        q.setParameter("roleType", roleType);
        return ((Long) q.getSingleResult());
    }

    /**
     * Find all statistics.
     *
     * @return the list
     */
    public static List<Participant> findAllStatistics() {
        EntityManager em = Donation.entityManager();
        StringBuilder queryBuilder = new StringBuilder("SELECT o FROM Participant AS o LEFT JOIN o.necessitys as n WHERE o.user.enabled=1 AND n.statusType='Accept' GROUP BY o ORDER BY COUNT(n) DESC");
        TypedQuery<Participant> q = em.createQuery(queryBuilder.toString(), Participant.class);
        return q.getResultList();
    }

	/**
	 * Find all participants.
	 *
	 * @return the list
	 */
	public static List<Participant> findAllParticipants() {
		UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = User.findUsersByEmailAddress(userDetails.getUsername()).getSingleResult();
        return entityManager().
        		createQuery("SELECT o FROM Participant o WHERE o.user!=:user", Participant.class).
        		setParameter("user", user).
        		getResultList();
    }
}
