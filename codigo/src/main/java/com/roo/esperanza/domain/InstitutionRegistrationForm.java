package com.roo.esperanza.domain;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

// TODO: Auto-generated Javadoc
/**
 * The Class InstitutionRegistrationForm.
 */
public class InstitutionRegistrationForm {
	
	/** The email address. */
	private String emailAddress;

	/** The social reason. */
    @NotNull
    @Size(min = 3, max=100)
    private String socialReason;
    
    /** The date. */
    @DateTimeFormat(pattern="dd/MM/yyyy")   
    private Date date;
	
    /** The phone. */
    private Integer phone;

    /** The cell phone. */
    private Integer cellPhone;
        
    /** The city. */
    @NotNull
    private String city;
    
    /** The address. */
    @NotNull
    private String address;
    
    /** The province. */
    @NotNull
    private  Province province;
    
    /** The country. */
    @NotNull
    private  Country country;
    
    /** The postal code. */
    @NotNull
	private  String postalCode;

    /** The short description. */
    @NotNull
    @Size(min = 30, max = 100)
    private String shortDescription;

    /** The long description. */
    @NotNull
    @Size(min = 100, max = 1000)
    private String longDescription;
    
    /** The cbu. */
    @NotNull
    private Integer cbu;
    
    /** The category. */
    @NotNull
    private Category category;

	/**
	 * Gets the email address.
	 *
	 * @return the email address
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * Sets the email address.
	 *
	 * @param emailAddress the new email address
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	/**
	 * Gets the social reason.
	 *
	 * @return the social reason
	 */
	public String getSocialReason() {
		return socialReason;
	}

	/**
	 * Sets the social reason.
	 *
	 * @param socialReason the new social reason
	 */
	public void setSocialReason(String socialReason) {
		this.socialReason = socialReason;
	}

	/**
	 * Gets the date.
	 *
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * Sets the date.
	 *
	 * @param date the new date
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * Gets the phone.
	 *
	 * @return the phone
	 */
	public Integer getPhone() {
		return phone;
	}

	/**
	 * Sets the phone.
	 *
	 * @param phone the new phone
	 */
	public void setPhone(Integer phone) {
		this.phone = phone;
	}

	/**
	 * Gets the cell phone.
	 *
	 * @return the cell phone
	 */
	public Integer getCellPhone() {
		return cellPhone;
	}

	/**
	 * Sets the cell phone.
	 *
	 * @param cellPhone the new cell phone
	 */
	public void setCellPhone(Integer cellPhone) {
		this.cellPhone = cellPhone;
	}

	/**
	 * Gets the city.
	 *
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Sets the city.
	 *
	 * @param city the new city
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * Gets the address.
	 *
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Sets the address.
	 *
	 * @param address the new address
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Gets the province.
	 *
	 * @return the province
	 */
	public Province getProvince() {
		return province;
	}

	/**
	 * Sets the province.
	 *
	 * @param province the new province
	 */
	public void setProvince(Province province) {
		this.province = province;
	}

	/**
	 * Gets the country.
	 *
	 * @return the country
	 */
	public Country getCountry() {
		return country;
	}

	/**
	 * Sets the country.
	 *
	 * @param country the new country
	 */
	public void setCountry(Country country) {
		this.country = country;
	}

	/**
	 * Gets the postal code.
	 *
	 * @return the postal code
	 */
	public String getPostalCode() {
		return postalCode;
	}

	/**
	 * Sets the postal code.
	 *
	 * @param postalCode the new postal code
	 */
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	/**
	 * Gets the short description.
	 *
	 * @return the short description
	 */
	public String getShortDescription() {
		return shortDescription;
	}

	/**
	 * Sets the short description.
	 *
	 * @param shortDescription the new short description
	 */
	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	/**
	 * Gets the long description.
	 *
	 * @return the long description
	 */
	public String getLongDescription() {
		return longDescription;
	}

	/**
	 * Sets the long description.
	 *
	 * @param longDescription the new long description
	 */
	public void setLongDescription(String longDescription) {
		this.longDescription = longDescription;
	}

	/**
	 * Gets the cbu.
	 *
	 * @return the cbu
	 */
	public Integer getCbu() {
		return cbu;
	}

	/**
	 * Sets the cbu.
	 *
	 * @param cbu the new cbu
	 */
	public void setCbu(Integer cbu) {
		this.cbu = cbu;
	}
	
	/**
	 * Gets the category.
	 *
	 * @return the category
	 */
	public Category getCategory() {
		return category;
	}

	/**
	 * Sets the category.
	 *
	 * @param category the new category
	 */
	public void setCategory(Category category) {
		this.category = category;
	}
}
