/**
 * 
 */
package com.roo.esperanza.domain;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

// TODO: Auto-generated Javadoc
/**
 * The Class ChangePasswordForm.
 *
 * @author rohit
 */
public class ChangePasswordForm {
	
	/** The old password. */
	@NotNull
	@Size(min = 3, max = 15)
	private String oldPassword;
	
	/** The new password_confirmation. */
	@NotNull
	@Size(min = 3, max = 15)
	private String newPassword_confirmation;
	
	/** The new password. */
	@NotNull
	@Size(min = 3, max = 15)
	private String newPassword;

	/**
	 * Gets the old password.
	 *
	 * @return the old password
	 */
	public String getOldPassword() {
		return oldPassword;
	}

	/**
	 * Sets the old password.
	 *
	 * @param oldPassword the new old password
	 */
	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	/**
	 * Gets the new password.
	 *
	 * @return the new password
	 */
	public String getNewPassword() {
		return newPassword;
	}

	/**
	 * Sets the new password.
	 *
	 * @param newPassword the new new password
	 */
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	/**
	 * Gets the new password_confirmation.
	 *
	 * @return the new password_confirmation
	 */
	public String getNewPassword_confirmation() {
		return newPassword_confirmation;
	}

	/**
	 * Sets the new password_confirmation.
	 *
	 * @param newPassword_confirmation the new new password_confirmation
	 */
	public void setNewPassword_confirmation(String newPassword_confirmation) {
		this.newPassword_confirmation = newPassword_confirmation;
	}

}
