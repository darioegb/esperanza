package com.roo.esperanza.domain;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.OneToMany;
import javax.persistence.TypedQuery;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import com.roo.esperanza.reference.DonationTypeEnum;

// TODO: Auto-generated Javadoc
/**
 * The Class DonationGood.
 */
@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findDonationGoodsByDescriptionLikeOrDonationTypeOrDateBetween" })
public class DonationGood extends Publication {

    /** The request donations. */
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "donationGood")
    private Set<DonationRequest> requestDonations = new HashSet<DonationRequest>();

    /** The photo. */
    @Column(name = "photo_path")
    private String photo;
    
    /**
     * Find all donation goods by creator.
     *
     * @param sortFieldName the sort field name
     * @param sortOrder the sort order
     * @param creator the creator
     * @return the list
     */
    public static List<DonationGood> findAllDonationGoodsByCreator(String sortFieldName, String sortOrder, Participant creator) {
        EntityManager em = DonationGood.entityManager();
        TypedQuery<DonationGood> q = em.createQuery("SELECT o FROM DonationGood AS o WHERE o.creator = :creator AND o.statusType='Accept'", DonationGood.class);
        q.setParameter("creator",creator);
        return q.getResultList();
    }
    
    /**
     * Find all donation goods by creator.
     *
     * @param firstResult the first result
     * @param maxResults the max results
     * @param sortFieldName the sort field name
     * @param sortOrder the sort order
     * @param creator the creator
     * @return the list
     */
    public static List<DonationGood> findAllDonationGoodsByCreator(int firstResult, int maxResults, String sortFieldName, String sortOrder, Participant creator) {
    	EntityManager em = DonationGood.entityManager();
    	String jpaQuery = "SELECT o FROM DonationGood o WHERE o.creator = :creator AND o.statusType='Accept'";
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            jpaQuery = jpaQuery + " ORDER BY " + sortFieldName;
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder)) {
                jpaQuery = jpaQuery + " " + sortOrder;
            }
        }
        TypedQuery<DonationGood> q = em.createQuery(jpaQuery, DonationGood.class);
        q.setParameter("creator",creator);
        return q.setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }

	/**
	 * Find donation goods by description like or donation type or date between.
	 *
	 * @param description the description
	 * @param donationType the donation type
	 * @param minDate the min date
	 * @param maxDate the max date
	 * @param sortFieldName the sort field name
	 * @param sortOrder the sort order
	 * @return the typed query
	 */
	public static TypedQuery<DonationGood> findDonationGoodsByDescriptionLikeOrDonationTypeOrDateBetween(String description, DonationTypeEnum donationType, Date minDate, Date maxDate, String sortFieldName, String sortOrder) {
        EntityManager em = DonationGood.entityManager();
		if (description.length() > 0){
	        description = description.replace('*', '%');
	        if (description.charAt(0) != '%')	description = "%" + description;
	        if (description.charAt(description.length() - 1) != '%')	description = description + "%";	       
        }
		UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = User.findUsersByEmailAddress(userDetails.getUsername()).getSingleResult();
        StringBuilder queryBuilder = new StringBuilder("SELECT o FROM DonationGood AS o WHERE LOWER(o.description) LIKE LOWER(:description)  OR o.donationType = :donationType OR o.date BETWEEN :minDate AND :maxDate AND o.creator.user.enabled=1 AND o.creator.user!=:user");
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            queryBuilder.append(" ORDER BY ").append(sortFieldName);
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder)) {
                queryBuilder.append(" ").append(sortOrder);
            }
        }
        TypedQuery<DonationGood> q = em.createQuery(queryBuilder.toString(), DonationGood.class);
        q.setParameter("description", description);
        q.setParameter("donationType", donationType);
        q.setParameter("minDate", minDate);
        q.setParameter("maxDate", maxDate);
        q.setParameter("user", user);
        return q;
    }

	/**
	 * Count find donation goods by description like or donation type or date between.
	 *
	 * @param description the description
	 * @param donationType the donation type
	 * @param minDate the min date
	 * @param maxDate the max date
	 * @return the long
	 */
	public static Long countFindDonationGoodsByDescriptionLikeOrDonationTypeOrDateBetween(String description, DonationTypeEnum donationType, Date minDate, Date maxDate) {
		EntityManager em = DonationGood.entityManager();
		if (description.length() > 0){
	        description = description.replace('*', '%');
	        if (description.charAt(0) != '%')	description = "%" + description;
	        if (description.charAt(description.length() - 1) != '%')	description = description + "%";	       
        }
		UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = User.findUsersByEmailAddress(userDetails.getUsername()).getSingleResult();
        TypedQuery<Long> q = em.createQuery("SELECT COUNT(o) FROM DonationGood AS o WHERE LOWER(o.description) LIKE LOWER(:description)  OR o.donationType = :donationType OR o.date BETWEEN :minDate AND :maxDate  AND o.creator.user.enabled=1 AND o.creator.user!=:user", Long.class);
        q.setParameter("description", description);
        q.setParameter("donationType", donationType);
        q.setParameter("minDate", minDate);
        q.setParameter("maxDate", maxDate);
        q.setParameter("user", user);

        return ((Long) q.getSingleResult());
    }
	
	/**
	 * Find all donation goods distinct user.
	 *
	 * @param sortFieldName the sort field name
	 * @param sortOrder the sort order
	 * @return the list
	 */
	public static List<DonationGood> findAllDonationGoodsDistinctUser(String sortFieldName, String sortOrder) {
        EntityManager em = DonationGood.entityManager();
        String query = "SELECT o FROM DonationGood AS o";
        UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = User.findUsersByEmailAddress(userDetails.getUsername()).getSingleResult();
        StringBuilder queryBuilder = new StringBuilder(query+" WHERE o.creator.user.enabled=1 AND o.creator.user!=:user");
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            queryBuilder.append(" ORDER BY ").append(sortFieldName);
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder))	queryBuilder.append(" " + sortOrder);
        }
        TypedQuery<DonationGood> q = em.createQuery(queryBuilder.toString(), DonationGood.class);
        q.setParameter("user", user);
        return q.getResultList();
    }
}
