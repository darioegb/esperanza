package com.roo.esperanza.domain;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import com.roo.esperanza.reference.GenreTypeEnum;

// TODO: Auto-generated Javadoc
/**
 * The Class Person.
 */
@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findPeopleByNecessitysOrFirstNameLike", "findPeopleByUser" })
public class Person extends Participant {

    /** The first name. */
    @NotNull
    @Column(name = "first_name")
    @Size(min = 3)
    private String firstName;

    /** The last name. */
    @NotNull
    @Column(name = "last_name")
    @Size(min = 3)
    private String lastName;

    /** The personal description. */
    @NotNull
    @Column(name = "personal_description")
    @Size(min = 20, max = 100)
    private String personalDescription;

    /** The genre type. */
    @NotNull
    @Enumerated(EnumType.STRING)
    private GenreTypeEnum genreType;

    /** The identification. */
    @Column(name = "identification")
    private Integer identification;

    /** The suscriptions. */
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "suscriptor")
    private Set<Suscription> suscriptions = new HashSet<Suscription>();
    
    /** The donation goods. */
    @OneToMany(cascade = CascadeType.ALL)
    private Set<DonationGood> donationGoods = new HashSet<DonationGood>();
    
    /**
     * Count find people by necessity description like or first name like.
     *
     * @param description the description
     * @param firstName the first name
     * @return the long
     */
    public static Long countFindPeopleByNecessityDescriptionLikeOrFirstNameLike(String description, String firstName) {
    	EntityManager em = Person.entityManager();
    	if(firstName.length() > 0){
			firstName = firstName.replace('*', '%');
	        if (firstName.charAt(0) != '%')	firstName = "%" + firstName;
	        if (firstName.charAt(firstName.length() - 1) != '%')	firstName = firstName + "%";
		}        
        if(description.length() > 0){
        	description = description.replace('*', '%');
            if (description.charAt(0) != '%')	description = "%" + description;
            if (description.charAt(description.length() - 1) != '%')	description = description + "%";    
        }
        String query = "SELECT COUNT(p) FROM Person AS p";
        if(description.length() > 0 && firstName.length() == 0)	query += " LEFT JOIN p.necessitys AS n WHERE LOWER(n.description) LIKE LOWER(:description)";
        else query += " LEFT JOIN p.necessitys AS n WHERE LOWER(p.firstName) LIKE LOWER(:firstName) OR LOWER(n.description) LIKE LOWER(:description)";
        UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = User.findUsersByEmailAddress(userDetails.getUsername()).getSingleResult();
        StringBuilder queryBuilder = new StringBuilder(query+" AND p.user.enabled=1 AND p.user!=:user");
        TypedQuery<Long> q = em.createQuery(queryBuilder.toString(), Long.class);
        q.setParameter("description", description);
        q.setParameter("user", user);
        if(firstName.length() > 0)	q.setParameter("firstName", firstName);      
        return ((Long) q.getSingleResult());
    }

	/**
	 * Find people by necessity description like or first name like.
	 *
	 * @param description the description
	 * @param firstName the first name
	 * @return the typed query
	 */
	public static TypedQuery<Person> findPeopleByNecessityDescriptionLikeOrFirstNameLike(String description, String firstName) {
        EntityManager em = Person.entityManager();
        if(firstName.length() > 0){
			firstName = firstName.replace('*', '%');
	        if (firstName.charAt(0) != '%')	firstName = "%" + firstName;
	        if (firstName.charAt(firstName.length() - 1) != '%')	firstName = firstName + "%";
		}        
        if(description.length() > 0){
        	description = description.replace('*', '%');
            if (description.charAt(0) != '%')	description = "%" + description;
            if (description.charAt(description.length() - 1) != '%')	description = description + "%";    
        }
        String query = "SELECT p FROM Person AS p";
        if(description.length() > 0 && firstName.length() == 0)	query += " LEFT JOIN p.necessitys AS n WHERE LOWER(n.description) LIKE LOWER(:description)";
        else query += " LEFT JOIN p.necessitys AS n WHERE LOWER(p.firstName) LIKE LOWER(:firstName) OR LOWER(n.description) LIKE LOWER(:description)";
        UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = User.findUsersByEmailAddress(userDetails.getUsername()).getSingleResult();
        StringBuilder queryBuilder = new StringBuilder(query+" AND p.user.enabled=1 AND p.user!=:user");
        TypedQuery<Person> q = em.createQuery(queryBuilder.toString(), Person.class);
        q.setParameter("description", description);
        q.setParameter("user", user);
        if(firstName.length() > 0)	q.setParameter("firstName", firstName);
        return q;
    }

	/**
	 * Find people by necessity description like or first name like.
	 *
	 * @param description the description
	 * @param firstName the first name
	 * @param sortFieldName the sort field name
	 * @param sortOrder the sort order
	 * @return the typed query
	 */
	public static TypedQuery<Person> findPeopleByNecessityDescriptionLikeOrFirstNameLike(String description, String firstName, String sortFieldName, String sortOrder) {
		EntityManager em = Person.entityManager();
		if(firstName.length() > 0){
			firstName = firstName.replace('*', '%');
	        if (firstName.charAt(0) != '%')	firstName = "%" + firstName;
	        if (firstName.charAt(firstName.length() - 1) != '%')	firstName = firstName + "%";
		}        
        if(description.length() > 0){
        	description = description.replace('*', '%');
            if (description.charAt(0) != '%')	description = "%" + description;
            if (description.charAt(description.length() - 1) != '%')	description = description + "%";    
        }
        String query = "SELECT p FROM Person AS p";
        if(description.length() > 0 && firstName.length() == 0)	query += " LEFT JOIN p.necessitys AS n WHERE LOWER(n.description) LIKE LOWER(:description)";
        else query += " LEFT JOIN p.necessitys AS n WHERE LOWER(p.firstName) LIKE LOWER(:firstName) OR LOWER(n.description) LIKE LOWER(:description)";
        UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = User.findUsersByEmailAddress(userDetails.getUsername()).getSingleResult();
        StringBuilder queryBuilder = new StringBuilder(query+" AND p.user.enabled=1 AND p.user!=:user");
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            queryBuilder.append(" ORDER BY ").append(sortFieldName);
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder))	queryBuilder.append(" " + sortOrder);
        }
        TypedQuery<Person> q = em.createQuery(queryBuilder.toString(), Person.class);
        q.setParameter("description", description);
        q.setParameter("user", user);
        if(firstName.length() > 0)	q.setParameter("firstName", firstName);
        return q;
    }
	
	/**
	 * Find people by donation good description like or first name like.
	 *
	 * @param description the description
	 * @param firstName the first name
	 * @param sortFieldName the sort field name
	 * @param sortOrder the sort order
	 * @return the typed query
	 */
	public static TypedQuery<Person> findPeopleByDonationGoodDescriptionLikeOrFirstNameLike(String description,
			String firstName, String sortFieldName, String sortOrder) {
		EntityManager em = Person.entityManager();
		if(firstName.length() > 0){
			firstName = firstName.replace('*', '%');
	        if (firstName.charAt(0) != '%')	firstName = "%" + firstName;
	        if (firstName.charAt(firstName.length() - 1) != '%')	firstName = firstName + "%";
		}        
        if(description.length() > 0){
        	description = description.replace('*', '%');
            if (description.charAt(0) != '%')	description = "%" + description;
            if (description.charAt(description.length() - 1) != '%')	description = description + "%";    
        }
        String query = "SELECT p FROM Person AS p";
        if(description.length() > 0 && firstName.length() == 0)	query += " LEFT JOIN p.donationGoods AS dg WHERE LOWER(dg.description) LIKE LOWER(:description)";
        else query += " LEFT JOIN p.donationGoods AS dg WHERE LOWER(p.firstName) LIKE LOWER(:firstName) OR LOWER(dg.description) LIKE LOWER(:description)";
        UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = User.findUsersByEmailAddress(userDetails.getUsername()).getSingleResult();
        StringBuilder queryBuilder = new StringBuilder(query+" AND p.user.enabled=1 AND p.user!=:user");
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            queryBuilder.append(" ORDER BY ").append(sortFieldName);
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder))	queryBuilder.append(" " + sortOrder);
        }
        TypedQuery<Person> q = em.createQuery(queryBuilder.toString(), Person.class);
        q.setParameter("description", description);
        q.setParameter("user", user);
        if(firstName.length() > 0)	q.setParameter("firstName", firstName);
        return q;
	}

	/**
	 * Count find people by donation good description like or first name like.
	 *
	 * @param description the description
	 * @param firstName the first name
	 * @return the float
	 */
	public static float countFindPeopleByDonationGoodDescriptionLikeOrFirstNameLike(String description,
			String firstName) {
		EntityManager em = Person.entityManager();
    	if(firstName.length() > 0){
			firstName = firstName.replace('*', '%');
	        if (firstName.charAt(0) != '%')	firstName = "%" + firstName;
	        if (firstName.charAt(firstName.length() - 1) != '%')	firstName = firstName + "%";
		}        
        if(description.length() > 0){
        	description = description.replace('*', '%');
            if (description.charAt(0) != '%')	description = "%" + description;
            if (description.charAt(description.length() - 1) != '%')	description = description + "%";    
        }
        String query = "SELECT COUNT(p) FROM Person AS p";
        if(description.length() > 0 && firstName.length() == 0)	query += " LEFT JOIN p.donationGoods AS dg WHERE LOWER(dg.description) LIKE LOWER(:description)";
        else query += " LEFT JOIN p.donationGoods AS dg WHERE LOWER(p.firstName) LIKE LOWER(:firstName) OR LOWER(dg.description) LIKE LOWER(:description)";
        UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = User.findUsersByEmailAddress(userDetails.getUsername()).getSingleResult();
        StringBuilder queryBuilder = new StringBuilder(query+" AND p.user.enabled=1 AND p.user!=:user");
        TypedQuery<Long> q = em.createQuery(queryBuilder.toString(), Long.class);
        q.setParameter("description", description);
        q.setParameter("user", user);
        if(firstName.length() > 0)	q.setParameter("firstName", firstName);      
        return ((Long) q.getSingleResult());
	}
	
	/**
	 * Find all people with necessitys distinct user.
	 *
	 * @param sortFieldName the sort field name
	 * @param sortOrder the sort order
	 * @return the list
	 */
	public static List<Person> findAllPeopleWithNecessitysDistinctUser(String sortFieldName, String sortOrder) {
        EntityManager em = Person.entityManager();
        UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = User.findUsersByEmailAddress(userDetails.getUsername()).getSingleResult();
        StringBuilder queryBuilder = new StringBuilder("SELECT p FROM Person AS p WHERE p.user.enabled=1 AND p.user!=:user AND SIZE(p.necessitys) > 0");
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            queryBuilder.append(" ORDER BY ").append(sortFieldName);
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder))	queryBuilder.append(" " + sortOrder);
        }
        TypedQuery<Person> q = em.createQuery(queryBuilder.toString(), Person.class);
        q.setParameter("user", user);
        return q.getResultList();
    }
	
	/**
	 * Find all people with donation goods distinct user.
	 *
	 * @param sortFieldName the sort field name
	 * @param sortOrder the sort order
	 * @return the list
	 */
	public static List<Person> findAllPeopleWithDonationGoodsDistinctUser(String sortFieldName, String sortOrder) {
        EntityManager em = Person.entityManager();
        UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = User.findUsersByEmailAddress(userDetails.getUsername()).getSingleResult();
        StringBuilder queryBuilder = new StringBuilder("SELECT p FROM Person AS p WHERE p.user.enabled=1 AND p.user!=:user AND SIZE(p.donationGoods) > 0");
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            queryBuilder.append(" ORDER BY ").append(sortFieldName);
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder))	queryBuilder.append(" " + sortOrder);
        }
        TypedQuery<Person> q = em.createQuery(queryBuilder.toString(), Person.class);
        q.setParameter("user", user);
        return q.getResultList();
    }
}
