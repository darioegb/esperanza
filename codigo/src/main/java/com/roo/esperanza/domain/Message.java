package com.roo.esperanza.domain;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.json.RooJson;
import org.springframework.roo.addon.tostring.RooToString;

// TODO: Auto-generated Javadoc
/**
 * The Class Message.
 */
@RooJavaBean
@RooToString
@RooJpaActiveRecord
@RooJson
public class Message {

    /** The conversation. */
    @ManyToOne
    private Conversation conversation;

    /** The date. */
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Date date;

    /** The status. */
    @NotNull
    private Boolean status;

    /** The message. */
    @NotNull
    @Column(name = "message")
    @Size(min = 10, max = 250)
    private String message;

    /**
     * Count find messages by status not and user.
     *
     * @param user the user
     * @return the long
     */
    public static Long countFindMessagesByStatusNotAndUser(User user) {
        EntityManager em = Message.entityManager();
        TypedQuery<Long> q = em.createQuery("SELECT COUNT(o) FROM Message AS o LEFT JOIN  o.conversation AS c WHERE o.status=false AND c.receiver.user=:user", Long.class);
        q.setParameter("user", user);
        return ((Long) q.getSingleResult());
    }

    /**
     * Find messages by participants.
     *
     * @param sender the sender
     * @param receiver the receiver
     * @return the typed query
     */
    public static TypedQuery<Message> findMessagesByParticipants(Participant sender, Participant receiver) {
        EntityManager em = Message.entityManager();
        TypedQuery<Message> q = em.createQuery("SELECT m FROM Message AS m LEFT JOIN m.conversation AS c WHERE (c.receiver = :receiver AND c.sender = :sender) OR (c.receiver = :sender AND c.sender = :receiver) ORDER BY m.id ASC", Message.class);
        q.setParameter("sender", sender);
        q.setParameter("receiver", receiver);

        return q;
    }
}
