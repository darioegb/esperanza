// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.roo.esperanza.domain;

import com.roo.esperanza.domain.Necessity;
import org.springframework.beans.factory.annotation.Configurable;

privileged aspect Necessity_Roo_Configurable {
    
    declare @type: Necessity: @Configurable;
    
}
