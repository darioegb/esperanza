package com.roo.esperanza.web;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.roo.esperanza.domain.Donation;
import com.roo.esperanza.domain.DonationRequest;
import com.roo.esperanza.domain.Notification;
import com.roo.esperanza.domain.Publication;
import com.roo.esperanza.domain.Role;
import com.roo.esperanza.domain.User;
import com.roo.esperanza.provider.ScriptAddService;
import com.roo.esperanza.provider.UtilityService;
import com.roo.esperanza.reference.DonationTypeEnum;
import com.roo.esperanza.reference.NotificationTypeEnum;
import com.roo.esperanza.reference.RoleTypeEnum;
import com.roo.esperanza.reference.StatusTypeEnum;

// TODO: Auto-generated Javadoc
/**
 * The Class PublicationController.
 */
@RequestMapping("/publications/**")
@Controller
public class PublicationController {
	
	/** The u service. */
	@Autowired
	UtilityService uService;
	
	/** The sa service. */
	@Autowired 
	ScriptAddService saService;
	
	/** The mail sender. */
	@Autowired
	private transient MailSender mailSender;
	
	/**
	 * Show.
	 *
	 * @param hash the hash
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(params = "q", produces = "text/html")
    public String show(@RequestParam(value="q", required = true) String hash, Model model) {
        addDateTimeFormatPatterns(model);
        Publication publication = Publication.findPublicationByHash(hash);
        saService.agregarJS("general/modal.js");
        saService.publicarJsCss(model);
        model.addAttribute("publication", publication);
        model.addAttribute("itemId", publication.getId());
        model.addAttribute("donation", new Donation());
        model.addAttribute("donationRequest", new DonationRequest());
        return "publications/show";
    }
	
	/**
	 * Find publications by date between or description like or status type or donation type or creator role type form.
	 *
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(params ={"find=ByDateBetweenOrDescriptionLikeOrStatusTypeOrDonationTypeOrCreatorRoleType", "form"}, method = RequestMethod.GET)
    public String findPublicationsByDateBetweenOrDescriptionLikeOrStatusTypeOrDonationTypeOrCreatorRoleTypeForm(Model model) {
		saService.agregarJS("general/removeDateTimesFinder.js");
        saService.publicarJsCss(model);
		model.addAttribute("statustypeenums", java.util.Arrays.asList(StatusTypeEnum.class.getEnumConstants()));
		model.addAttribute("donationtypeenums", java.util.Arrays.asList(DonationTypeEnum.class.getEnumConstants()));
    	model.addAttribute("roletypeenums", Arrays.asList(RoleTypeEnum.values()).subList(1, 3));
        addDateTimeFormatPatterns(model);
        return "publications/findPublicationsByDateBetweenOrDescriptionLikeOrStatusTypeOrDonationTypeOrCreatorRoleType";
    }
	
	/**
	 * Find publications by date between or description like or status type or donation type or creator role type.
	 *
	 * @param minDate the min date
	 * @param maxDate the max date
	 * @param description the description
	 * @param statusType the status type
	 * @param donationType the donation type
	 * @param role the role
	 * @param page the page
	 * @param size the size
	 * @param sortFieldName the sort field name
	 * @param sortOrder the sort order
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value="/list", method = RequestMethod.GET)
    public String findPublicationsByDateBetweenOrDescriptionLikeOrStatusTypeOrDonationTypeOrCreatorRoleType(@RequestParam(value="minDate", required=false) Date minDate, 
    		@RequestParam(value="maxDate", required=false) Date maxDate, 
    		@RequestParam(value="description", required = false) String description, 
    		@RequestParam(value="statusType", required = false) StatusTypeEnum statusType, 
    		@RequestParam(value="donationType", required = false) DonationTypeEnum donationType, 
    		@RequestParam(value="role", required = false) Role role, 
    		@RequestParam(value = "page", required = false) Integer page, 
    		@RequestParam(value = "size", required = false) Integer size, 
    		@RequestParam(value = "sortFieldName", required = false) String sortFieldName, 
    		@RequestParam(value = "sortOrder", required = false) String sortOrder, Model model) {
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            if(sortFieldName == null && sortOrder == null){
            	sortFieldName="id";
            	sortOrder = "DESC";
            }
            model.addAttribute("publications", Publication.findPublicationsByDateBetweenOrDescriptionLikeOrStatusTypeOrDonationTypeOrCreatorRoleType(minDate, maxDate, description, statusType, donationType, role, sortFieldName, sortOrder).setFirstResult(firstResult).setMaxResults(sizeNo).getResultList());
            float nrOfPages = (float) Publication.countFindPublicationsByDateBetweenOrDescriptionLikeOrStatusTypeOrDonationTypeOrCreatorRoleType(minDate, maxDate, description, statusType, donationType, role) / sizeNo;
            model.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
        	if(description.length() == 0 && statusType == null && donationType == null && role == null){
            	model.addAttribute("publications", Publication.findAllPublicationsByStatusType(sortFieldName, sortOrder));
        	}else{
            	model.addAttribute("publications", Publication.findPublicationsByDateBetweenOrDescriptionLikeOrStatusTypeOrDonationTypeOrCreatorRoleType(minDate, maxDate, description, statusType, donationType, role).getResultList());
        	}
        }
        addDateTimeFormatPatterns(model);
        return "publications/list";
    }
	
	/**
	 * Accept.
	 *
	 * @param publication the publication
	 * @param model the model
	 * @param request the request
	 * @param redirectAttrs the redirect attrs
	 * @return the string
	 */
	@RequestMapping(value="/accept", method = RequestMethod.POST, produces = "text/html")
    public String accept(@ModelAttribute("publication") Publication publication, 
    		Model model, HttpServletRequest request, final RedirectAttributes redirectAttrs) {
        model.asMap().clear();
        try {
        	User user = publication.getCreator().getUser();
        	publication.setStatusType(StatusTypeEnum.Accept);
        	Notification notification = new Notification();
        	Date date = new Date();
        	notification.setDate(date);
        	notification.setNotificationType(NotificationTypeEnum.PublicactionStatus);
        	notification.setStatus(true);
        	try {
        		Boolean status = uService.checkInternetConnexion();
        		if(status){
	        		SimpleMailMessage mail = new SimpleMailMessage();
	        		mail.setTo(user.getEmailAddress());
	        		mail.setSubject("Publicación dada de baja - Esperanza");
	        		mail.setText("Hola "+user.getEmailAddress()+",\n. Su publicación ha sido aprobada.\n Muchas Gracias Administración de Esperanza");
	        		mailSender.send(mail);
        		}
        	} catch (IOException e) {
        		e.printStackTrace();
        	}
    		updatePublicationStatus(publication, notification);
        } catch (Exception e) {
			e.printStackTrace();
		}
        redirectAttrs.addFlashAttribute("acceptPublication", "true");
        return "redirect:/publications/list";
    }
	
	/**
	 * Cancel publication form.
	 *
	 * @param id the id
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value="/cancel/{id}", produces = "text/html")
    public String cancelPublicationForm(@PathVariable("id") Long id, Model model) {
		model.addAttribute("idPublication", id);
		model.addAttribute("notification", new Notification());
        return "publications/cancelPublication";
    }
	
	/**
	 * Cancel.
	 *
	 * @param notification the notification
	 * @param idPublication the id publication
	 * @param model the model
	 * @param request the request
	 * @param redirectAttrs the redirect attrs
	 * @return the string
	 */
	@RequestMapping(value="/cancel", method = RequestMethod.POST, produces = "text/html")
    public String cancel(@ModelAttribute("notification") Notification notification, 
    		@RequestParam("idPublication") Long idPublication,
    		Model model, HttpServletRequest request, final RedirectAttributes redirectAttrs) {
        model.asMap().clear();
        try {
        	Publication publication = Publication.findPublication(idPublication);
        	User user = publication.getCreator().getUser();
        	publication.setStatusType(StatusTypeEnum.Cancel);
        	Date date = new Date();
        	notification.setDate(date);
        	notification.setNotificationType(NotificationTypeEnum.PublicactionStatus);
        	notification.setStatus(true);
        	try {
        		Boolean status = uService.checkInternetConnexion();
        		if(status){
        			SimpleMailMessage mail = new SimpleMailMessage();
	        		mail.setTo(user.getEmailAddress());
	        		mail.setSubject("Publicación dada de baja - Esperanza");
	        		mail.setText("Hola "+user.getEmailAddress()+",\n. Su publicación ha sido dada de baja."+notification.getDescription()+" \n Muchas Gracias Administración de Esperanza");
	        		mailSender.send(mail);
        		}	
        	} catch (IOException e) {
        		e.printStackTrace();
        	}
        	updatePublicationStatus(publication, notification);
		} catch (Exception e) {
			e.printStackTrace();
		}
        redirectAttrs.addFlashAttribute("cancelPublication", "true");
        return "redirect:/publications/list";
    }
	
	/**
	 * Update publication status.
	 *
	 * @param publication the publication
	 * @param notification the notification
	 * @param mail the mail
	 */
	@Transactional
	private void updatePublicationStatus(Publication publication, Notification notification){
		publication.merge();
    	notification.persist();
	}
	
	/**
	 * Adds the date time format patterns.
	 *
	 * @param model the model
	 */
	void addDateTimeFormatPatterns(Model model) {
		model.addAttribute("publication_date_date_format", DateTimeFormat.patternForStyle("M-", LocaleContextHolder.getLocale()));
    }

}
