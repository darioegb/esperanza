package com.roo.esperanza.web;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.gvnix.addon.web.mvc.addon.jquery.GvNIXWebJQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.roo.esperanza.domain.Donation;
import com.roo.esperanza.domain.Necessity;
import com.roo.esperanza.domain.Notification;
import com.roo.esperanza.domain.Participant;
import com.roo.esperanza.domain.Person;
import com.roo.esperanza.domain.Publication;
import com.roo.esperanza.domain.Puntuation;
import com.roo.esperanza.provider.ScriptAddService;
import com.roo.esperanza.provider.UtilityService;
import com.roo.esperanza.reference.DonationStatusTypeEnum;
import com.roo.esperanza.reference.NotificationTypeEnum;
import com.roo.esperanza.reference.ScoreTypeEnum;

// TODO: Auto-generated Javadoc
/**
 * The Class DonationController.
 */
@RequestMapping("/donation/**")
@Controller
@RooWebScaffold(path = "donation", formBackingObject = Donation.class)
@GvNIXWebJQuery
public class DonationController {
	
	/** The sa service. */
	@Autowired 
	ScriptAddService saService;
	
	/** The u service. */
	@Autowired
	UtilityService uService;
	
	/** The mail sender. */
	@Autowired
	private transient MailSender mailSender;
	
	/**
	 * Update status.
	 *
	 * @param hash the hash
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value = "accept", params = "q", method = RequestMethod.GET, produces = "text/html")
    public String accept(@RequestParam(value="q", required = true) String hash, Model model, final RedirectAttributes redirectAttrs) {
        model.asMap().clear();
        Donation donation = Donation.findDonationByHash(hash);
        String hashNecessity = uService.hashSha256(donation.getPublication().getId().toString());
        donation.setDonationStatusType(DonationStatusTypeEnum.Accept);
        try {
        	Boolean status = uService.checkInternetConnexion();
        	if(status){
        		SimpleMailMessage mail = new SimpleMailMessage();
        		mail.setTo(donation.getDonor().getUser().getEmailAddress());
        		mail.setSubject("Aceptación de donación - Esperanza");
        		mail.setText("Hola "+donation.getDonor().getUser().getEmailAddress()+",\n. El participante "+donation.getReceiver().getUser().getEmailAddress()+" aceptó su donación a la brevedad se comunicara con usted para acordar la entrega.");
        		mailSender.send(mail);
        	}
        } catch (IOException e) {
        	e.printStackTrace();
        }
        donation.merge();
        redirectAttrs.addFlashAttribute("acceptDonation", "true");
        return "redirect:/necessitys?q="+hashNecessity;
    }

	/**
	 * Delete.
	 *
	 * @param id the id
	 * @param page the page
	 * @param size the size
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value = "cancel", params = "q", method = RequestMethod.GET, produces = "text/html")
    public String cancel(@RequestParam(value="q", required = true) String hash, Model model, final RedirectAttributes redirectAttrs) {
        model.asMap().clear();
        Donation donation = Donation.findDonationByHash(hash);
        String hashNecessity = uService.hashSha256(donation.getPublication().getId().toString());
        donation.setDonationStatusType(DonationStatusTypeEnum.Cancel);
        try {
        	Boolean status = uService.checkInternetConnexion();
        	if(status){
        		SimpleMailMessage mail = new SimpleMailMessage();
        		mail.setTo(donation.getDonor().getUser().getEmailAddress());
        		mail.setSubject("Aceptación de donación - Esperanza");
        		mail.setText("Hola "+donation.getDonor().getUser().getEmailAddress()+",\n. El participante "+donation.getReceiver().getUser().getEmailAddress()+"no aceptó su donación a la brevedad se comunicara con usted para informarle el porque del rechazo.");
        		mailSender.send(mail);
        	}
        } catch (IOException e) {
        	e.printStackTrace();
        }
        donation.merge();
        redirectAttrs.addFlashAttribute("cancelDonation", "true");
        return "redirect:/necessitys?q="+hashNecessity;
    }
	
	/**
	 * Show.
	 *
	 * @param hash the hash
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(params = "q", produces = "text/html")
    public String show(@RequestParam(value="q", required = true) String hash, Model model) {
        addDateTimeFormatPatterns(model);
        Donation donation = Donation.findDonationByHash(hash);
        model.addAttribute("donation", donation);
        model.addAttribute("itemId",donation.getId());
        return "donation/show";
    }
	

    /**
     * Creates the.
     *
     * @param donation the donation
     * @param donorId the donor id
     * @param necessityId the necessity id
     * @param model the model
     * @param request the request
     * @param redirectAttrs the redirect attrs
     * @return the string
     */
    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@ModelAttribute("donation") Donation donation, 
    		@RequestParam("donorId") Long donorId, @RequestParam("dataId") Long necessityId, Model model,
    		HttpServletRequest request, final RedirectAttributes redirectAttrs) {
        try {
            model.asMap().clear();
            Person donor = Person.findPerson(donorId);
            donation.setDonor(donor);
            Necessity necessity = Necessity.findNecessity(necessityId);
            Participant participant = necessity.getCreator();
            donation.setReceiver(participant);
            Date date = new Date();
            donation.setDate(date);
            donation.setDonationStatusType(DonationStatusTypeEnum.Opened);
            donation.setPublication(necessity);
            participant.getDonations().add(donation);
            String hash = uService.hashSha256(donor.getId().toString());
            Notification notification = new Notification();
            notification.setDescription("el participante "+donor.getFirstName()+" "+donor.getLastName()+" quiere donarle "+ donation.getDescription());
            notification.setDate(date);
            notification.setNotificationType(NotificationTypeEnum.DonationReceived);
            notification.setStatus(false);
            notification.setUser(participant.getUser());
            try {
            	Boolean status = uService.checkInternetConnexion();
            	if(status){
	            	SimpleMailMessage mail = new SimpleMailMessage();
	        		mail.setTo(participant.getUser().getEmailAddress());
	        		mail.setSubject("Inicio de donación - Esperanza");
	        		mail.setText("Hola "+participant.getUser().getEmailAddress()+",\n. El participante "+donor.getUser().getEmailAddress()+" le escribio:\n "+donation.getDescription()+" para enviarle un mensaje ingrese el siguiente link ya estando logueado en el sitio. <a href=\"http://localhost:8080/Esperanza/people?q="+hash+"\">Enlace de activación</a>");
	                mailSender.send(mail);
            	}
            } catch (IOException e) {
            	e.printStackTrace();
            }
            participant.merge();
    		notification.persist();
            redirectAttrs.addFlashAttribute("createDonation", "true");
            return "redirect:/necessitys?find=ByDescriptionLikeOrDonationTypeOrPriorityTypeOrDateBetween&description=&participantFilter=&donationType=&priorityType=";
        } catch (Exception e) {
            redirectAttrs.addFlashAttribute("errorDonation", "true");
            return "redirect:/necessitys?find=ByDescriptionLikeOrDonationTypeOrPriorityTypeOrDateBetween&description=&participantFilter=&donationType=&priorityType=";
        }
    }

	/**
	 * Find donations by receiver or date between or description like or donation type.
	 *
	 * @param minDate the min date
	 * @param maxDate the max date
	 * @param description the description
	 * @param personFilter the person filter
	 * @param page the page
	 * @param size the size
	 * @param sortFieldName the sort field name
	 * @param sortOrder the sort order
	 * @param model the model
	 * @param request the request
	 * @return the string
	 */
	@RequestMapping(params = "find=ByReceiverOrDateBetweenOrDescriptionLikeOrPublication", method = RequestMethod.GET)
    public String findDonationsByReceiverOrDateBetweenOrDescriptionLikeOrDonationType(@RequestParam(value="minDate", required=false) Date minDate, 
    		@RequestParam(value="maxDate", required=false) Date maxDate, 
    		@RequestParam(value="description", required = false) String description,
    		@RequestParam(value="personFilter", required = false) String personFilter,
    		@RequestParam(value="publication", required = false) Long publicationId,
    		@RequestParam(value = "page", required = false) Integer page, 
    		@RequestParam(value = "size", required = false) Integer size, 
    		@RequestParam(value = "sortFieldName", required = false) String sortFieldName, 
    		@RequestParam(value = "sortOrder", required = false) String sortOrder, Model model,HttpServletRequest request) {
		HttpSession sessionObj = request.getSession();
		Participant receiver = Participant.findParticipant((Long)sessionObj.getAttribute("id"));
		if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            if(page == null && size != null){
    			model.addAttribute("donations", Donation.findLastDonationsByReceiver(receiver, sizeNo));
            }else{
            	final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            	model.addAttribute("donations", Donation.findDonationsByReceiverOrDateBetweenOrDescriptionLikeOrPersonLikeOrDonationTypeOrPublication(receiver, minDate, maxDate, description, personFilter, publicationId, sortFieldName, sortOrder).setFirstResult(firstResult).setMaxResults(sizeNo).getResultList());
            	float nrOfPages = (float) Donation.countFindDonationsByReceiverOrDateBetweenOrDescriptionLikeOrPersonLikeOrDonationTypeOrPublication(receiver, minDate, maxDate, description, personFilter, publicationId) / sizeNo;
            	model.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
            }
        } else {
    		if(description.length() == 0 && publicationId == null && personFilter.length() == 0 && ((minDate == null && maxDate == null) || (minDate.compareTo(maxDate) == 0))){
    			model.addAttribute("donations", Donation.findAllDonationsByReceiver(receiver, sortFieldName, sortOrder));
    		}else{
    			model.addAttribute("donations", Donation.findDonationsByReceiverOrDateBetweenOrDescriptionLikeOrPersonLikeOrDonationTypeOrPublication(receiver, minDate, maxDate, description, personFilter, publicationId, sortFieldName, sortOrder).getResultList());
    		}
    	}
		saService.agregarJS("general/modal.js");
        saService.publicarJsCss(model);
		model.addAttribute("scoretypeenums", Arrays.asList(ScoreTypeEnum.values()));
		model.addAttribute("puntuation", new Puntuation());
		model.addAttribute("notification", new Notification());
        addDateTimeFormatPatterns(model);
        return "donation/list";
    }

	/**
	 * Find donations by receiver or date between or description like or donation type form.
	 *
	 * @param model the model
	 * @param request the request
	 * @return the string
	 */
	@RequestMapping(params = { "find=ByReceiverOrDateBetweenOrDescriptionLikeOrPublication", "form" }, method = RequestMethod.GET)
    public String findDonationsByReceiverOrDateBetweenOrDescriptionLikeOrDonationTypeForm(Model model, HttpServletRequest request) {
		HttpSession sessionObj = request.getSession();
		Participant creator = Participant.findParticipant((Long)sessionObj.getAttribute("id"));
		saService.agregarJS("general/removeDateTimesFinder.js");
        saService.publicarJsCss(model);
        addDateTimeFormatPatterns(model);
		model.addAttribute("publications", Publication.findAllPublicationsByCreator(creator));
        return "donation/findDonationsByReceiverOrDateBetweenOrDescriptionLikeOrPublication";
    }
}
