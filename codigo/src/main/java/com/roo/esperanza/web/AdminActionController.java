package com.roo.esperanza.web;
import com.roo.esperanza.domain.AdminAction;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * The Class AdminActionController.
 */
@RequestMapping("/adminactions")
@Controller
@RooWebScaffold(path = "adminactions", formBackingObject = AdminAction.class)
public class AdminActionController {
}
