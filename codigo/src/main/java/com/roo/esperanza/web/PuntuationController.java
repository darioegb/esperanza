package com.roo.esperanza.web;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.roo.esperanza.domain.Participant;
import com.roo.esperanza.domain.Person;
import com.roo.esperanza.domain.Puntuation;

// TODO: Auto-generated Javadoc
/**
 * The Class PuntuationController.
 */
@RequestMapping("/puntuations")
@Controller
@RooWebScaffold(path = "puntuations", formBackingObject = Puntuation.class)
public class PuntuationController {

	/**
	 * Creates the.
	 *
	 * @param puntuation the puntuation
	 * @param PersonId the person id
	 * @param model the model
	 * @param request the request
	 * @param redirectAttrs the redirect attrs
	 * @return the string
	 */
	@RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@ModelAttribute("puntuation") Puntuation puntuation,
    		@RequestParam("donorId") String[] PersonIds, Model model, HttpServletRequest request, 
    		final RedirectAttributes redirectAttrs) {
		try {
        	model.asMap().clear();
        	System.out.println(puntuation);
        	for (String personId : PersonIds) {
        		Puntuation puntuationParticular = new Puntuation();
        		puntuationParticular.setComment(puntuation.getComment());
        		puntuationParticular.setScoreType(puntuation.getScoreType());
	        	Person person = (Person) Participant.findParticipantByHash(personId);
	        	Date date = new Date();
	        	puntuationParticular.setDate(date);
	        	person.getArrayScore().add(puntuationParticular);
	        	person.merge();
        	}
            redirectAttrs.addFlashAttribute("createPuntuation", "true");
            return "redirect:/donation?find=ByReceiverOrDateBetweenOrDescriptionLikeOrDonationType&description=&personFilter=&donationType=";
		} catch (Exception e) {
			redirectAttrs.addFlashAttribute("errorPuntuation", "true");
			return "redirect:/donation?find=ByReceiverOrDateBetweenOrDescriptionLikeOrDonationType&description=&personFilter=&donationType=";
		}
    }

	/**
	 * List.
	 *
	 * @param page the page
	 * @param size the size
	 * @param sortFieldName the sort field name
	 * @param sortOrder the sort order
	 * @param idParticipant the id participant
	 * @param uiModel the ui model
	 * @return the string
	 */
	@RequestMapping(produces = "text/html")
    public String list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, 
    		@RequestParam(value = "sortFieldName", required = false) String sortFieldName, @RequestParam(value = "sortOrder", required = false) String sortOrder, 
    		@RequestParam(value = "idParticipant", required = true) Long idParticipant, Model uiModel) {
        Participant participant = Participant.findParticipant(idParticipant);
		if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            uiModel.addAttribute("puntuations", Puntuation.findPuntuationEntries(participant, firstResult, sizeNo, sortFieldName, sortOrder));
            float nrOfPages = (float) Puntuation.countPuntuations(participant) / sizeNo;
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            uiModel.addAttribute("puntuations", Puntuation.findAllPuntuations(participant, sortFieldName, sortOrder));
        }
        addDateTimeFormatPatterns(uiModel);
        return "puntuations/list";
    }
}
