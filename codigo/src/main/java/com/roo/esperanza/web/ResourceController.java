package com.roo.esperanza.web;
import org.springframework.roo.addon.web.mvc.controller.finder.RooWebFinder;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.roo.esperanza.domain.Denouncement;
import com.roo.esperanza.domain.Resource;

// TODO: Auto-generated Javadoc
/**
 * The Class ResourceController.
 */
@RequestMapping("/resources")
@Controller
@RooWebScaffold(path = "resources", formBackingObject = Resource.class)
@RooWebFinder
public class ResourceController {

	/**
	 * Show.
	 *
	 * @param hash the hash
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(params = "q", produces = "text/html")
    public String show(@RequestParam(value="q", required = true) String hash, Model model) {
		Denouncement denouncement = Denouncement.findDenouncementByHash(hash);
		model.addAttribute("resource", Resource.findResourcesByDenouncement(denouncement).getSingleResult());
		model.addAttribute("idDenouncement", denouncement.getId());
        return "resources/show";
    }
}
