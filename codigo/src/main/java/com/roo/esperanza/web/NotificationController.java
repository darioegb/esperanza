package com.roo.esperanza.web;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.gvnix.addon.web.mvc.addon.jquery.GvNIXWebJQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.roo.addon.web.mvc.controller.finder.RooWebFinder;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.roo.esperanza.domain.Conversation;
import com.roo.esperanza.domain.Donation;
import com.roo.esperanza.domain.Institution;
import com.roo.esperanza.domain.Message;
import com.roo.esperanza.domain.Notification;
import com.roo.esperanza.domain.Participant;
import com.roo.esperanza.domain.Person;
import com.roo.esperanza.domain.User;
import com.roo.esperanza.provider.UtilityService;
import com.roo.esperanza.reference.DonationStatusTypeEnum;
import com.roo.esperanza.reference.NotificationTypeEnum;

// TODO: Auto-generated Javadoc
/**
 * The Class NotificationController.
 */
@RequestMapping("/notifications")
@Controller
@RooWebScaffold(path = "notifications", formBackingObject = Notification.class)
@RooWebFinder
@GvNIXWebJQuery
public class NotificationController {
	
	/** The u service. */
	@Autowired
	UtilityService uService;

	/**
	 * Find notifications by status not or notification type.
	 *
	 * @param notificationType the notification type
	 * @param page the page
	 * @param size the size
	 * @param sortFieldName the sort field name
	 * @param sortOrder the sort order
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(params = "find=ByStatusNotOrNotificationType", method = RequestMethod.GET)
    public String findNotificationsByStatusNotOrNotificationType(@RequestParam("notificationType") NotificationTypeEnum notificationType, 
    		@RequestParam(value = "page", required = false) Integer page, 
    		@RequestParam(value = "size", required = false) Integer size, 
    		@RequestParam(value = "sortFieldName", required = false) String sortFieldName, 
    		@RequestParam(value = "sortOrder", required = false) String sortOrder,
    		Model model) {
		if(sortFieldName == null && sortOrder == null){
        	sortFieldName = "date";
        	sortOrder = "DESC";
        }
		UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		User user = User.findUsersByEmailAddress(userDetails.getUsername()).getSingleResult();
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            model.addAttribute("notifications", Notification.findNotificationsByNotificationTypeAndUser(notificationType, user, sortFieldName, sortOrder).setFirstResult(firstResult).setMaxResults(sizeNo).getResultList());
            float nrOfPages = (float) Notification.countFindNotificationsByNotificationTypeAndUser(notificationType, user) / sizeNo;
            model.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
        	model.addAttribute("notifications", Notification.findNotificationsByNotificationTypeAndUser(notificationType, user, sortFieldName, sortOrder).getResultList());
        }
        addDateTimeFormatPatterns(model);
        return "notifications/list";
    }
	
	/**
	 * Creates the.
	 *
	 * @param notification the notification
	 * @param participantId the participant id
	 * @param model the model
	 * @param request the request
	 * @param redirectAttrs the redirect attrs
	 * @return the string
	 */
	@RequestMapping(value={"/claim", "/thanks"}, method = RequestMethod.POST, produces = "text/html")
    public String create(@ModelAttribute("notification") Notification notification, 
    		@RequestParam("dataId") String[] donationIds, @RequestParam("donorId") String[] donorIds, Model model,
    		HttpServletRequest request, final RedirectAttributes redirectAttrs) {
        try {
        	for (int i = 0; i < donationIds.length; i++) {
	        	String uri = request.getRequestURI();
	        	HttpSession sessionObj = request.getSession();
	            model.asMap().clear();
	            Notification notificationParticular = new Notification();
	            notificationParticular.setDescription(notification.getDescription());
	            notificationParticular.setNotificationType(notification.getNotificationType());
	            Participant participant = Participant.findParticipantByHash(donorIds[i]);
	            User user = participant.getUser();
	    		Participant sessionParticipant = Participant.findParticipant((Long) sessionObj.getAttribute("id"));
	    		Donation donation = Donation.findDonationByHash(donationIds[i]);
	    		notificationParticular.setUser(user);
	            Date date = new Date();
	            notificationParticular.setDate(date);
	            notificationParticular.setStatus(false);
	            Message message = new Message();
	            message.setDate(date);
	        	message.setMessage(notificationParticular.getDescription());
	        	message.setStatus(false);
	            if(uri.contains("claim")){
	            	donation.setDonationStatusType(DonationStatusTypeEnum.NotReceived);
	            	if(sessionParticipant.getUser().getRol().getRoleType().toString().equals("ROLE_PERSON")){
	            		notificationParticular.setDescription("Ha recibido un reclamo: "+notificationParticular.getDescription()+" de la donación "+donation.getDescription()+" del participante "+((Person) sessionParticipant).getFirstName()+" "+((Person) sessionParticipant).getLastName());
	            	}else{
	            		notificationParticular.setDescription("Ha recibido un reclamo: "+notificationParticular.getDescription()+" de la donación "+donation.getDescription()+" del participante "+((Institution) sessionParticipant).getSocialReason());
	            	}
	            }else{
	            	donation.setDonationStatusType(DonationStatusTypeEnum.Received);
	            	if(sessionParticipant.getUser().getRol().getRoleType().toString().equals("ROLE_PERSON")){
	            		notificationParticular.setDescription("Ha recibido un agradecimiento: "+notificationParticular.getDescription()+" de la donación "+donation.getDescription()+" del participante "+((Person) sessionParticipant).getFirstName()+" "+((Person) sessionParticipant).getLastName());
	            	}else{
	            		notificationParticular.setDescription("Ha recibido un agradecimiento: "+notificationParticular.getDescription()+" de la donación "+donation.getDescription()+" del participante "+((Institution) sessionParticipant).getSocialReason());
	            	}
	            }
	            user.getNotifications().add(notificationParticular);
	            if(Conversation.checkConversationByParticipants(sessionParticipant, participant).getResultList().size() == 0 || (Conversation.checkConversationByParticipants(sessionParticipant, participant).getResultList().size() == 1 && Conversation.checkConversationByParticipants(sessionParticipant, participant).getResultList().get(0).getSender().equals(participant))){
	            	Conversation conversation = new Conversation();
	            	conversation.setReceiver(participant);
	            	conversation.setSender(sessionParticipant);
	                message.setConversation(conversation);
	                conversation.persist();
	            }else{
	            	Conversation conversation = Conversation.findConversationsBySenderAndReceiver(sessionParticipant, participant).getSingleResult();
	            	message.setConversation(conversation);
	            }
	            donation.merge();
	            notificationParticular.persist();
	            message.persist();
        	}
            redirectAttrs.addFlashAttribute("createNotification", "true");
            return "redirect:/donation?find=ByReceiverOrDateBetweenOrDescriptionLikeOrPublication&description=&personFilter=&donationType=";
		} catch (Exception e) {
			redirectAttrs.addFlashAttribute("errorNotification", "true");
			return "redirect:/donation?find=ByReceiverOrDateBetweenOrDescriptionLikeOrPublication&description=&personFilter=&donationType=";
		}
    }
	
	/**
	 * Show notification.
	 *
	 * @param id the id
	 * @param model the model
	 * @param request the request
	 * @return the string
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "text/html")
    public String showNotification(@PathVariable("id") Long id, Model model, HttpServletRequest request) {
		HttpSession sessionObj = request.getSession();
		String returnUri = "";
		Notification notification = Notification.findNotification(id);
		if(notification.getStatus() == false){
			notification.setStatus(true);
	    	notification.merge();
		}
		switch (notification.getNotificationType()) {
		case DonationReceived:
			Participant participant = Participant.findParticipant((Long) sessionObj.getAttribute("id")); 
			returnUri = "redirect:/donation?q="+uService.hashSha256(Donation.findDonationByReceiverAndDate(participant, notification.getDate()).getSingleResult().getId().toString());
			break;

		default:
			model.addAttribute("notification", notification);
	        model.addAttribute("itemId", id);
	        returnUri = "notifications/show";
			break;
		}
        return returnUri;

    }
	
	/**
	 * Gets the count no read.
	 *
	 * @param request the request
	 * @param response the response
	 * @param model the model
	 * @return the count no read
	 */
	@RequestMapping(value = "/count", method = RequestMethod.GET, produces = "text/html")
	@ResponseBody
    public String getCountNoRead(HttpServletRequest request,
            HttpServletResponse response, Model model) {
		UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		User user = User.findUsersByEmailAddress(userDetails.getUsername()).getSingleResult();
		Long count = Notification.countFindNotificationsByStatusNotAndUser(user);
		
        return  count.toString();

    }

}
