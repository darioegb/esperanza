package com.roo.esperanza.web;
import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.roo.esperanza.domain.Denouncement;
import com.roo.esperanza.domain.Notification;
import com.roo.esperanza.domain.Participant;
import com.roo.esperanza.domain.Resource;
import com.roo.esperanza.domain.User;
import com.roo.esperanza.provider.UtilityService;
import com.roo.esperanza.reference.NotificationTypeEnum;
import com.roo.esperanza.reference.StatusTypeEnum;

// TODO: Auto-generated Javadoc
/**
 * The Class DenouncementController.
 */
@RequestMapping("/denouncements/**")
@Controller
@RooWebScaffold(path = "denouncements", formBackingObject = Denouncement.class, create = false)
public class DenouncementController {
	
	/** The mail sender. */
	@Autowired
	private transient MailSender mailSender;
	
	/** The u service. */
	@Autowired
	UtilityService uService;

	/**
	 * Creates the.
	 *
	 * @param denouncement the denouncement
	 * @param complainantId the complainant id
	 * @param denoucedId the denouced id
	 * @param model the model
	 * @param request the request
	 * @param redirectAttrs the redirect attrs
	 * @return the string
	 */
	@RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@ModelAttribute("denouncement") Denouncement denouncement, 
    		@RequestParam("complainantId") Long complainantId, @RequestParam("denoucedId") Long denoucedId, 
    		Model model, HttpServletRequest request, final RedirectAttributes redirectAttrs) {
		User  denouced =  User.findUser(denoucedId);
        String url = (denouced.getRol().getId() == 2) ? "?find=ByNecessityDescriptionLikeOrFirstNameLike&description=&firstName=" : "?find=ByNecessityDescriptionOrCategory&description=&category=";
        try {
			model.asMap().clear();
			denouncement.setComplainant(Participant.findParticipant(complainantId).getUser());
			denouncement.setDenouced(denouced);
			Date date = new Date();
			denouncement.setDate(date);
			denouncement.setStatusType(StatusTypeEnum.Create);
			denouncement.persist();
	        redirectAttrs.addFlashAttribute("createDenouncement", "true");
	        return "redirect:/potencialbeneficiary"+url;
		} catch (Exception e) {
			redirectAttrs.addFlashAttribute("errorDenouncement", "true");
			return "redirect:/potencialbeneficiary"+url;
		}        
    }
	
	/**
	 * Accept publication form.
	 *
	 * @param id the id
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value="/accept/{id}", params = "form", produces = "text/html")
    public String acceptPublicationForm(@PathVariable("id") Long id, Model model) {
		model.addAttribute("idDenouncement", id);
		model.addAttribute("notification", new Notification());
        return "denouncements/acceptDenouncement";
    }
	
	/**
	 * Accept.
	 *
	 * @param notification the notification
	 * @param idDenouncement the id denouncement
	 * @param model the model
	 * @param request the request
	 * @param redirectAttrs the redirect attrs
	 * @return the string
	 */
	@RequestMapping(value="/accept", method = RequestMethod.POST, produces = "text/html")
    public String accept(@ModelAttribute("notification") Notification notification, 
    		@RequestParam("idDenouncement") Long idDenouncement, 
    		Model model, HttpServletRequest request, final RedirectAttributes redirectAttrs) {
        model.asMap().clear();
        try {
        	Denouncement denouncement = Denouncement.findDenouncement(idDenouncement);
        	User user = denouncement.getDenouced();
        	Resource resource = Resource.findResourcesByDenouncement(denouncement).getSingleResult();
        	denouncement.setStatusType(StatusTypeEnum.Accept);
        	Date date = new Date();
        	notification.setDate(date);
        	notification.setNotificationType(NotificationTypeEnum.Denouncements);
        	notification.setStatus(true);
    		try {
    			Boolean status = uService.checkInternetConnexion();
    			if(status){
    				SimpleMailMessage mail = new SimpleMailMessage();
        			mail.setTo(user.getEmailAddress());
        			mail.setSubject("Denuncia tomada - Esperanza");
        			mail.setText("Hola "+user.getEmailAddress()+",\n. Ha sido denunciado por otro usuario en relación a su "+resource.getResourceType()+" "+denouncement.getAdminResolution()+".\n Administración de Esperanza");
        			mailSender.send(mail);
    			}
    		} catch (IOException e) {
    			e.printStackTrace();
    		}
        	updateDenouncementStatus(denouncement, notification);
        } catch (Exception e) {
			e.printStackTrace();
		}
        redirectAttrs.addFlashAttribute("acceptDenouncement", "true");
        return "redirect:/denouncement/list";
    }
	
	/**
	 * Cancel publication form.
	 *
	 * @param id the id
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value="/cancel/{id}", produces = "text/html")
    public String cancelPublicationForm(@PathVariable("id") Long id, Model model) {
		model.addAttribute("idDenouncement", id);
		model.addAttribute("notification", new Notification());
        return "denouncements/cancelDenouncement";
    }
	
	/**
	 * Cancel.
	 *
	 * @param notification the notification
	 * @param idDenouncement the id denouncement
	 * @param model the model
	 * @param request the request
	 * @param redirectAttrs the redirect attrs
	 * @return the string
	 */
	@RequestMapping(value="/cancel", method = RequestMethod.POST, produces = "text/html")
    public String cancel(@ModelAttribute("notification") Notification notification, 
    		@RequestParam("idDenouncement") Long idDenouncement,
    		Model model, HttpServletRequest request, final RedirectAttributes redirectAttrs) {
        model.asMap().clear();
        try {
        	Denouncement denouncement = Denouncement.findDenouncement(idDenouncement);
        	User user = denouncement.getComplainant();
        	denouncement.setStatusType(StatusTypeEnum.Cancel);
        	Date date = new Date();
        	notification.setDate(date);
        	notification.setNotificationType(NotificationTypeEnum.Denouncements);
        	notification.setStatus(true);
        	try {
    			Boolean status = uService.checkInternetConnexion();
    			if(status){
    				SimpleMailMessage mail = new SimpleMailMessage();
        			mail.setTo(user.getEmailAddress());
        			mail.setSubject("Denuncia rechazada - Esperanza");
        			mail.setText("Hola "+user.getEmailAddress()+",\n. Su denuncia ha sido rechazada por "+"sobre el recurso "+notification.getDescription()+" \n Muchas Gracias Administración de Esperanza");
            		mailSender.send(mail);
    			}
    		} catch (IOException e) {
    			e.printStackTrace();
    		}
    		updateDenouncementStatus(denouncement, notification);
		} catch (Exception e) {
			e.printStackTrace();
		}
        redirectAttrs.addFlashAttribute("cancelDenouncement", "true");
        return "redirect:/denouncements/list";
    }
	
	/**
	 * Update denouncement status.
	 *
	 * @param denouncement the denouncement
	 * @param notification the notification
	 * @param mail the mail
	 */
	@Transactional
	private void updateDenouncementStatus(Denouncement denouncement, Notification notification){
		denouncement.merge();
    	notification.persist();
	}

	/**
	 * List.
	 *
	 * @param page the page
	 * @param size the size
	 * @param sortFieldName the sort field name
	 * @param sortOrder the sort order
	 * @param uiModel the ui model
	 * @return the string
	 */
	@RequestMapping(value="/list", produces = "text/html")
    public String list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, @RequestParam(value = "sortFieldName", required = false) String sortFieldName, @RequestParam(value = "sortOrder", required = false) String sortOrder, Model uiModel) {
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            uiModel.addAttribute("denouncements", Denouncement.findDenouncementEntries(firstResult, sizeNo, sortFieldName, sortOrder));
            float nrOfPages = (float) Denouncement.countDenouncements() / sizeNo;
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            uiModel.addAttribute("denouncements", Denouncement.findAllDenouncements(sortFieldName, sortOrder));
        }
        addDateTimeFormatPatterns(uiModel);
        return "denouncements/list";
    }
}
