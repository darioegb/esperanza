package com.roo.esperanza.web;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.gvnix.addon.web.mvc.addon.jquery.GvNIXWebJQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.roo.esperanza.domain.Donation;
import com.roo.esperanza.domain.Institution;
import com.roo.esperanza.domain.Necessity;
import com.roo.esperanza.domain.Participant;
import com.roo.esperanza.domain.Person;
import com.roo.esperanza.domain.Publication;
import com.roo.esperanza.provider.ScriptAddService;
import com.roo.esperanza.reference.DonationTypeEnum;
import com.roo.esperanza.reference.PriorityTypeEnum;
import com.roo.esperanza.reference.StatusTypeEnum;

// TODO: Auto-generated Javadoc
/**
 * The Class NecessityController.
 */
@RequestMapping("/necessitys")
@Controller
@RooWebScaffold(path = "necessitys", formBackingObject = Necessity.class)
@GvNIXWebJQuery
public class NecessityController {
	
	/** The sa service. */
	@Autowired 
	ScriptAddService saService;

	/**
	 * Creates the.
	 *
	 * @param necessity the necessity
	 * @param bindingResult the binding result
	 * @param model the model
	 * @param request the request
	 * @param redirectAttrs the redirect attrs
	 * @return the string
	 */
	@RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid Necessity necessity, BindingResult bindingResult, Model model, 
    		HttpServletRequest request, final RedirectAttributes redirectAttrs) {
        if (bindingResult.hasErrors()) {
        	if(((necessity.getLink().length() < 5) && (necessity.getLink().length() > 0))){
        		populateEditFormReducid(model, necessity);
        		return "necessitys/create";
        	}
        }
        model.asMap().clear();
        necessity.setStatusType(StatusTypeEnum.Create);
        setNecessity(request, necessity);
        necessity.persist();
        redirectAttrs.addFlashAttribute("createNecessity", "true");
        return "redirect:/necessitys";
    }

	/**
	 * Creates the form.
	 *
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(params = "form", produces = "text/html")
    public String createForm(Model model) {
		populateEditFormReducid(model, new Necessity());
        return "necessitys/create";
    }

	/**
	 * Update.
	 *
	 * @param necessity the necessity
	 * @param bindingResult the binding result
	 * @param model the model
	 * @param request the request
	 * @param redirectAttrs the redirect attrs
	 * @return the string
	 */
	@RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid Necessity necessity, BindingResult bindingResult, Model model, 
    		HttpServletRequest request, final RedirectAttributes redirectAttrs) {
        if (bindingResult.hasErrors()) {
        	populateEditFormReducid(model, necessity);
            return "necessitys/update";
        }
        model.asMap().clear();
        setNecessity(request, necessity);
        necessity.merge();
        redirectAttrs.addFlashAttribute("updateNecessity", "true");
        return "redirect:/necessitys";
    }
	
	/**
	 * Close.
	 *
	 * @param hash the hash
	 * @param model the model
	 * @param redirectAttrs the redirect attrs
	 * @return the string
	 */
	@RequestMapping(value = "close", params = "q", method = RequestMethod.GET, produces = "text/html")
    public String close(@RequestParam(value="q", required = true) String hash, Model model, final RedirectAttributes redirectAttrs) {
        model.asMap().clear();
        Necessity necessity = (Necessity) Publication.findPublicationByHash(hash);
        necessity.setStatusType(StatusTypeEnum.Finish);
        necessity.merge();
        redirectAttrs.addFlashAttribute("closeNecessity", "true");
        return "redirect:/necessitys";
    }

	/**
	 * Update form.
	 *
	 * @param hash the hash
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(params = {"q", "form"}, produces = "text/html")
    public String updateForm(@RequestParam(value="q", required = true) String hash, Model model) {
		populateEditFormReducid(model, (Necessity) Publication.findPublicationByHash(hash));
        return "necessitys/update";
    }
	
	@RequestMapping(params = "q", produces = "text/html")
    public String show(@RequestParam(value="q", required = true) String hash, Model model) {
		Necessity necessity = (Necessity) Publication.findPublicationByHash(hash);
		Long countDonations = Donation.countFindDonationsByPublication(necessity.getId());
		List<Donation> donations = Donation.findDonationsByPublication(necessity.getId());
        addDateTimeFormatPatterns(model);
        model.addAttribute("necessity", necessity);
        model.addAttribute("countDonations", countDonations);
        model.addAttribute("donations", donations);
        model.addAttribute("itemId", necessity.getId());
        return "necessitys/show";
    }
	
	/**
	 * Populate edit form reducid.
	 *
	 * @param model the model
	 * @param necessity the necessity
	 */
	void populateEditFormReducid(Model model, Necessity necessity) {
		model.addAttribute("necessity", necessity);
        addDateTimeFormatPatterns(model);
        model.addAttribute("donationtypeenums", Arrays.asList(DonationTypeEnum.values()));
        model.addAttribute("prioritytypeenums", Arrays.asList(PriorityTypeEnum.values()));
    }

	 /**
 	 * Sets the necessity.
 	 *
 	 * @param request the request
 	 * @param necessity the necessity
 	 */
 	void setNecessity(HttpServletRequest request, Necessity necessity){
		UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        HttpSession sessionObj = request.getSession();
        if(userDetails.getAuthorities().toArray()[0].toString().equals("ROLE_PERSON")){
        	Person person = Person.findPerson((Long)sessionObj.getAttribute("id"));
        	necessity.setCreator(person);
        }else{
        	Institution institution = Institution.findInstitution((Long)sessionObj.getAttribute("id"));
        	necessity.setCreator(institution);
        }
	}

	/**
	 * Find necessitys by description like or donation type or priority type or date between.
	 *
	 * @param description the description
	 * @param participantFilter the participant filter
	 * @param donationType the donation type
	 * @param priorityType the priority type
	 * @param minDate the min date
	 * @param maxDate the max date
	 * @param page the page
	 * @param size the size
	 * @param sortFieldName the sort field name
	 * @param sortOrder the sort order
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(params = "find=ByDescriptionLikeOrDonationTypeOrPriorityTypeOrDateBetween", method = RequestMethod.GET)
    public String findNecessitysByDescriptionLikeOrDonationTypeOrPriorityTypeOrDateBetween(@RequestParam(value="description", required = false) String description,
    		@RequestParam(value="participantFilter", required = false) String participantFilter,
    		@RequestParam(value="donationType", required = false) DonationTypeEnum donationType, 
    		@RequestParam(value="priorityType", required = false) PriorityTypeEnum priorityType, 
    		@RequestParam(value="minDate", required=false) Date minDate, 
    		@RequestParam(value="maxDate", required=false) Date maxDate, 
    		@RequestParam(value = "page", required = false) Integer page, 
    		@RequestParam(value = "size", required = false) Integer size, 
    		@RequestParam(value = "sortFieldName", required = false) String sortFieldName, 
    		@RequestParam(value = "sortOrder", required = false) String sortOrder, Model model) {
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            model.addAttribute("necessitys", Necessity.findNecessitysByDescriptionLikeOrParticipantLikeOrDonationTypeOrPriorityTypeOrDateBetween(description, participantFilter, donationType, priorityType, minDate, maxDate, sortFieldName, sortOrder).setFirstResult(firstResult).setMaxResults(sizeNo).getResultList());
            float nrOfPages = (float) Necessity.countFindNecessitysByDescriptionLikeOrParticipantLikeOrDonationTypeOrPriorityTypeOrDateBetween(description, participantFilter, donationType, priorityType, minDate, maxDate) / sizeNo;
            model.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
        	if(description.length() == 0 && participantFilter.length() == 0 &&  priorityType == null && donationType == null && ((minDate == null && maxDate == null) || (minDate.compareTo(maxDate) == 0))){
        		model.addAttribute("necessitys", Necessity.findAllNecessitysDistinctUser(sortFieldName, sortOrder));
        	}else{
        		model.addAttribute("necessitys", Necessity.findNecessitysByDescriptionLikeOrParticipantLikeOrDonationTypeOrPriorityTypeOrDateBetween(description, participantFilter, donationType, priorityType, minDate, maxDate, sortFieldName, sortOrder).getResultList());
        	}
        }
        saService.agregarJS("general/modal.js");
        saService.publicarJsCss(model);
        model.addAttribute("donation", new Donation());
        addDateTimeFormatPatterns(model);
        return "necessitys/listsearch";
    }

	/**
	 * List.
	 *
	 * @param page the page
	 * @param size the size
	 * @param sortFieldName the sort field name
	 * @param sortOrder the sort order
	 * @param uiModel the ui model
	 * @param request the request
	 * @return the string
	 */
	@RequestMapping(produces = "text/html")
    public String list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, @RequestParam(value = "sortFieldName", required = false) String sortFieldName, @RequestParam(value = "sortOrder", required = false) String sortOrder, Model uiModel, HttpServletRequest request) {
		HttpSession sessionObj = request.getSession();
		Participant creator = Participant.findParticipant((Long) sessionObj.getAttribute("id"));
		if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            uiModel.addAttribute("necessitys", Necessity.findAllNecessityByCreator(firstResult, sizeNo, sortFieldName, sortOrder, creator));
            float nrOfPages = (float) Necessity.countNecessitys() / sizeNo;
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            uiModel.addAttribute("necessitys", Necessity.findAllNecessitysByCreator(sortFieldName, sortOrder, creator));
        }
        addDateTimeFormatPatterns(uiModel);
        return "necessitys/list";
    }

	/**
	 * Find necessitys by description like or donation type or priority type or date between form.
	 *
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(params = { "find=ByDescriptionLikeOrDonationTypeOrPriorityTypeOrDateBetween", "form" }, method = RequestMethod.GET)
    public String findNecessitysByDescriptionLikeOrDonationTypeOrPriorityTypeOrDateBetweenForm(Model model) {
		saService.agregarJS("general/removeDateTimesFinder.js");
        saService.publicarJsCss(model);
        model.addAttribute("prioritytypeenums", java.util.Arrays.asList(PriorityTypeEnum.class.getEnumConstants()));
        model.addAttribute("donationtypeenums", java.util.Arrays.asList(DonationTypeEnum.class.getEnumConstants()));
        addDateTimeFormatPatterns(model);
        return "necessitys/findNecessitysByDescriptionLikeOrDonationTypeOrPriorityTypeOrDateBetween";
    }
	
}
