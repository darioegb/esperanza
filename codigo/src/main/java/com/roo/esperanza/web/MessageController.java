package com.roo.esperanza.web;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.gvnix.addon.web.mvc.addon.jquery.GvNIXWebJQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.roo.addon.web.mvc.controller.json.RooWebJson;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.roo.esperanza.domain.Conversation;
import com.roo.esperanza.domain.Message;
import com.roo.esperanza.domain.Participant;
import com.roo.esperanza.domain.User;
import com.roo.esperanza.provider.ScriptAddService;

// TODO: Auto-generated Javadoc
/**
 * The Class MessageController.
 */
@RequestMapping("/messages/**")
@Controller
@RooWebScaffold(path = "messages", formBackingObject = Message.class)
@GvNIXWebJQuery
@RooWebJson(jsonObject = Message.class)
public class MessageController {
	
	/** The sa service. */
	@Autowired 
	ScriptAddService saService;

    /**
     * Creates the.
     *
     * @param message the message
     * @param participantReceiverId the participant receiver id
     * @param model the model
     * @param request the request
     * @return the string
     */
    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@ModelAttribute("msj") Message message,@RequestParam("participantReceiverId")Long participantReceiverId, 
    		Model model, HttpServletRequest request) {
    	HttpSession sessionObj = request.getSession();
        model.asMap().clear();
        Participant receiver = Participant.findParticipant(participantReceiverId);
        Participant sender = Participant.findParticipant((Long) sessionObj.getAttribute("id"));
        Conversation conversation = null;
        Date date = new Date();
        message.setDate(date);
        message.setStatus(false);
        message.setConversation(getOrCreateConversation(sender, receiver, conversation));
        message.persist();
        return "redirect:/messages";
    }
    
    @RequestMapping(params="new", method = RequestMethod.POST, produces = "text/html")
	@ResponseBody
    public String create(@RequestParam("message") String text, @RequestParam("participantReceiverId")Long participantReceiverId, 
    		HttpServletRequest request) {
    		HttpSession sessionObj = request.getSession();
        	Message message = new Message();
        	message.setMessage(text);
            Participant receiver = Participant.findParticipant(participantReceiverId);
            Participant sender = Participant.findParticipant((Long) sessionObj.getAttribute("id"));
            Conversation conversation = null;
            Date date = new Date();
            message.setDate(date);
            message.setStatus(false);
            try {
            	message.setConversation(getOrCreateConversation(sender, receiver, conversation));
            } catch (EmptyResultDataAccessException e) {
            	conversation = newConversation(sender, receiver, conversation);
            	message.setConversation(conversation);
            } catch (Exception e) {
    			return "Error";
    		}
            message.persist();
        	return "Ok";
    }
    
    private Conversation getOrCreateConversation(Participant sender, Participant receiver, Conversation conversation){
    	if(Conversation.checkConversationByParticipants(sender, receiver).getResultList().size() != 0){
            conversation = Conversation.findConversationsBySenderAndReceiver(sender, receiver).setMaxResults(1).getSingleResult();      	
        }else{
        	conversation = newConversation(sender, receiver, conversation);
        }
		return conversation;
    }
    
    private Conversation newConversation(Participant sender, Participant receiver, Conversation conversation){
    	conversation = new Conversation();
    	conversation.setReceiver(receiver);
    	conversation.setSender(sender);
        conversation.persist();
        return conversation;
    }

    /**
     * Update.
     *
     * @param message the message
     * @param bindingResult the binding result
     * @param model the model
     * @param request the request
     * @return the string
     */
    @RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid Message message, BindingResult bindingResult, Model model, HttpServletRequest request) {
        if (bindingResult.hasErrors()) {
            populateEditForm(model, message);
            return "messages/update";
        }
        model.asMap().clear();
        Date date = new Date();
        message.setDate(date);
        message.merge();
        return "redirect:/messages";
    }

    /**
     * Update form.
     *
     * @param id the id
     * @param model the model
     * @return the string
     */
    @RequestMapping(value = "/{id}", params = "form", produces = "text/html")
    public String updateForm(@PathVariable("id") Long id, Model model) {
        populateEditForm(model, Message.findMessage(id));
        return "messages/update";
    }
    
    /**
     * List json.
     *
     * @param participantId the participant id
     * @param request the request
     * @return the response entity
     */
    @RequestMapping(params="list", headers = "Accept=application/json")
    @ResponseBody
    public ResponseEntity<String> listJson(@RequestParam("participantId") Long participantId, HttpServletRequest request) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        try {
        	HttpSession sessionObj = request.getSession();
            Participant sessionParticipant = Participant.findParticipant((Long) sessionObj.getAttribute("id"));
            Participant participant = Participant.findParticipant(participantId);
            List<Message> result = Message.findMessagesByParticipants(sessionParticipant, participant).getResultList();
            for (Iterator<Message> iterator = result.iterator(); iterator.hasNext();) {
				Message message = (Message) iterator.next();
				if(message.getStatus() == false){
					message.setStatus(true);
					message.merge();
				}
			}
            return new ResponseEntity<String>(Message.toJsonArray(result), headers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<String>("{\"ERROR\":"+e.getMessage()+"\"}", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    

    /**
     * List conversations.
     *
     * @param page the page
     * @param size the size
     * @param sortFieldName the sort field name
     * @param sortOrder the sort order
     * @param model the model
     * @param request the request
     * @return the string
     */
    @RequestMapping(produces = "text/html")
    public String list(@RequestParam(value = "page", required = false) Integer page, 
    		@RequestParam(value = "size", required = false) Integer size, @RequestParam(value = "sortFieldName", required = false) String sortFieldName, 
    		@RequestParam(value = "sortOrder", required = false) String sortOrder, Model model, HttpServletRequest request) {
    	HttpSession sessionObj = request.getSession();
        Participant participant = Participant.findParticipant((Long) sessionObj.getAttribute("id"));
        List<Conversation> conversations = null; 
    	if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            conversations = Conversation.findConversationsBySenderOrReceiver(participant, participant).setFirstResult(firstResult).setMaxResults(sizeNo).getResultList();
        	model.addAttribute("conversations", conversations);
            float nrOfPages = (float) Conversation.countFindConversationsBySenderOrReceiver(participant, participant) / sizeNo;
            model.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
        	conversations = Conversation.findConversationsBySenderOrReceiver(participant, participant).getResultList();
        	model.addAttribute("conversations", conversations);
        }
    	saService.agregarJS("general/messagesFind.js");
        saService.publicarJsCss(model);
        if(conversations.size() > 0){
        	model.addAttribute("messages", Message.findMessagesByParticipants(conversations.get(0).getSender(), conversations.get(0).getReceiver()).getResultList());
        	model.addAttribute("msj", new Message());
        }
        addDateTimeFormatPatterns(model);
        
        return "messages/list";
    }
    
    /**
     * Gets the count no read.
     *
     * @param request the request
     * @param response the response
     * @param model the model
     * @return the count no read
     */
    @RequestMapping(value = "/count", method = RequestMethod.GET, produces = "text/html")
	@ResponseBody
    public String getCountNoRead(HttpServletRequest request,
            HttpServletResponse response, Model model) {
		UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		User user = User.findUsersByEmailAddress(userDetails.getUsername()).getSingleResult();
		Long count = Message.countFindMessagesByStatusNotAndUser(user);
		 return count.toString();
    }
}
