package com.roo.esperanza.web;

import java.io.IOException;
import java.util.Random;

import javax.persistence.TypedQuery;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.authentication.encoding.MessageDigestPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import com.roo.esperanza.domain.User;
import com.roo.esperanza.provider.UtilityService;

// TODO: Auto-generated Javadoc
/**
 * The Class ForgotPasswordController.
 */
@RequestMapping("/forgotpassword/**")
@Controller
public class ForgotPasswordController {
	
	/** The u service. */
	@Autowired
	UtilityService uService;

    /** The mail sender. */
    @Autowired
    private transient MailSender mailSender;

    /** The simple mail message. */
    private transient SimpleMailMessage simpleMailMessage;

	/** The message digest password encoder. */
	@Autowired
	private MessageDigestPasswordEncoder messageDigestPasswordEncoder;

    /**
     * Form backing object.
     *
     * @return the forgot password form
     */
    @ModelAttribute("forgotpasswordForm")
    public ForgotPasswordForm formBackingObject() {
        return new ForgotPasswordForm();
    }

    /**
     * Index.
     *
     * @return the string
     */
    @RequestMapping
    public String index() {
        return "forgotpassword/index";
    }

    /**
     * Thanks.
     *
     * @return the string
     */
    @RequestMapping(value = "/thanks", produces = "text/html")
    public String thanks() {
        return "forgotpassword/thanks";
    }

    /**
     * Update.
     *
     * @param form the form
     * @param result the result
     * @return the string
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST, produces = "text/html")
    public String update(@Valid @ModelAttribute("forgotpasswordForm") ForgotPasswordForm form, BindingResult result) {
        if (result.hasErrors()) {
        	System.out.println(result);
        	return "forgotpassword/index";
        } else {
        	TypedQuery<User> userQuery=User.findUsersByEmailAddress(form.getEmailAddress());
        	if(null!=userQuery && userQuery.getMaxResults()>0){
        		User User = userQuery.getSingleResult();
        		Random random = new Random(System.currentTimeMillis());
        		String newPassword = "pass"+random.nextLong();
        		User.setPassword(messageDigestPasswordEncoder.encodePassword(newPassword, null));
        		User.merge();
        		try {
        			Boolean status = uService.checkInternetConnexion();
        			if(status){
        				SimpleMailMessage mail = new SimpleMailMessage();
	            		mail.setTo(form.getEmailAddress());
	            		mail.setSubject("Password Recover");
	            		mail.setText("Hola "+User.getEmailAddress()+",\n. Usted solicitó para recuperar la contraseña. Su contraseña es  "+newPassword+". \n Muchas Gracias Administración de Esperanza");
	            		mailSender.send(mail);
	            	}
        		} catch (IOException e) {
        			e.printStackTrace();
        		}
        	}

            return "forgotpassword/thanks";
        }
    }

    /**
     * Send message.
     *
     * @param mailTo the mail to
     * @param message the message
     */
    public void sendMessage(String mailTo, String message) {
        simpleMailMessage.setTo(mailTo);
        simpleMailMessage.setText(message);
        mailSender.send(simpleMailMessage);
    }
}
