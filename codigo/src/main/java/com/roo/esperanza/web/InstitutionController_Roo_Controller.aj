// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.roo.esperanza.web;

import com.roo.esperanza.domain.Address;
import com.roo.esperanza.domain.Category;
import com.roo.esperanza.domain.Donation;
import com.roo.esperanza.domain.Institution;
import com.roo.esperanza.domain.Multimedia;
import com.roo.esperanza.domain.Necessity;
import com.roo.esperanza.domain.Puntuation;
import com.roo.esperanza.domain.Resource;
import com.roo.esperanza.domain.User;
import com.roo.esperanza.web.InstitutionController;
import java.io.UnsupportedEncodingException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.joda.time.format.DateTimeFormat;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

privileged aspect InstitutionController_Roo_Controller {
    
    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String InstitutionController.create(@Valid Institution institution, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, institution);
            return "institutions/create";
        }
        uiModel.asMap().clear();
        institution.persist();
        return "redirect:/institutions/" + encodeUrlPathSegment(institution.getId().toString(), httpServletRequest);
    }
    
    @RequestMapping(params = "form", produces = "text/html")
    public String InstitutionController.createForm(Model uiModel) {
        populateEditForm(uiModel, new Institution());
        return "institutions/create";
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
    public String InstitutionController.delete(@PathVariable("id") Long id, @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        Institution institution = Institution.findInstitution(id);
        institution.remove();
        uiModel.asMap().clear();
        uiModel.addAttribute("page", (page == null) ? "1" : page.toString());
        uiModel.addAttribute("size", (size == null) ? "10" : size.toString());
        return "redirect:/institutions";
    }
    
    void InstitutionController.addDateTimeFormatPatterns(Model uiModel) {
        uiModel.addAttribute("institution_date_date_format", DateTimeFormat.patternForStyle("M-", LocaleContextHolder.getLocale()));
    }
    
    void InstitutionController.populateEditForm(Model uiModel, Institution institution) {
        uiModel.addAttribute("institution", institution);
        addDateTimeFormatPatterns(uiModel);
        uiModel.addAttribute("addresses", Address.findAllAddresses());
        uiModel.addAttribute("categorys", Category.findAllCategorys());
        uiModel.addAttribute("donations", Donation.findAllDonations());
        uiModel.addAttribute("multimedias", Multimedia.findAllMultimedias());
        uiModel.addAttribute("necessitys", Necessity.findAllNecessitys());
        uiModel.addAttribute("puntuations", Puntuation.findAllPuntuations());
        uiModel.addAttribute("resources", Resource.findAllResources());
        uiModel.addAttribute("users", User.findAllUsers());
    }
    
    String InstitutionController.encodeUrlPathSegment(String pathSegment, HttpServletRequest httpServletRequest) {
        String enc = httpServletRequest.getCharacterEncoding();
        if (enc == null) {
            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
        }
        try {
            pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
        } catch (UnsupportedEncodingException uee) {}
        return pathSegment;
    }
    
}
