package com.roo.esperanza.web;
import java.util.Arrays;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.gvnix.addon.web.mvc.addon.jquery.GvNIXWebJQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.roo.addon.web.mvc.controller.finder.RooWebFinder;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.roo.esperanza.domain.DonationGood;
import com.roo.esperanza.domain.DonationRequest;
import com.roo.esperanza.domain.Participant;
import com.roo.esperanza.domain.Person;
import com.roo.esperanza.domain.Publication;
import com.roo.esperanza.provider.ImageService;
import com.roo.esperanza.provider.ScriptAddService;
import com.roo.esperanza.reference.DonationTypeEnum;
import com.roo.esperanza.reference.StatusTypeEnum;

// TODO: Auto-generated Javadoc
/**
 * The Class DonationGoodController.
 */
@RequestMapping("/donationgoods")
@Controller
@RooWebScaffold(path = "donationgoods", formBackingObject = DonationGood.class)
@RooWebFinder
@GvNIXWebJQuery
public class DonationGoodController {
	
	/** The sa service. */
	@Autowired 
	ScriptAddService saService;
	
	/** The i service. */
	@Autowired
	ImageService iService;

	/**
	 * Creates the.
	 *
	 * @param donationGood the donation good
	 * @param photoFile the photo file
	 * @param bindingResult the binding result
	 * @param model the model
	 * @param request the request
	 * @param redirectAttrs the redirect attrs
	 * @return the string
	 */
	@RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@ModelAttribute("donationGood")DonationGood donationGood, @RequestParam("photoFile") MultipartFile photoFile, 
    		BindingResult bindingResult, Model model, HttpServletRequest request, final RedirectAttributes redirectAttrs) {
		if (bindingResult.hasErrors()) {
        	if(((donationGood.getLink().length() < 5) && (donationGood.getLink().length() > 0))){
        		populateEditFormReducid(model, donationGood);
        		return "donationgoods/create";
        	}
        }
        model.asMap().clear();
        setDonationGood(request, donationGood, photoFile, false);
        donationGood.setStatusType(StatusTypeEnum.Create);
        Person person = (Person) donationGood.getCreator();
        person.getDonationGoods().add(donationGood);
        person.merge();
        redirectAttrs.addFlashAttribute("createDonationGood", "true");
        return "redirect:/donationgoods";
    }

	/**
	 * Creates the form.
	 *
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(params = "form", produces = "text/html")
    public String createForm(Model model) {
		populateEditFormReducid(model, new DonationGood());
        return "donationgoods/create";
    }
	
	/**
	 * Populate edit form reducid.
	 *
	 * @param model the model
	 * @param donationGood the donation good
	 */
	void populateEditFormReducid(Model model, DonationGood donationGood) {
        model.addAttribute("donationGood", donationGood);
        addDateTimeFormatPatterns(model);
        model.addAttribute("donationtypeenums", Arrays.asList(DonationTypeEnum.values()));
    }

	/**
	 * Update form.
	 *
	 * @param hash the hash
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(params = {"q", "form"}, produces = "text/html")
    public String updateForm(@RequestParam(value="q", required = true) String hash, Model model) {
		DonationGood donationGood  = (DonationGood) Publication.findPublicationByHash(hash);
		populateEditFormReducid(model, donationGood);
		model.addAttribute("picturePath", donationGood.getPhoto());
        return "donationgoods/update";
    }

	/**
	 * Update.
	 *
	 * @param donationGood the donation good
	 * @param photoFile the photo file
	 * @param bindingResult the binding result
	 * @param model the model
	 * @param request the request
	 * @param redirectAttrs the redirect attrs
	 * @return the string
	 */
	@RequestMapping(value="/update", method = RequestMethod.POST, produces = "text/html")
    public String update(@Valid DonationGood donationGood, @RequestParam("photoFile") MultipartFile photoFile, 
    		BindingResult bindingResult, Model model, HttpServletRequest request, final RedirectAttributes redirectAttrs) {
        if (bindingResult.hasErrors()) {
        	populateEditFormReducid(model, donationGood);
            return "donationgoods/update";
        }
        model.asMap().clear();
        try {
        	setDonationGood(request, donationGood, photoFile, true);
            donationGood.merge();

		} catch (Exception e) {
			e.printStackTrace();
		}
        redirectAttrs.addFlashAttribute("updateDonationGood", "true");
        return "redirect:/donationgoods";
    }
	
	 /**
 	 * Sets the donation good.
 	 *
 	 * @param request the request
 	 * @param donationGood the donation good
 	 * @param photoFile the photo file
 	 * @param update the update
 	 */
 	void setDonationGood(HttpServletRequest request, DonationGood donationGood, MultipartFile photoFile, Boolean update){
		HttpSession sessionObj = request.getSession();
		Person person = Person.findPerson((Long)sessionObj.getAttribute("id"));
		if(update)	iService.deleteFile(donationGood.getPhoto());
		donationGood.setCreator(person);
		Boolean statusImageUpload = iService.UploadFile(photoFile, person.getUser().getId(), 2);
		if(statusImageUpload){	
			donationGood.setPhoto(iService.getRelativePathImage());
		}
	}

	/**
	 * Find donation goods by description like or donation type or date between.
	 *
	 * @param description the description
	 * @param donationType the donation type
	 * @param minDate the min date
	 * @param maxDate the max date
	 * @param page the page
	 * @param size the size
	 * @param sortFieldName the sort field name
	 * @param sortOrder the sort order
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(params = "find=ByDescriptionLikeOrDonationTypeOrDateBetween", method = RequestMethod.GET)
    public String findDonationGoodsByDescriptionLikeOrDonationTypeOrDateBetween(@RequestParam(value="description", required = false) String description, 
    		@RequestParam(value="donationType", required = false) DonationTypeEnum donationType, 
    		@RequestParam(value="minDate", required=false)Date minDate, 
    		@RequestParam(value="maxDate", required=false)Date maxDate, 
    		@RequestParam(value = "page", required = false) Integer page, 
    		@RequestParam(value = "size", required = false) Integer size, 
    		@RequestParam(value = "sortFieldName", required = false) String sortFieldName, 
    		@RequestParam(value = "sortOrder", required = false) String sortOrder, Model model) {
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            model.addAttribute("donationgoods", DonationGood.findDonationGoodsByDescriptionLikeOrDonationTypeOrDateBetween(description, donationType, minDate, maxDate, sortFieldName, sortOrder).setFirstResult(firstResult).setMaxResults(sizeNo).getResultList());
            float nrOfPages = (float) DonationGood.countFindDonationGoodsByDescriptionLikeOrDonationTypeOrDateBetween(description, donationType, minDate, maxDate) / sizeNo;
            model.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
        	if(description.length() == 0 && donationType == null && ((minDate == null && maxDate == null) || (((minDate == null && maxDate == null) || (minDate.compareTo(maxDate) == 0))))){
        		model.addAttribute("donationgoods", DonationGood.findAllDonationGoodsDistinctUser(sortFieldName, sortOrder));
        	}else{
        		model.addAttribute("donationgoods", DonationGood.findDonationGoodsByDescriptionLikeOrDonationTypeOrDateBetween(description, donationType, minDate, maxDate, sortFieldName, sortOrder).getResultList());
        	}
        }
        saService.agregarJS("general/modal.js");
        saService.publicarJsCss(model);
        model.addAttribute("donationRequest", new DonationRequest());
        addDateTimeFormatPatterns(model);
        return "donationgoods/listsearch";
    }

	/**
	 * List.
	 *
	 * @param page the page
	 * @param size the size
	 * @param sortFieldName the sort field name
	 * @param sortOrder the sort order
	 * @param uiModel the ui model
	 * @param request the request
	 * @return the string
	 */
	@RequestMapping(produces = "text/html")
    public String list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, @RequestParam(value = "sortFieldName", required = false) String sortFieldName, @RequestParam(value = "sortOrder", required = false) String sortOrder, Model uiModel, HttpServletRequest request) {
		HttpSession sessionObj = request.getSession();
		Participant creator = Participant.findParticipant((Long) sessionObj.getAttribute("id"));
		if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            uiModel.addAttribute("donationgoods", DonationGood.findAllDonationGoodsByCreator(firstResult, sizeNo, sortFieldName, sortOrder, creator));
            float nrOfPages = (float) DonationGood.countDonationGoods() / sizeNo;
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            uiModel.addAttribute("donationgoods", DonationGood.findAllDonationGoodsByCreator(sortFieldName, sortOrder, creator));
        }
        addDateTimeFormatPatterns(uiModel);
        return "donationgoods/list";
    }

	/**
	 * Find donation goods by description like or donation type or date between form.
	 *
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(params = { "find=ByDescriptionLikeOrDonationTypeOrDateBetween", "form" }, method = RequestMethod.GET)
    public String findDonationGoodsByDescriptionLikeOrDonationTypeOrDateBetweenForm(Model model) {
		saService.agregarJS("general/removeDateTimesFinder.js");
        saService.publicarJsCss(model);
        model.addAttribute("donationtypeenums", java.util.Arrays.asList(DonationTypeEnum.class.getEnumConstants()));
        addDateTimeFormatPatterns(model);
        return "donationgoods/findDonationGoodsByDescriptionLikeOrDonationTypeOrDateBetween";
    }
}
