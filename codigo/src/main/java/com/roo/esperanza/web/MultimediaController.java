package com.roo.esperanza.web;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.gvnix.addon.web.mvc.addon.jquery.GvNIXWebJQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.roo.esperanza.domain.Institution;
import com.roo.esperanza.domain.Multimedia;
import com.roo.esperanza.provider.ImageService;
import com.roo.esperanza.provider.UtilityService;
import com.roo.esperanza.reference.MultimediaTypeEnum;

// TODO: Auto-generated Javadoc
/**
 * The Class MultimediaController.
 */
@RequestMapping("/multimedias/**")
@Controller
@RooWebScaffold(path = "multimedias", formBackingObject = Multimedia.class)
@GvNIXWebJQuery
public class MultimediaController {
	
	/** The i service. */
	@Autowired
	ImageService iService;
	
	/** The u service. */
	@Autowired
	UtilityService uService;

	/**
	 * Creates the image.
	 *
	 * @param multimedia the multimedia
	 * @param multimediaFile the multimedia file
	 * @param model the model
	 * @param request the request
	 * @param redirectAttrs the redirect attrs
	 * @return the string
	 */
	@RequestMapping(value="/image", method = RequestMethod.POST, produces = "text/html")
    public String createImage(@ModelAttribute("multimedia") Multimedia multimedia, 
    		@RequestParam("multimediaFile") MultipartFile multimediaFile, Model model, 
    		HttpServletRequest request, final RedirectAttributes redirectAttrs) {
		model.asMap().clear();
    	HttpSession sessionObj = request.getSession();
    	Long id = (Long) sessionObj.getAttribute("id");
		Boolean statusImageUpload = iService.UploadFile(multimediaFile, id, 1);
		if(statusImageUpload){
			multimedia.setMultimediaType(MultimediaTypeEnum.Image);
			multimedia.setPath(iService.getRelativePathImage());
	        Institution institution = Institution.findInstitution(id);
	        institution.getArrayMutimedia().add(multimedia);
	        redirectAttrs.addFlashAttribute("updateMultimedia", "true");
	        institution.merge();
	        
		}
        return "redirect:/multimedias/list";
    }

	/**
	 * Creates the image form.
	 *
	 * @param request the request
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value="/image", params = "form", produces = "text/html")
    public String createImageForm(HttpServletRequest request, Model model) {
        populateEditForm(model, new Multimedia());
        return "multimedias/createimage";
    }
	
	/**
	 * Creates the video.
	 *
	 * @param multimedia the multimedia
	 * @param model the model
	 * @param request the request
	 * @param redirectAttrs the redirect attrs
	 * @return the string
	 */
	@RequestMapping(value="/video", method = RequestMethod.POST, produces = "text/html")
    public String createVideo(@ModelAttribute("multimedia") Multimedia multimedia, Model model, 
    		HttpServletRequest request, final RedirectAttributes redirectAttrs) {
		model.asMap().clear();
    	HttpSession sessionObj = request.getSession();
    	Long id = (Long) sessionObj.getAttribute("id");
		multimedia.setMultimediaType(MultimediaTypeEnum.Video);
		multimedia.setPath(uService.urlEmbedYouTube(multimedia.getPath()));
        Institution institution = Institution.findInstitution(id);
        institution.getArrayMutimedia().add(multimedia);
        institution.merge();
        redirectAttrs.addFlashAttribute("updateMultimedia", "true");   
        return "redirect:/multimedias/list";
    }

	/**
	 * Creates the video form.
	 *
	 * @param request the request
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value="/video", params = "form", produces = "text/html")
    public String createVideoForm(HttpServletRequest request, Model model) {
        populateEditForm(model, new Multimedia());
        return "multimedias/createvideo";
    }


	/**
	 * List.
	 *
	 * @param page the page
	 * @param size the size
	 * @param sortFieldName the sort field name
	 * @param sortOrder the sort order
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value="/list", produces = "text/html")
    public String list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, 
    		@RequestParam(value = "sortFieldName", required = false) String sortFieldName, 
    		@RequestParam(value = "sortOrder", required = false) String sortOrder, Model model) {
    	if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            model.addAttribute("multimedias", Multimedia.findMultimediaEntries(firstResult, sizeNo, sortFieldName, sortOrder));
            float nrOfPages = (float) Multimedia.countMultimedias() / sizeNo;
            model.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
        	model.addAttribute("multimedias", Multimedia.findAllMultimedias(sortFieldName, sortOrder));
        }
        return "multimedias/list";
    }
}
