package com.roo.esperanza.web;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.gvnix.addon.web.mvc.addon.jquery.GvNIXWebJQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.roo.addon.web.mvc.controller.finder.RooWebFinder;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.security.authentication.encoding.MessageDigestPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.roo.esperanza.domain.ConfigNotification;
import com.roo.esperanza.domain.Role;
import com.roo.esperanza.domain.User;
import com.roo.esperanza.reference.RoleTypeEnum;

// TODO: Auto-generated Javadoc
/**
 * The Class UserController.
 */
@RooWebScaffold(path = "users", formBackingObject = User.class)
@RequestMapping("/users")
@Controller
@RooWebFinder
@GvNIXWebJQuery
public class UserController {

    /** The message digest password encoder. */
    @Autowired
    private MessageDigestPasswordEncoder messageDigestPasswordEncoder;

    /**
     * Creates the.
     *
     * @param user the user
     * @param result the result
     * @param model the model
     * @param request the request
     * @return the string
     */
    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid User user, BindingResult result, Model model, HttpServletRequest request) {
        if (result.hasErrors()) {
            model.addAttribute("user", user);
            addDateTimeFormatPatterns(model);
            return "users/create";
        }
        if (user.getId() != null) {
            User savedUser = User.findUser(user.getId());
            if (!savedUser.getPassword().equals(user.getPassword())) {
                user.setPassword(messageDigestPasswordEncoder.encodePassword(user.getPassword(), null));
            }
        } else {
            user.setPassword(messageDigestPasswordEncoder.encodePassword(user.getPassword(), null));
        }
        if(user.getRol().getRoleType() == RoleTypeEnum.ROLE_ADMIN){
        	ConfigNotification configNotifications = new ConfigNotification();
        	configNotifications.setReceiveMails(true);
        	configNotifications.setDenouncements(true);
        	configNotifications.setPublications(true);
        }
        user.persist();
        return "redirect:/users/" + encodeUrlPathSegment(user.getId().toString(), request);
    }
    
    

	/**
	 * Populate edit form.
	 *
	 * @param uiModel the ui model
	 * @param user the user
	 */
	void populateEditForm(Model uiModel, User user) {
        uiModel.addAttribute("user", user);
        addDateTimeFormatPatterns(uiModel);
        uiModel.addAttribute("confignotifications", ConfigNotification.findAllConfigNotifications());
        uiModel.addAttribute("roles", Role.findAllRoles().subList(0, 1));
    }
	
	/**
	 * Update.
	 *
	 * @param user the user
	 * @param bindingResult the binding result
	 * @param uiModel the ui model
	 * @param httpServletRequest the http servlet request
	 * @return the string
	 */
	@RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid User user, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, user);
            return "users/update";
        }
        uiModel.asMap().clear();
        user.setPassword(messageDigestPasswordEncoder.encodePassword(user.getPassword(), null));
        user.merge();
        return "redirect:/users/" + encodeUrlPathSegment(user.getId().toString(), httpServletRequest);
    }

	/**
	 * List.
	 *
	 * @param page the page
	 * @param size the size
	 * @param sortFieldName the sort field name
	 * @param sortOrder the sort order
	 * @param uiModel the ui model
	 * @return the string
	 */
	@RequestMapping(produces = "text/html")
    public String list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, @RequestParam(value = "sortFieldName", required = false) String sortFieldName, @RequestParam(value = "sortOrder", required = false) String sortOrder, Model uiModel) {
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            uiModel.addAttribute("users", User.findUserEntriesDistinctAdmin(firstResult, sizeNo, sortFieldName, sortOrder));
            float nrOfPages = (float) User.countUsers() / sizeNo;
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            uiModel.addAttribute("users", User.findAllUsersDistinctAdmin(sortFieldName, sortOrder));
        }
        addDateTimeFormatPatterns(uiModel);
        return "users/list";
    }

	/**
	 * Update form.
	 *
	 * @param hash the hash
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(params = {"q", "form"}, produces = "text/html")
    public String updateForm(@RequestParam(value="q", required = true) String hash, Model model) {
        populateEditForm(model, User.findUserByHash(hash));
        return "users/update";
    }

	/**
	 * Show.
	 *
	 * @param hash the hash
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(params = "q", produces = "text/html")
    public String show(@RequestParam(value="q", required = true) String hash, Model model) {
		User user = User.findUserByHash(hash);
        addDateTimeFormatPatterns(model);
        model.addAttribute("user", user);
        model.addAttribute("itemId", user.getId());
        return "users/show";
    }

}
