/**
 * 
 */
package com.roo.esperanza.web;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;

// TODO: Auto-generated Javadoc
/**
 * The Class ForgotPasswordForm.
 *
 * @author rohit
 */
public class ForgotPasswordForm {
	
	/** The email address. */
	@Email
	@Size(min=3, max=65)
	private String emailAddress;

	/**
	 * Gets the email address.
	 *
	 * @return the emailAddress
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * Sets the email address.
	 *
	 * @param emailAddress            the emailAddress to set
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

}
