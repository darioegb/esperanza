package com.roo.esperanza.web;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.gvnix.addon.web.mvc.addon.jquery.GvNIXWebJQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.roo.addon.web.mvc.controller.finder.RooWebFinder;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.roo.esperanza.domain.Address;
import com.roo.esperanza.domain.Category;
import com.roo.esperanza.domain.Country;
import com.roo.esperanza.domain.Denouncement;
import com.roo.esperanza.domain.Institution;
import com.roo.esperanza.domain.Message;
import com.roo.esperanza.domain.Multimedia;
import com.roo.esperanza.domain.Participant;
import com.roo.esperanza.domain.ProfileImageDataUpdateForm;
import com.roo.esperanza.domain.Province;
import com.roo.esperanza.domain.User;
import com.roo.esperanza.provider.ImageService;
import com.roo.esperanza.provider.ScriptAddService;
import com.roo.esperanza.reference.GenreTypeEnum;
import com.roo.esperanza.reference.MultimediaTypeEnum;

// TODO: Auto-generated Javadoc
/**
 * The Class InstitutionController.
 */
@RequestMapping("/institutions/**")
@Controller
@RooWebScaffold(path = "institutions", formBackingObject = Institution.class)
@RooWebFinder
@GvNIXWebJQuery
public class InstitutionController {
	
	/** The sa service. */
	@Autowired 
	ScriptAddService saService;
	  
	/** The i service. */
	@Autowired
	ImageService iService;
	
	/**
	 * Update profile picture.
	 *
	 * @param profileImageForm the profile image form
	 * @param profilePicture the profile picture
	 * @param bindingResult the binding result
	 * @param model the model
	 * @param request the request
	 * @param redirectAttrs the redirect attrs
	 * @return the string
	 */
	@RequestMapping(value = "/updateprofilepicture", method = RequestMethod.POST, produces = "text/html")
	public String updateProfilePicture(@Valid @ModelAttribute("profileImageForm") ProfileImageDataUpdateForm profileImageForm, 
			@RequestParam("profilePicture") MultipartFile profilePicture, BindingResult bindingResult, Model model, 
			HttpServletRequest request, final RedirectAttributes redirectAttrs) {
		UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Institution institution = Institution.findInstitution(profileImageForm.getIdUser());
		if (bindingResult.hasErrors()) {
		  populateEditForm(model, institution, userDetails);
		  return "institutions/update";
		}
		model.asMap().clear();
		iService.deleteFile(profileImageForm.getOldName());
		Boolean statusImageUpload = iService.UploadFile(profilePicture, profileImageForm.getIdUser(), 0);
		if(statusImageUpload){	
			institution.setProfilePicture(iService.getRelativePathImage());
			institution.merge();
		}
		redirectAttrs.addFlashAttribute("updateParticipant", "true");
		return "redirect:/institutions?form";
    }

	/**
	 * Update.
	 *
	 * @param institution the institution
	 * @param emailAddress the email address
	 * @param countryId the country id
	 * @param provinceId the province id
	 * @param city the city
	 * @param addressDirection the address direction
	 * @param postalCode the postal code
	 * @param profilePicturePath the profile picture path
	 * @param bindingResult the binding result
	 * @param model the model
	 * @param httpServletRequest the http servlet request
	 * @param redirectAttrs the redirect attrs
	 * @return the string
	 */
	@RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid Institution institution, @RequestParam("emailAddress")String emailAddress, @RequestParam("country")Long countryId, 
		  @RequestParam("province")Long provinceId, @RequestParam("city")String city, @RequestParam("addressDirection")String addressDirection, 
	      @RequestParam("postalCode")String postalCode, @RequestParam("profilePicturePath") String profilePicturePath, 
	      BindingResult bindingResult, Model model, HttpServletRequest httpServletRequest, final RedirectAttributes redirectAttrs) {
		UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (bindingResult.hasErrors()) {
            populateEditForm(model, institution, userDetails);
            return "institutions/update";
        }
        model.asMap().clear();
        User user = User.findUsersByEmailAddress(emailAddress).getSingleResult();
	  	Address address = Institution.findInstitutionsByUser(user).getSingleResult().getAddress();
	  	address.setAddress(addressDirection);
	  	address.setCity(city);
	  	address.setCountry(Country.findCountry(countryId));
	  	address.setPostalCode(postalCode);
	  	address.setProvince(Province.findProvince(provinceId));
        institution.setUser(user);
        institution.setAddress(address);
        institution.setProfilePicture(profilePicturePath);
        institution.merge();
        redirectAttrs.addFlashAttribute("updateParticipant", "true");
        return "redirect:/institutions?form";
    }

	/**
	 * Update form.
	 *
	 * @param request the request
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(params = "form", method = RequestMethod.GET, produces = "text/html")
    public String updateForm(HttpServletRequest request, Model model) {
		saService.agregarJS("general/ajax.js");
	    saService.publicarJsCss(model);
	    UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	    HttpSession sessionObj = request.getSession();
	    Institution institution = Institution.findInstitution((Long)sessionObj.getAttribute("id")); 
	    populateEditForm(model, institution, userDetails);
        return "institutions/update";
    }
	
	/**
	 * Populate edit form.
	 *
	 * @param model the model
	 * @param institution the institution
	 * @param userDetails the user details
	 */
	void populateEditForm(Model model, Institution institution,UserDetails userDetails){
		model.addAttribute("profileImageForm", new ProfileImageDataUpdateForm());
	    model.addAttribute("profilePicturePath", institution.getProfilePicture());
	    model.addAttribute("emailAddress", userDetails.getUsername());
	    model.addAttribute("institution", institution);
	    List<Country> countries = Country.findAllCountrys();
	    List<Province> provinces = Province.findProvincesByCountry(institution.getAddress().getCountry()).getResultList();
	    model.addAttribute("countries", countries);
	    model.addAttribute("provinces", provinces);
	    model.addAttribute("genretypeenums", Arrays.asList(GenreTypeEnum.values()));
	    List<Category> categorys = Category.findAllCategorys("name", "ASC");
	    model.addAttribute("categorys", categorys);
	  }

	/**
	 * Show.
	 *
	 * @param hash the hash
	 * @param model the model
	 * @param request the request
	 * @return the string
	 */
	@RequestMapping(params = "q", produces = "text/html")
    public String show(@RequestParam(value="q", required = true) String hash, Model model, HttpServletRequest request) {
		saService.agregarJS("general/modal.js");
	    saService.publicarJsCss(model);
		HttpSession sessionObj = request.getSession();
        addDateTimeFormatPatterns(model);
        Institution institution = (Institution) Participant.findParticipantByHash(hash);
        model.addAttribute("institution", institution);
        model.addAttribute("denouncement", new Denouncement());
        model.addAttribute("msj", new Message());
        model.addAttribute("userId", (Long) sessionObj.getAttribute("id"));
        model.addAttribute("id", institution.getId());
        model.addAttribute("imageGallery", Multimedia.findMultimediasByMultimediaTypeAndInstitution(MultimediaTypeEnum.Image, institution.getId()).getResultList());
        model.addAttribute("videoGallery", Multimedia.findMultimediasByMultimediaTypeAndInstitution(MultimediaTypeEnum.Video, institution.getId()).getResultList());
        return "institutions/show";
    }

	/**
	 * List.
	 *
	 * @param page the page
	 * @param size the size
	 * @param sortFieldName the sort field name
	 * @param sortOrder the sort order
	 * @param uiModel the ui model
	 * @return the string
	 */
	@RequestMapping(value="/list", produces = "text/html")
    public String list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, @RequestParam(value = "sortFieldName", required = false) String sortFieldName, @RequestParam(value = "sortOrder", required = false) String sortOrder, Model uiModel) {
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            uiModel.addAttribute("institutions", Institution.findInstitutionEntries(firstResult, sizeNo, sortFieldName, sortOrder));
            float nrOfPages = (float) Institution.countInstitutions() / sizeNo;
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            uiModel.addAttribute("institutions", Institution.findAllInstitutions(sortFieldName, sortOrder));
        }
        addDateTimeFormatPatterns(uiModel);
        return "institutions/list";
    }
}
