package com.roo.esperanza.web;
import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.roo.esperanza.domain.Contact;
import com.roo.esperanza.domain.Institution;
import com.roo.esperanza.domain.Person;
import com.roo.esperanza.domain.User;
import com.roo.esperanza.provider.UtilityService;

// TODO: Auto-generated Javadoc
/**
 * The Class IndexController.
 */
@RequestMapping({"/contact/**", "/aboutas/**", "/"})
@Controller
public class IndexController {
	
	/** The u service. */
	@Autowired
	UtilityService uService;
	
	/** The mail sender. */
	@Autowired
    private transient MailSender mailSender;
	
	/**
	 * Send mail to contact.
	 *
	 * @param contact the contact
	 * @param result the result
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value = "contact", method = RequestMethod.POST, produces = "text/html")
    public String sendMailToContact(@Valid @ModelAttribute("contactForm") Contact contact,
    		BindingResult result, Map<String, Object> model) {
		if (result.hasErrors()) {
			System.out.println(result);
			return "contact"; // back to form
		}
		try {
			Boolean status = uService.checkInternetConnexion();
			if(status){
				SimpleMailMessage mail = new SimpleMailMessage();
				mail.setTo("darioegb@gmail.com");
				mail.setCc(contact.getEmailAddress());
				mail.setSubject("User Question");
				mail.setText("User :\n"+ contact.getName() + "email:\n"+ contact.getEmailAddress() + " said:\n" + contact.getDescription());
		        mailSender.send(mail);
				model.put("sendMail", "true");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "contact";
    }
	
	/**
	 * Contact.
	 *
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value = "contact", method = RequestMethod.GET, produces = "text/html")
    public String contact(Map<String, Object> model) {
        Contact contactForm = new Contact();  
        model.put("contactForm", contactForm);
         
        return "contact";
    }
    
    /**
     * Aboutas.
     *
     * @return the string
     */
    @RequestMapping(value = "aboutas", produces = "text/html")
    public String aboutas() {
        return "aboutas";
    }
    
    /**
     * Home.
     *
     * @param request the request
     * @return the string
     */
    @RequestMapping(value = "/", produces = "text/html")
    public String home(HttpServletRequest request) {
    	UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	HttpSession sessionObj = request.getSession(); 
    	if(userDetails.getAuthorities().toArray()[0].toString().equals("ROLE_PERSON")){
    		sessionObj.setAttribute("id", Person.findPeopleByUser(User.findUsersByEmailAddress(userDetails.getUsername()).getSingleResult()).getSingleResult().getId());
    	}else if(userDetails.getAuthorities().toArray()[0].toString().equals("ROLE_INSTITUTION")){
    		sessionObj.setAttribute("id", Institution.findInstitutionsByUser(User.findUsersByEmailAddress(userDetails.getUsername()).getSingleResult()).getSingleResult().getId());
    	}
        return "index";
    }
}
