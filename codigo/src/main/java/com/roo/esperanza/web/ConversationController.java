package com.roo.esperanza.web;
import com.roo.esperanza.domain.Conversation;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * The Class ConversationController.
 */
@RequestMapping("/corversation")
@Controller
@RooWebScaffold(path = "corversation", formBackingObject = Conversation.class)
public class ConversationController {
}
