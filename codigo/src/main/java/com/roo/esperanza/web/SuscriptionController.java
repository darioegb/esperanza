package com.roo.esperanza.web;
import com.roo.esperanza.domain.Suscription;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.roo.addon.web.mvc.controller.finder.RooWebFinder;
import org.gvnix.addon.web.mvc.addon.jquery.GvNIXWebJQuery;

// TODO: Auto-generated Javadoc
/**
 * The Class SuscriptionController.
 */
@RequestMapping("/suscriptions/**")
@Controller
@RooWebScaffold(path = "suscriptions", formBackingObject = Suscription.class)
@RooWebFinder
@GvNIXWebJQuery
public class SuscriptionController {

	/**
	 * Find suscriptions by state not form.
	 *
	 * @param uiModel the ui model
	 * @return the string
	 */
	@RequestMapping(params = { "find=ByStateNot", "form" }, method = RequestMethod.GET)
    public String findSuscriptionsByStateNotForm(Model uiModel) {
        return "suscriptions/index";
    }
}
