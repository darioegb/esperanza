package com.roo.esperanza.web;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.MessageDigestPasswordEncoder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.roo.esperanza.domain.ChangePasswordForm;
import com.roo.esperanza.domain.ConfigNotification;
import com.roo.esperanza.domain.Institution;
import com.roo.esperanza.domain.Person;
import com.roo.esperanza.domain.User;
import com.roo.esperanza.provider.ChangePasswordValidator;

// TODO: Auto-generated Javadoc
/**
 * The Class ConfigurationController.
 */
@RequestMapping("/configuration/**")
@Controller
public class ConfigurationController {
	
	/** The validator. */
	@Autowired
	private ChangePasswordValidator validator;

	/** The message digest password encoder. */
	@Autowired
	private MessageDigestPasswordEncoder messageDigestPasswordEncoder;

	/**
	 * Index.
	 *
	 * @return the string
	 */
	@RequestMapping
    public String index() {
        return "configuration/index";
    }
	
	/**
	 * Change password form.
	 *
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value = "/changepassword", method = RequestMethod.GET, produces = "text/html")
	public String changePasswordForm(Model model) {
		if (SecurityContextHolder.getContext().getAuthentication()
				.isAuthenticated()) {
			model.addAttribute("changePasswordForm", new ChangePasswordForm());
			return "configuration/changepassword";
		} else {
			return "redirect:/login";
		}
	}

	/**
	 * Update.
	 *
	 * @param form the form
	 * @param result the result
	 * @param redirectAttrs the redirect attrs
	 * @return the string
	 */
	@RequestMapping(value = "/changepassword/update", method = RequestMethod.POST, produces = "text/html")
	public String update(@Valid @ModelAttribute("changePasswordForm") ChangePasswordForm form,
			BindingResult result, final RedirectAttributes redirectAttrs) {
		validator.validate(form, result);
		if (result.hasErrors()) {
			return "configuration/changepassword"; // back to form
		} else {
			if (SecurityContextHolder.getContext().getAuthentication()
					.isAuthenticated()) {
				UserDetails userDetails = (UserDetails) SecurityContextHolder
						.getContext().getAuthentication().getPrincipal();
				String newPassword = form.getNewPassword();
				Query query = User
						.findUsersByEmailAddress(userDetails.getUsername());
				User person = (User) query.getSingleResult();
				person.setPassword(messageDigestPasswordEncoder.encodePassword(newPassword, null));
				person.merge();
				redirectAttrs.addFlashAttribute("changePassword", "true");
				return "redirect:/configuration/changepassword";
			} else {
				return "redirect:/login";
			}
		}
	}
    
    /**
     * Hide profile form.
     *
     * @param model the model
     * @return the string
     */
    @RequestMapping(value = "/hideprofile", method = RequestMethod.GET, produces = "text/html")
    public String hideProfileForm(Model model) {
    	UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	User user = User.findUsersByEmailAddress(userDetails.getUsername()).getSingleResult();
    	model.addAttribute("user", user);
    	return "configuration/hideprofile";
    }
    
    /**
     * Hide profile.
     *
     * @param emailAddress the email address
     * @param model the model
     * @param redirectAttrs the redirect attrs
     * @return the string
     */
    @RequestMapping(value = "/hideprofile", method = RequestMethod.POST, produces = "text/html")
    public String hideProfile(@RequestParam("emailAddress")String emailAddress, Model model, final RedirectAttributes redirectAttrs) {
    	User user = User.findUsersByEmailAddress(emailAddress).getSingleResult();
    	Boolean hide = (user.getEnabled()) ? false : true;
    	String redirect = (user.getEnabled()) ? "hideProfile" : "showProfile";
    	user.setEnabled(hide);
    	user.merge();
    	redirectAttrs.addFlashAttribute(redirect, "true");
    	return "redirect:/configuration/hideprofile";
    }
    
    /**
     * Config notification person update form.
     *
     * @param model the model
     * @param request the request
     * @return the string
     */
    @RequestMapping(value = "/confignotifications",  method = RequestMethod.GET, params = "form", produces = "text/html")
    public String ConfigNotificationPersonUpdateForm(Model model, HttpServletRequest request) {
    	UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	if(userDetails.getAuthorities().toArray()[0].toString().equals("ROLE_PERSON")){
    		Person person = Person.findPeopleByUser(User.findUsersByEmailAddress(userDetails.getUsername()).getSingleResult()).getSingleResult();
    		model.addAttribute("configNotification", person.getUser().getConfigNotification());
    	}else if(userDetails.getAuthorities().toArray()[0].toString().equals("ROLE_INSTITUTION")){
    		Institution institution = Institution.findInstitutionsByUser(User.findUsersByEmailAddress(userDetails.getUsername()).getSingleResult()).getSingleResult();
    		model.addAttribute("configNotification", institution.getUser().getConfigNotification());
    		System.out.println(institution);
    	}else{
    		User user = User.findUsersByEmailAddress(userDetails.getUsername()).getSingleResult();
    		model.addAttribute("configNotification", user.getConfigNotification());	
    	}
        return "configuration/confignotifications";
    }
    
    /**
     * Config notification person update.
     *
     * @param configNotification the config notification
     * @param bindingResult the binding result
     * @param model the model
     * @param request the request
     * @param redirectAttrs the redirect attrs
     * @return the string
     */
    @RequestMapping(value="/confignotifications", method = RequestMethod.PUT, produces = "text/html")
    public String ConfigNotificationPersonUpdate(@Valid ConfigNotification configNotification, 
    		BindingResult bindingResult, Model model, HttpServletRequest request, final RedirectAttributes redirectAttrs) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("configNotification", configNotification);
            return "configuration/confignotifications";
        }
       model.asMap().clear();
        configNotification.merge();
        redirectAttrs.addFlashAttribute("updateConfigNotifications", "true");
        return "redirect:/configuration/confignotifications?form";
    }
}
