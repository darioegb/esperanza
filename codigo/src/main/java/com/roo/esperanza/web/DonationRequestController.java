package com.roo.esperanza.web;
import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.roo.esperanza.domain.Donation;
import com.roo.esperanza.domain.DonationGood;
import com.roo.esperanza.domain.DonationRequest;
import com.roo.esperanza.domain.Notification;
import com.roo.esperanza.domain.Participant;
import com.roo.esperanza.domain.Person;
import com.roo.esperanza.provider.UtilityService;
import com.roo.esperanza.reference.DonationStatusTypeEnum;
import com.roo.esperanza.reference.NotificationTypeEnum;
import com.roo.esperanza.reference.StatusTypeEnum;

// TODO: Auto-generated Javadoc
/**
 * The Class DonationRequestController.
 */
@RequestMapping("/donationrequests/**")
@Controller
@RooWebScaffold(path = "donationrequests", formBackingObject = DonationRequest.class)
public class DonationRequestController {
	
	/** The u service. */
	@Autowired
	UtilityService uService;
	
	/** The mail sender. */
	@Autowired
	private transient MailSender mailSender;
	
	/**
	 * Update status.
	 *
	 * @param hash the hash
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value = "accept", params = "q", method = RequestMethod.GET, produces = "text/html")
    public String updateStatus(@RequestParam(value="q", required = true) String hash, Model model) {
        model.asMap().clear();
        DonationRequest donationRequest = DonationRequest.findDonationRequestByHash(hash);
        donationRequest.setStatusType(StatusTypeEnum.Accept);
        Person donor = (Person)donationRequest.getDonationGood().getCreator();
        Participant receiver = donationRequest.getApplicant();
        DonationGood donationGood = donationRequest.getDonationGood();
        Donation donation = new Donation();
        Date date = new Date();
        donation.setDate(date);
        donation.setDescription(donationGood.getDescription());
        donation.setDonor(donor);
        donation.setDonationStatusType(DonationStatusTypeEnum.Opened);
        donation.setPublication(donationGood);
        donation.setReceiver(receiver);
        receiver.getDonations().add(donation);
        String hashParticipant = uService.hashSha256(donor.getId().toString());
        Notification notification = new Notification();
        notification.setDescription("El participante "+donor.getFirstName()+" "+donor.getLastName()+" acepto donarle "+donationGood.getDescription());
        notification.setDate(date);
        notification.setNotificationType(NotificationTypeEnum.DonationReceived);
        notification.setStatus(false);
        notification.setUser(receiver.getUser());
        try {
        	Boolean status = uService.checkInternetConnexion();
        	if(status){
        		SimpleMailMessage mail = new SimpleMailMessage();
        		mail.setTo(receiver.getUser().getEmailAddress());
        		mail.setSubject("Inicio de donación - Esperanza");
        		mail.setText("Hola "+receiver.getUser().getEmailAddress()+",\n. El participante "+donor.getUser().getEmailAddress()+" le escribio:\n "+donation.getDescription()+" para enviarle un mensaje ingrese el siguiente link ya estando logueado en el sitio. <a href=\"http://localhost:8080/Esperanza/people?q="+hashParticipant+"\">Enlace de activación</a>");
        		mailSender.send(mail);
        	}
        } catch (IOException e) {
        	e.printStackTrace();
        }
        donationRequest.merge();
        receiver.merge();
		notification.persist();
        return "redirect:/donationrequests/list?q="+hash;
    }

	/**
	 * Delete.
	 *
	 * @param id the id
	 * @param page the page
	 * @param size the size
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
    public String delete(@PathVariable("id") Long id, @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model model) {
        model.asMap().clear();
		DonationRequest donationRequest = DonationRequest.findDonationRequest(id);
        donationRequest.setStatusType(StatusTypeEnum.Cancel);
        donationRequest.merge();
        String hash = uService.hashSha256(id.toString());
        return "redirect:/donationrequests/list?q="+hash;
    }

	/**
	 * Creates the.
	 *
	 * @param donationRequest the donation request
	 * @param donationGoodId the donation good id
	 * @param model the model
	 * @param request the request
	 * @param redirectAttrs the redirect attrs
	 * @return the string
	 */
	@RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@ModelAttribute("donationRequest") DonationRequest donationRequest,
    		@RequestParam("dataId") Long donationGoodId, Model model, HttpServletRequest request, 
    		final RedirectAttributes redirectAttrs) {
        try {
        	HttpSession sessionObj = request.getSession();
        	model.asMap().clear();
        	Participant applicant = Participant.findParticipant((Long) sessionObj.getAttribute("id"));
        	donationRequest.setApplicant(applicant);
        	donationRequest.setDonationGood(DonationGood.findDonationGood(donationGoodId));
        	Date date = new Date();
        	donationRequest.setDate(date);
        	donationRequest.setStatusType(StatusTypeEnum.Create);
            donationRequest.persist();
            redirectAttrs.addFlashAttribute("createDonationRequest", "true");
            return "redirect:/donationgoods?find=ByDescriptionLikeOrDonationTypeOrDateBetween&description=&donationType=";
		} catch (Exception e) {
			redirectAttrs.addFlashAttribute("errorDonationRequest", "true");
			return "redirect:/donationgoods?find=ByDescriptionLikeOrDonationTypeOrDateBetween&description=&donationType=";
		}
    }

	/**
	 * List.
	 *
	 * @param page the page
	 * @param size the size
	 * @param sortFieldName the sort field name
	 * @param sortOrder the sort order
	 * @param hash the hash
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value="/list", params = "q", produces = "text/html")
    public String list(@RequestParam(value = "page", required = false) Integer page, 
    		@RequestParam(value = "size", required = false) Integer size, 
    		@RequestParam(value = "sortFieldName", required = false) String sortFieldName, 
    		@RequestParam(value = "sortOrder", required = false) String sortOrder,
    		@RequestParam(value="q", required = true) String hash, Model model) {
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            model.addAttribute("donationrequests", DonationRequest.findDonationRequestsByDonationGoodHash(hash).setFirstResult(firstResult).setMaxResults(sizeNo).getResultList());
            float nrOfPages = (float) DonationRequest.countFindDonationRequestsByDonationGoodHash(hash) / sizeNo;
            model.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
        	model.addAttribute("donationrequests", DonationRequest.findDonationRequestsByDonationGoodHash(hash).getResultList());
        }
        addDateTimeFormatPatterns(model);
        return "donationrequests/list";
    }
}
