package com.roo.esperanza.web;
import com.roo.esperanza.domain.PublicationHistory;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * The Class PublicationHistoryController.
 */
@RequestMapping("/publicationhistorys")
@Controller
@RooWebScaffold(path = "publicationhistorys", formBackingObject = PublicationHistory.class)
public class PublicationHistoryController {
}
