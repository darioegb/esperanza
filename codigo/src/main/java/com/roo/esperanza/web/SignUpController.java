package com.roo.esperanza.web;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.authentication.encoding.MessageDigestPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

import com.roo.esperanza.domain.Address;
import com.roo.esperanza.domain.Category;
import com.roo.esperanza.domain.ConfigNotification;
import com.roo.esperanza.domain.Country;
import com.roo.esperanza.domain.Institution;
import com.roo.esperanza.domain.InstitutionRegistrationForm;
import com.roo.esperanza.domain.Person;
import com.roo.esperanza.domain.PersonRegistrationForm;
import com.roo.esperanza.domain.Role;
import com.roo.esperanza.domain.User;
import com.roo.esperanza.domain.UserRegistrationForm;
import com.roo.esperanza.provider.ImageService;
import com.roo.esperanza.provider.ScriptAddService;
import com.roo.esperanza.provider.SignUpValidator;
import com.roo.esperanza.provider.UtilityService;
import com.roo.esperanza.reference.GenreTypeEnum;
import com.roo.esperanza.reference.RoleTypeEnum;

// TODO: Auto-generated Javadoc
/**
 * The Class SignUpController.
 */
@RequestMapping({"/signup/**", "/completeregister/**"})
@Controller
public class SignUpController {
	
	/** The u service. */
	@Autowired
	UtilityService uService;

    /** The validator. */
    @Autowired
    private SignUpValidator validator;

    /** The mail sender. */
    @Autowired
    private transient MailSender mailSender;

	/** The message digest password encoder. */
	@Autowired
	private MessageDigestPasswordEncoder messageDigestPasswordEncoder;
	
	/** The sa service. */
	@Autowired 
	ScriptAddService saService;
	
	/** The i service. */
	@Autowired
	ImageService iService;

    /**
     * Creates the form.
     *
     * @param model the model
     * @return the string
     */
    @RequestMapping(value= "signup", params = "form", method = RequestMethod.GET, produces = "text/html")
    public String createForm(Model model) {
    	populateEditForm(model, new UserRegistrationForm());
        return "signup/index";
    }
        
    /**
     * Activate user.
     *
     * @param activationKey the activation key
     * @param emailAddress the email address
     * @param model the model
     * @param request the request
     * @return the string
     */
    @RequestMapping(value= "signup", params = "activate", method = RequestMethod.GET, produces = "text/html")
    public String activateUser(@RequestParam(value = "activate", required = true) String activationKey,@RequestParam(value = "emailAddress", required = true) String emailAddress,Model model, HttpServletRequest request) {
        TypedQuery<User> query = User.findUsersByActivationKeyAndEmailAddress(activationKey, emailAddress);
        User user=query.getSingleResult();
        if(null!=user){
        	user.setActivationDate(new Date());
        	user.setEnabled(true);
        	user.merge();
        	if(user.getRol().getId() > 1){
        		HttpSession sessionObj = request.getSession();
        		 sessionObj.setAttribute("emailAddress" , user.getEmailAddress());
        		 sessionObj.setAttribute("roleId" , user.getRol().getId().toString());
        		return "redirect:/completeregister?form";
        	}
        	return "login";
        }
        else{
        	return "signup/error";
        }

    }

    /**
     * Creates the.
     *
     * @param userRegistration the user registration
     * @param result the result
     * @param model the model
     * @param request the request
     * @return the string
     */
    @RequestMapping(value= "signup", method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid @ModelAttribute("User") UserRegistrationForm userRegistration, BindingResult result, Model model, HttpServletRequest request) {
        validator.validate(userRegistration, result);
        if (result.hasErrors()) {
        	System.out.println(result);
            populateEditForm(model, userRegistration);
        	return "signup/index";
        } else {
        	model.asMap().clear();
            Random random = new Random(System.currentTimeMillis());
            String activationKey = "activationKey:" + random.nextInt();

            User user = new User();
            user.setActivationDate(null);
            user.setEmailAddress(userRegistration.getEmailAddress());
            user.setPassword(messageDigestPasswordEncoder.encodePassword(userRegistration.getPassword(), null));
            user.setActivationKey(activationKey);
            user.setEnabled(false);
            user.setLocked(false);
            user.setRol(Role.findRolesByRoleType(userRegistration.getRoleType()).getSingleResult());
            user.persist();
            try {
            	Boolean status = uService.checkInternetConnexion();
            	if(status){
	            	SimpleMailMessage mail = new SimpleMailMessage();
	        		mail.setTo(user.getEmailAddress());
	        		mail.setSubject("Activación de usuario - Esperanza");
	        		mail.setText("Hola "+user.getEmailAddress()+",\n. Usted ha registrado con nosotros. Por favor, haga clic en este enlace para activar tu cuenta - <a href=\"http://localhost:8080/Esperanza/signup?emailAddress="+user.getEmailAddress()+"&activate="+activationKey+"\">Enlace de activación</a>. \n Muchas Gracias Administración de Esperanza");
	                mailSender.send(mail);
            	}
            } catch (IOException e) {
            	e.printStackTrace();
            }
            return "signup/thanks";
        }
    }
    
    /**
     * Creates the pacticipant form.
     *
     * @param model the model
     * @param request the request
     * @return the string
     */
    @RequestMapping(value= "completeregister", params = "form", method = RequestMethod.GET, produces = "text/html")
	public String createPacticipantForm(Model model, HttpServletRequest request) {
		HttpSession sessionObj = request.getSession();
		populateEditForm(model, sessionObj);
		saService.agregarJS("general/ajax.js");
		saService.publicarJsCss(model);
		if(sessionObj.getAttribute("roleId").equals("2")){
        	model.addAttribute("personForm", new PersonRegistrationForm());
        	model.addAttribute("isPerson", true);
        	return "completeregister/index";
    	}
		List<Category> categorys = Category.findAllCategorys("name", "ASC");
		model.addAttribute("categorys", categorys);
		model.addAttribute("institutionForm", new InstitutionRegistrationForm());
         return "completeregister/index";
		
    }
    
    /**
     * Complete register person.
     *
     * @param personForm the person form
     * @param bindingResult the binding result
     * @param model the model
     * @param request the request
     * @param redirectAttrs the redirect attrs
     * @return the string
     */
    @RequestMapping(value = "completeregister/person", method = RequestMethod.POST, produces = "text/html")
    public String completeRegisterPerson(@Valid @ModelAttribute("personForm") PersonRegistrationForm personForm, 
    		BindingResult bindingResult, Model model, HttpServletRequest request, final RedirectAttributes redirectAttrs) {
    	TypedQuery<User> query = User.findUsersByEmailAddress(personForm.getEmailAddress());
       	User user = query.getSingleResult();
        HttpSession sessionObj = request.getSession();
        if (bindingResult.hasErrors()) {
            populateEditForm(model, sessionObj);
            model.addAttribute("personForm", personForm);
            model.addAttribute("isPerson", true);
            return "completeregister/index";
        }
        sessionObj.removeAttribute("emailAddress");
    	sessionObj.removeAttribute("roleId");
        model.asMap().clear();
        Person person = new Person();
        Address address = new Address();
        ConfigNotification configNotifications = new ConfigNotification();
        setPerson(person, personForm, address, user, configNotifications);
        persistPerson(person, address, configNotifications);
        redirectAttrs.addFlashAttribute("registerSuccess", "true");
        return "redirect:/login";
    }
    
    /**
     * Complete register institution.
     *
     * @param institutionForm the institution form
     * @param bindingResult the binding result
     * @param model the model
     * @param request the request
     * @return the string
     */
    @RequestMapping(value = "completeregister/institution", method = RequestMethod.POST, produces = "text/html")
    public String completeRegisterInstitution(@Valid @ModelAttribute("institutionForm") InstitutionRegistrationForm institutionForm, 
    		BindingResult bindingResult, Model model, HttpServletRequest request) {
    	TypedQuery<User> query = User.findUsersByEmailAddress(institutionForm.getEmailAddress());
      	User user = query.getSingleResult();
      	HttpSession sessionObj = request.getSession();
    	if (bindingResult.hasErrors()) {
    		System.out.println(bindingResult);
    		populateEditForm(model, sessionObj);
            model.addAttribute("institutionForm", institutionForm);
            return "completeregister/index";
        }
    	sessionObj.removeAttribute("emailAddress");
    	sessionObj.removeAttribute("roleId");
        model.asMap().clear();
        Institution institution = new Institution();
        Address address = new Address();
        ConfigNotification configNotifications = new ConfigNotification();
      	setInstitution(institution, institutionForm, address, user, configNotifications);
        persistIstitution(institution, address, configNotifications);
        return "redirect:/login";
    }
    
    /**
     * Encode url path segment.
     *
     * @param pathSegment the path segment
     * @param httpServletRequest the http servlet request
     * @return the string
     */
    String encodeUrlPathSegment(String pathSegment, HttpServletRequest httpServletRequest) {
        String enc = httpServletRequest.getCharacterEncoding();
        if (enc == null) {
            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
        }
        try {
            pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
        } catch (UnsupportedEncodingException uee) {}
        return pathSegment;
    }
    
    /**
     * Populate edit form.
     *
     * @param model the model
     * @param userForm the user form
     */
    void populateEditForm(Model model, UserRegistrationForm userForm) {
    	model.addAttribute("roletypeenums", Arrays.asList(RoleTypeEnum.values()).subList(1, 3));
        model.addAttribute("User", userForm);
        model.addAttribute("captcha_form",userForm.getReCaptchaHtml());
    }
    
    /**
     * Populate edit form.
     *
     * @param model the model
     * @param sessionObj the session obj
     */
    void populateEditForm(Model model, HttpSession sessionObj){
		List<Country> countries = Country.findAllCountrys();
		model.addAttribute("countries", countries);
		model.addAttribute("genretypeenums", Arrays.asList(GenreTypeEnum.values()));
		model.addAttribute("emailAddress", sessionObj.getAttribute("emailAddress"));
    }

    /**
     * Thanks.
     *
     * @return the string
     */
    @RequestMapping(value = "/thanks", produces = "text/html")
    public String thanks() {
        return "signup/thanks";
    }
    
    /**
     * Error.
     *
     * @return the string
     */
    @RequestMapping(value = "/error", produces = "text/html")
    public String error() {
        return "signup/error";
    }
    
    /**
     * Sets the person.
     *
     * @param person the person
     * @param personForm the person form
     * @param address the address
     * @param user the user
     * @param configNotifications the config notifications
     */
    void setPerson(Person person, PersonRegistrationForm personForm, Address address, User user, ConfigNotification configNotifications){
    	address.setAddress(personForm.getAddress());
        address.setCity(personForm.getCity());
        address.setProvince(personForm.getProvince());
        address.setCountry(personForm.getCountry());
        address.setPostalCode(personForm.getPostalCode());
        configNotifications.setClaimDonation(true);
        configNotifications.setComplaintReceived(true);
        configNotifications.setDonationGoods(true);
        configNotifications.setDonationReceived(true);
        configNotifications.setNeeds(true);
        configNotifications.setPublicactionStatus(true);
        configNotifications.setReceiveMails(true);
        configNotifications.setDonationRequest(true);
        configNotifications.setStateComplaintFiled(true);
        configNotifications.setStatistics(true);
        configNotifications.setThanksToDonation(true);
        user.setConfigNotification(configNotifications);
        person.setUser(user);
        person.setAddress(address);
        person.setFirstName(personForm.getFirstName());
        person.setLastName(personForm.getLastName());
        person.setPersonalDescription(personForm.getPersonalDescription());
        person.setGenreType(personForm.getGenreType());
        person.setDate(personForm.getDate());
        person.setIdentification(personForm.getIdentification());
        person.setProfilePicture(iService.getDefaultPersonImage());
        person.setPhone(personForm.getPhone());
        person.setCellPhone(personForm.getCellPhone());
    }
    
    /**
     * Sets the institution.
     *
     * @param institution the institution
     * @param institutionForm the institution form
     * @param address the address
     * @param user the user
     * @param configNotifications the config notifications
     */
    void setInstitution(Institution institution, InstitutionRegistrationForm institutionForm, Address address, User user, ConfigNotification configNotifications){
    	address.setAddress(institutionForm.getAddress());
        address.setCity(institutionForm.getCity());
        address.setProvince(institutionForm.getProvince());
        address.setCountry(institutionForm.getCountry());
        address.setPostalCode(institutionForm.getPostalCode());
        configNotifications.setComplaintReceived(true);
        configNotifications.setDonationGoods(true);
        configNotifications.setDonationReceived(true);
        configNotifications.setPublicactionStatus(true);
        configNotifications.setReceiveMails(true);
        configNotifications.setStateComplaintFiled(true);
        configNotifications.setStatistics(true);
        configNotifications.setSuscriptions(true);
        user.setConfigNotification(configNotifications);
        institution.setUser(user);
        institution.setAddress(address);
        institution.setSocialReason(institutionForm.getSocialReason());
        institution.setDate(institutionForm.getDate());
        institution.setProfilePicture(iService.getDefaultInstitutionImage());
        institution.setPhone(institutionForm.getPhone());
        institution.setCellPhone(institutionForm.getCellPhone());
        institution.setCbu(institutionForm.getCbu());
        institution.setShortDescription(institutionForm.getShortDescription());
        institution.setLongDescription(institutionForm.getLongDescription());
        institution.setCategory(institutionForm.getCategory());
    }
    
    /**
     * Persist person.
     *
     * @param person the person
     * @param address the address
     * @param configNotifications the config notifications
     */
    @Transactional
    private void persistPerson(Person person, Address address, ConfigNotification configNotifications){
    	address.persist();
    	configNotifications.persist();
    	person.persist();    	
    }
    
    /**
     * Persist istitution.
     *
     * @param institution the institution
     * @param address the address
     * @param configNotifications the config notifications
     */
    @Transactional
    private void persistIstitution(Institution institution, Address address, ConfigNotification configNotifications){
    	address.persist();
    	configNotifications.persist();
    	institution.persist();    	
    }
    
}
