// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.roo.esperanza.web;

import com.roo.esperanza.domain.Necessity;
import com.roo.esperanza.domain.Participant;
import com.roo.esperanza.domain.PublicationHistory;
import com.roo.esperanza.reference.DonationTypeEnum;
import com.roo.esperanza.reference.PriorityTypeEnum;
import com.roo.esperanza.reference.StatusTypeEnum;
import com.roo.esperanza.web.NecessityController;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import org.joda.time.format.DateTimeFormat;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

privileged aspect NecessityController_Roo_Controller {
       
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
    public String NecessityController.delete(@PathVariable("id") Long id, @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        Necessity necessity = Necessity.findNecessity(id);
        necessity.remove();
        uiModel.asMap().clear();
        uiModel.addAttribute("page", (page == null) ? "1" : page.toString());
        uiModel.addAttribute("size", (size == null) ? "10" : size.toString());
        return "redirect:/necessitys";
    }
    
    void NecessityController.addDateTimeFormatPatterns(Model uiModel) {
        uiModel.addAttribute("necessity_date_date_format", DateTimeFormat.patternForStyle("M-", LocaleContextHolder.getLocale()));
    }
    
    void NecessityController.populateEditForm(Model uiModel, Necessity necessity) {
        uiModel.addAttribute("necessity", necessity);
        addDateTimeFormatPatterns(uiModel);
        uiModel.addAttribute("participants", Participant.findAllParticipants());
        uiModel.addAttribute("publicationhistorys", PublicationHistory.findAllPublicationHistorys());
        uiModel.addAttribute("donationtypeenums", Arrays.asList(DonationTypeEnum.values()));
        uiModel.addAttribute("prioritytypeenums", Arrays.asList(PriorityTypeEnum.values()));
        uiModel.addAttribute("statustypeenums", Arrays.asList(StatusTypeEnum.values()));
    }
    
    String NecessityController.encodeUrlPathSegment(String pathSegment, HttpServletRequest httpServletRequest) {
        String enc = httpServletRequest.getCharacterEncoding();
        if (enc == null) {
            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
        }
        try {
            pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
        } catch (UnsupportedEncodingException uee) {}
        return pathSegment;
    }
    
}
