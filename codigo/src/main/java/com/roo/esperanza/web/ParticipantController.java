package com.roo.esperanza.web;
import javax.servlet.http.HttpServletRequest;

import org.gvnix.addon.web.mvc.addon.jquery.GvNIXWebJQuery;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.roo.esperanza.domain.Category;
import com.roo.esperanza.domain.Institution;
import com.roo.esperanza.domain.Participant;
import com.roo.esperanza.domain.Person;

// TODO: Auto-generated Javadoc
/**
 * The Class ParticipantController.
 */
@RequestMapping({"/potencialbeneficiary/**", "/potencialdonor/**", "participants"})
@Controller
@RooWebScaffold(path = "participants", formBackingObject = Participant.class, create=false, update=false, delete=false)
@GvNIXWebJQuery
public class ParticipantController {
	
	 /**
  	 * Show.
  	 *
  	 * @param hash the hash
  	 * @param model the model
  	 * @param request the request
  	 * @return the string
  	 */
  	@RequestMapping(params = "q", produces = "text/html")
	  public String show(@RequestParam(value="q", required = true) String hash, Model model, HttpServletRequest request) {
  		Participant participant = Participant.findParticipantByHash(hash);
        model.addAttribute("participant", participant);
        model.addAttribute("itemId",participant.getId());
        return "participants/show";
  	}
	
	/**
	 * Find potencial beneficiary form.
	 *
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value = "potencialbeneficiary", params = "form", method = RequestMethod.GET)
    public String findPotencialBeneficiaryForm(Model model) {
        model.addAttribute("categorys", Category.findAllCategorys("name", "ASC"));
        return "participants/findParticipants";
    }
	
	/**
	 * Find people by necessitys or first name like form.
	 *
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value = "potencialdonor", params = "form", method = RequestMethod.GET)
    public String findPeopleByNecessitysOrFirstNameLikeForm(Model model) {
        return "participants/findParticipants";
    }
	
	/**
	 * Find people by necessitys or first name like.
	 *
	 * @param description the description
	 * @param firstName the first name
	 * @param page the page
	 * @param size the size
	 * @param sortFieldName the sort field name
	 * @param sortOrder the sort order
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value = "potencialbeneficiary", params = "find=ByNecessityDescriptionLikeOrFirstNameLike", method = RequestMethod.GET)
    public String findPeopleByNecessitysOrFirstNameLike(@RequestParam(value="description", required = false) String description, 
    		@RequestParam(value="firstName", required = false) String firstName, @RequestParam(value = "page", required = false) Integer page, 
    		@RequestParam(value = "size", required = false) Integer size, @RequestParam(value = "sortFieldName", required = false) String sortFieldName, 
    		@RequestParam(value = "sortOrder", required = false) String sortOrder, Model model) {
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            model.addAttribute("people", Person.findPeopleByNecessityDescriptionLikeOrFirstNameLike(description, firstName, sortFieldName, sortOrder).setFirstResult(firstResult).setMaxResults(sizeNo).getResultList());
            float nrOfPages = (float) Person.countFindPeopleByNecessityDescriptionLikeOrFirstNameLike(description, firstName) / sizeNo;
            model.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
        	if(description.length() == 0 && firstName.length() == 0){
        		model.addAttribute("people", Person.findAllPeopleWithNecessitysDistinctUser(sortFieldName, sortOrder));
        	}else{
        		model.addAttribute("people", Person.findPeopleByNecessityDescriptionLikeOrFirstNameLike(description, firstName, sortFieldName, sortOrder).getResultList());
        	}
        }
        addDateTimeFormatPatterns(model);
        model.addAttribute("findPerson", true);
        return "participants/list";
    }
	
	/**
	 * Find institutions by necessity description or category.
	 *
	 * @param description the description
	 * @param category the category
	 * @param page the page
	 * @param size the size
	 * @param sortFieldName the sort field name
	 * @param sortOrder the sort order
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value = "potencialbeneficiary", params = "find=ByNecessityDescriptionOrCategory", method = RequestMethod.GET)
    public String findInstitutionsByNecessityDescriptionOrCategory(@RequestParam(value="description", required = false) String description,
    		@RequestParam(value="category", required = false) Category category, 
    		@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, 
    		@RequestParam(value = "sortFieldName", required = false) String sortFieldName, @RequestParam(value = "sortOrder", required = false) String sortOrder, 
    		Model model) {
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            model.addAttribute("institutions", Institution.findInstitutionsByNecessityDescriptionOrCategory(description, category, sortFieldName, sortOrder).setFirstResult(firstResult).setMaxResults(sizeNo).getResultList());
            float nrOfPages = (float) Institution.countFindInstitutionsByNecessityDescriptionOrCategory(description, category) / sizeNo;
            model.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
        	if(description.length() == 0 && category == null){
        		model.addAttribute("institutions", Institution.findAllInstitutionsDistinctUser(sortFieldName, sortOrder));
        	}else{
        		model.addAttribute("institutions", Institution.findInstitutionsByNecessityDescriptionOrCategory(description, category, sortFieldName, sortOrder).getResultList());
        	}
        }
        addDateTimeFormatPatterns(model);
        return "participants/list";
    }
	
	/**
	 * Find people by donation good description like or first name like.
	 *
	 * @param description the description
	 * @param firstName the first name
	 * @param page the page
	 * @param size the size
	 * @param sortFieldName the sort field name
	 * @param sortOrder the sort order
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value = "potencialdonor", params = "find=ByDonationGoodDescriptionLikeOrFirstNameLike", method = RequestMethod.GET)
    public String findPeopleByDonationGoodDescriptionLikeOrFirstNameLike(@RequestParam(value="description", required = false) String description, 
    		@RequestParam(value="firstName", required = false) String firstName, @RequestParam(value = "page", required = false) Integer page, 
    		@RequestParam(value = "size", required = false) Integer size, @RequestParam(value = "sortFieldName", required = false) String sortFieldName, 
    		@RequestParam(value = "sortOrder", required = false) String sortOrder, Model model) {
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            model.addAttribute("people", Person.findPeopleByDonationGoodDescriptionLikeOrFirstNameLike(description, firstName, sortFieldName, sortOrder).setFirstResult(firstResult).setMaxResults(sizeNo).getResultList());
            float nrOfPages = (float) Person.countFindPeopleByDonationGoodDescriptionLikeOrFirstNameLike(description, firstName) / sizeNo;
            model.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
        	if(description.length() == 0 && firstName.length() == 0){
        		model.addAttribute("people", Person.findAllPeopleWithDonationGoodsDistinctUser(sortFieldName, sortOrder));
        	}else{
        		model.addAttribute("people", Person.findPeopleByDonationGoodDescriptionLikeOrFirstNameLike(description, firstName, sortFieldName, sortOrder).getResultList());
        	}
        }
        addDateTimeFormatPatterns(model);
        model.addAttribute("findPerson", true);
        return "participants/list";
    }
}
