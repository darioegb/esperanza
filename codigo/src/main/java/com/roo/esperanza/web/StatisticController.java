package com.roo.esperanza.web;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.roo.esperanza.domain.Necessity;
import com.roo.esperanza.domain.Participant;
import com.roo.esperanza.provider.ScriptAddService;
import com.roo.esperanza.reference.RoleTypeEnum;
import com.roo.esperanza.reference.StatusTypeEnum;

// TODO: Auto-generated Javadoc
/**
 * The Class StatisticController.
 */
@RequestMapping("/statistics/**")
@Controller
public class StatisticController {
	
	/** The sa service. */
	@Autowired 
	ScriptAddService saService;
	private Boolean change;

	/**
	 * Find statistics by date between or user role type.
	 *
	 * @param minDate the min date
	 * @param maxDate the max date
	 * @param roleType the role type
	 * @param page the page
	 * @param size the size
	 * @param sortFieldName the sort field name
	 * @param sortOrder the sort order
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(params = "find=ByDateBetweenOrUserRoleType", method = RequestMethod.GET)
    public String findStatisticsByDateBetweenOrUserRoleType(@RequestParam(value="minDate", required=false) Date minDate, 
    		@RequestParam(value="maxDate", required=false) Date maxDate, 
    		@RequestParam(value="roleType", required = false) RoleTypeEnum roleType, 
    		@RequestParam(value = "page", required = false) Integer page, 
    		@RequestParam(value = "size", required = false) Integer size, 
    		@RequestParam(value = "sortFieldName", required = false) String sortFieldName, 
    		@RequestParam(value = "sortOrder", required = false) String sortOrder, Model model) {
		List<Participant> participants = null;
		if (page != null || (size != null && size != 0)) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            try {
            	participants =  Participant.findStatisticsByDateBetweenOrUserRoleType(minDate, maxDate, roleType).setFirstResult(firstResult).setMaxResults(sizeNo).getResultList();
                 float nrOfPages = (float) Participant.countFindStatisticsByDateBetweenOrUserRoleType(minDate, maxDate, roleType) / sizeNo;
                 model.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
			} catch (Exception e) {}
        } else {
    		if(roleType == null && ((minDate == null && maxDate == null) || (minDate.compareTo(maxDate) == 0))){
    			participants = Participant.findAllStatistics();
    		}else{
    			participants = Participant.findStatisticsByDateBetweenOrUserRoleType(minDate, maxDate, roleType).getResultList();
    		}
    	}
		change = false;
		for (Participant participant : participants) {
			Set<Necessity> necessitys = participant.getNecessitys();
			for (Iterator<Necessity> iterator = necessitys.iterator(); iterator.hasNext();) {
				Necessity necessity = (Necessity) iterator.next();
				if(!necessity.getStatusType().equals(StatusTypeEnum.Accept)){
					iterator.remove();
					change = true;
				}
			}
			if(change){	
				participant.setNecessitys(necessitys);
				change = false;
			}
		}
		model.addAttribute("participants", participants);
		saService.agregarJS("general/Chart.js");
		saService.agregarJS("general/graphic.js");
        saService.publicarJsCss(model);
        addDateTimeFormatPatterns(model);
        return "statistics/list";
    }

	/**
	 * Find statistics by date between or user role type form.
	 *
	 * @param model the model
	 * @param request the request
	 * @return the string
	 */
	@RequestMapping(params = { "find=ByDateBetweenOrUserRoleType", "form" }, method = RequestMethod.GET)
    public String findStatisticsByDateBetweenOrUserRoleTypeForm(Model model, HttpServletRequest request) {
		saService.agregarJS("general/removeDateTimesFinder.js");
        saService.publicarJsCss(model);
    	model.addAttribute("roletypeenums", Arrays.asList(RoleTypeEnum.values()).subList(1, 3));
        addDateTimeFormatPatterns(model);
        return "statistics/findStatisticsByDateBetweenOrUserRoleType";
    }
	
	/**
	 * Adds the date time format patterns.
	 *
	 * @param model the model
	 */
	void addDateTimeFormatPatterns(Model model) {
		model.addAttribute("participant_date_date_format", DateTimeFormat.patternForStyle("M-", LocaleContextHolder.getLocale()));
    }

}
