package com.roo.esperanza.web;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.roo.addon.web.mvc.controller.json.RooWebJson;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.roo.esperanza.domain.Country;
import com.roo.esperanza.domain.Province;

// TODO: Auto-generated Javadoc
/**
 * The Class ProvinceController.
 */
@RequestMapping("/provinces/**")
@Controller
@RooWebScaffold(path = "provinces", formBackingObject = Province.class)
@RooWebJson(jsonObject = Province.class)
public class ProvinceController {

	/**
	 * Json find provinces by country.
	 *
	 * @param countryId the country id
	 * @return the response entity
	 */
	@RequestMapping(params = "findByCountry", headers = "Accept=application/json")
    @ResponseBody
    public ResponseEntity<String> jsonFindProvincesByCountry(@RequestParam("countryId") Integer countryId) {
        HttpHeaders headers = new HttpHeaders();
        Country country = Country.findCountry(countryId.longValue());
        try {
            headers.add("Content-Type", "application/json; charset=utf-8");
            return new ResponseEntity<String>(Province.toJsonArray(Province.findProvincesByCountry(country).getResultList()), headers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<String>("{\"ERROR\":"+e.getMessage()+"\"}", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
