package com.roo.esperanza.web;
import com.roo.esperanza.domain.UserRole;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.roo.addon.web.mvc.controller.finder.RooWebFinder;
import org.gvnix.addon.web.mvc.addon.jquery.GvNIXWebJQuery;

/**
 * The Class UserRoleController.
 */
@RequestMapping("/userroles")
@Controller
@RooWebScaffold(path = "userroles", formBackingObject = UserRole.class)
@RooWebFinder
@GvNIXWebJQuery
public class UserRoleController {
}
