$(document).ready(function(){
	  $('#participants a').click(function(e){
		  e.preventDefault();
		  var id = $(this).parent().data('session');
		  var participant = $(this).data('id');
		  $('#participants a').removeClass('active');
		  $(this).addClass('active');
		  getMessages(participant, id);
	  });
	  function getMessages(participant, id) {
		  $.getJSON('messages?list', {participantId: participant}, function(json){
			  var list = $('#messageList');
			  list.children('a').remove();
				$.each(json, function(i, val){
					var date = new Date(val.date);
					var formated = date.getDate()+"/"+date.getMonth()+"/"+date.getFullYear()+" "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds();
					var align = (val.conversation.sender.id!=id)? "text-left" : "text-right";
					list.append('<a class="list-group-item" style="background-color: white;"><h5 class="list-group-item-heading">'+formated+'</h5><p class="list-group-item-text">'+val.message+'</p></a>');
					list.find('a:last-child').addClass(align);
				});
			});
	}
	  $('#filter').keyup(function() {
	        $('#messageList a').toggleClass('hide', this.value.length > 0);
	        var rex = new RegExp($(this).val(), 'i');
            $('#participants a').hide();
            console.log($(this).val().length);
            $('#participants a').filter(function () {    	            	
                return rex.test($(this).text());
            }).show();
	    });
	  $("form").submit(function(e) {
  		e.preventDefault();
  		$('input[name="participantReceiverId"]').attr('value', $('a[data-id].active').attr('data-id'));
  		var participantReceiverId = $('input[name="participantReceiverId"]').val();
  		var message = $('textarea[name="message"]').val();
  		$.ajax({
  			type:'POST',
  			url: "messages?new",
  			data: {message:message, participantReceiverId : participantReceiverId},
  			success: function() {},
  			complete: function() {
  				$('textarea[name="message"]').val('');
  				var participantSender = $('#participants').attr('data-session');
  				getMessages(participantReceiverId, participantSender);
  			}
  		});
   	});
});