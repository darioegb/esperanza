$(document).ready(function(){
	if($('form').attr('id') != 'command' && $('form').attr('id') != 'configNotification' && $('form').attr('action') != 'configuration/hideprofile' && $('form').attr('action') != 'donation'){
		$.validate({
			lang : 'es',
		    modules : 'security, date, file, toggleDisabled'
		});
	}else{
		$.validate({
			lang : 'es',
		    modules : 'security, date, file'
		});
	}
	$('.date-time').datepicker({
	    language: "es",
	    orientation: "bottom",
	    autoclose: true,
	    todayHighlight: true,
	    format: 'dd/mm/yyyy'
	});
});