$(document).ready(function(){
	/**
	 * Description
	 * @method getMyNotificationCount
	 * @return 
	 */
	function getMyNotificationCount() {
	$.get("notifications/count")
		.done(function(count) {
			if(count > 0)	$("#i_notifications>a>.badge").text(count);
			setTimeout(function () {
				getMyNotificationCount();
            }, 30000000)
		});
	}
	/**
	 * Description
	 * @method getMyMessagesCount
	 * @return 
	 */
	function getMyMessagesCount(){
	$.get("messages/count")
		.done(function(count) {
			if(count > 0)	$("#i_messages>a>.badge").text(count);
			setTimeout(function () {
				getMyMessagesCount();
            }, 30000000)
		});
	}
	getMyMessagesCount();
	getMyNotificationCount();
});