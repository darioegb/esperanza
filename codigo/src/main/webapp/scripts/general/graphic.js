 $(document).ready(function() {
	 $('button[name="chartButton"]').click(function(event) {
		 var labelArray = [];
		 var dataArray = [];
		 if ($(this).attr('data-label')==="list") {
			 $(this).attr('data-label', 'graphic'); 
			 $('.statisticList').css('display', 'inherit');
			 $('#graphic').css('display', 'none');
		}else{
			$(this).attr('data-label', 'list');  
			$('.statisticList').css('display', 'none');
			$('#graphic').css('display', 'inherit');
			$( ".statisticList .list-group-item" ).each(function(index) {
				labelArray.push($(this).find('i:nth-child(2)').text());
				dataArray.push($(this).find('i:last-child>b').text());
			});
			var barChartData = {
					labels : labelArray,
					datasets : [{fillColor : "rgba(143,188,143,0.5)",strokeColor : "rgba(143,188,143,0.8)",highlightFill: "rgba(143,188,143,0.75)",highlightStroke: "rgba(143,188,143,1)",data : dataArray}]}
					var ctx = document.getElementById("canvas").getContext("2d");
				var myNewChart = new Chart(ctx).Bar(barChartData, {tooltipFillColor : "rgba(32,178,170,0.8)",responsive : true, scaleShowVerticalLines : false, scaleShowGridLines: false
		    });
		}
	});
});