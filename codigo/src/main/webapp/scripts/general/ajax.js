$(document).ready(function(){
	//Load provinces for country
	$("#_country_id").change(function(){
		$("#_province_id").children('option:not(:first)').remove();
		$.getJSON("provinces?findByCountry", {countryId: $(this).val()}, function(json){
			$.each(json, function(i, val){
				$("#_province_id").append($("<option></option>")
						.attr("value",val.id)
						.text(val.name));
			});
		});
	});
});