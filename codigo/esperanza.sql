-- MySQL dump 10.13  Distrib 5.7.9, for osx10.9 (x86_64)
--
-- Host: localhost    Database: esperanza
-- ------------------------------------------------------
-- Server version	5.7.9

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `address` varchar(100) DEFAULT NULL,
  `city` varchar(65) DEFAULT NULL,
  `country` varchar(65) DEFAULT NULL,
  `postal_code` varchar(255) DEFAULT NULL,
  `province` varchar(65) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
INSERT INTO `address` VALUES (1,'Coronel Suarez 495','Gral San Martín','10','5570','11',4),(2,'Lavalle sin numero ','Lavalle','10','5580','11',3),(3,'Buenos Aires 234','Ciudad','10','5550','11',0),(4,'9 de julio 100','Lujan','10','5560','11',0),(5,'Almirante brown 32','Guaymallen','10','5562','11',0),(6,'San Martin 1312','Las Heras','10','5552','11',0),(7,'San Martin 123','Ciudad','10','5550','11',0),(8,'San Martin 1202','Las Heras','10','5552','11',0),(9,'San Lorenzo 20','Lavalle','10','5580','11',0),(10,'placeqgixoamifd','cityznsxjcjotz','10','5568','16',1),(11,'placepqkepoxxvj','cityiqfaoswegv','10','5531','6',1),(12,'placegnujmgwpke','cityteoiwlprmm','10','5588','9',1),(13,'placedlurytymoi','cityaadquzmmar','10','5520','3',1),(14,'placebwfkkwbtnl','citypqkerwhuba','10','5575','3',1),(15,'placeelqytzqeyg','cityjclyhsrfzk','10','5585','11',1),(16,'placexwruvxxxta','cityyoxybkvanp','10','5539','8',1),(17,'placemjiqdtildk','citynmxalcdmzl','10','5529','5',1),(18,'placegoybmfqoui','cityhmlxdxdzos','10','5589','16',1),(19,'placeyjzwiadpqk','citydjnmxbqytz','10','5560','7',1),(20,'placetshgmsayst','cityophpffkjri','10','5549','15',1),(21,'placecgaimmzkba','citybervdgsynu','10','5540','17',1),(22,'placetgcseulxbr','citydrzvhwmvth','10','5513','18',1),(23,'placealdjlcdlur','cityxphrlfszro','10','5559','13',1),(24,'placeeydzkcjlce','cityrturbesbgy','10','5584','18',1),(25,'placeofkinmyisr','citydsdoixqljk','10','5585','10',1),(26,'placeppecxiwkjs','cityiqcqvhwpiv','10','5513','6',1),(27,'placewogpdvwsbe','citypozhnqspuh','10','5588','1',1),(28,'placefzioyainpn','citysyspzbimji','10','5560','6',1),(29,'placemnegufsxhv','cityfqowpmqtut','10','5538','17',1);
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_action`
--

DROP TABLE IF EXISTS `admin_action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_action` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `reason` varchar(200) NOT NULL,
  `status` bit(1) NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_action`
--

LOCK TABLES `admin_action` WRITE;
/*!40000 ALTER TABLE `admin_action` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_action` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_action_denoucements`
--

DROP TABLE IF EXISTS `admin_action_denoucements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_action_denoucements` (
  `admin_action` bigint(20) NOT NULL,
  `denoucements` bigint(20) NOT NULL,
  PRIMARY KEY (`admin_action`,`denoucements`),
  UNIQUE KEY `UK_npqplhmk916a7ymr5lrsc91nc` (`denoucements`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_action_denoucements`
--

LOCK TABLES `admin_action_denoucements` WRITE;
/*!40000 ALTER TABLE `admin_action_denoucements` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_action_denoucements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(65) NOT NULL,
  `version` int(11) DEFAULT NULL,
  `parent` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Agricultura',3,NULL),(2,'Animales',1,NULL),(3,'Arte',1,NULL),(4,'Ciencía y tecnología',1,NULL),(5,'Discapacidad',1,NULL),(6,'Deportes',1,NULL),(7,'Educación',1,NULL),(8,'Comedor',1,NULL),(9,'Niñez',1,NULL),(10,'Tercera edad',1,NULL),(11,'Salud',1,NULL),(12,'Viviendas',1,NULL);
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `config_notification`
--

DROP TABLE IF EXISTS `config_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config_notification` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `complaint_received` bit(1) DEFAULT NULL,
  `donation_goods` bit(1) DEFAULT NULL,
  `donation_received` bit(1) DEFAULT NULL,
  `publicaction_status` bit(1) DEFAULT NULL,
  `receive_mails` bit(1) NOT NULL,
  `state_complaint_filed` bit(1) DEFAULT NULL,
  `statistics` bit(1) DEFAULT NULL,
  `suscriptions` bit(1) DEFAULT NULL,
  `claim_donation` bit(1) DEFAULT NULL,
  `needs` bit(1) DEFAULT NULL,
  `donation_request` bit(1) DEFAULT NULL,
  `thanks_to_donation` bit(1) DEFAULT NULL,
  `denouncements` bit(1) DEFAULT NULL,
  `publications` bit(1) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config_notification`
--

LOCK TABLES `config_notification` WRITE;
/*!40000 ALTER TABLE `config_notification` DISABLE KEYS */;
INSERT INTO `config_notification` VALUES (1,'','','','\0','','\0','','',NULL,NULL,NULL,NULL,NULL,NULL,1),(2,'','','','','','','',NULL,'','','','',NULL,NULL,1),(3,'','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,1),(4,'\0','','','\0','','\0','\0','',NULL,NULL,NULL,NULL,NULL,NULL,1),(5,'','','','\0','','','',NULL,'','','','\0',NULL,NULL,1),(6,'','','\0','','','\0','',NULL,'\0','','','',NULL,NULL,1),(7,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','',1),(8,'','','','','','','',NULL,'','','','',NULL,NULL,1),(9,'','','','','','','',NULL,'','','','',NULL,NULL,1),(10,'','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,1),(11,'','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,1),(12,'','','','\0','','\0','','',NULL,NULL,NULL,NULL,NULL,NULL,1),(13,'','','','\0','','\0','','',NULL,NULL,NULL,NULL,NULL,NULL,1),(14,'','','','\0','','\0','','',NULL,NULL,NULL,NULL,NULL,NULL,1),(15,'','','','\0','','\0','','',NULL,NULL,NULL,NULL,NULL,NULL,1),(16,'','','','\0','','\0','','',NULL,NULL,NULL,NULL,NULL,NULL,1),(17,'','','','\0','','\0','','',NULL,NULL,NULL,NULL,NULL,NULL,1),(18,'','','','\0','','\0','','',NULL,NULL,NULL,NULL,NULL,NULL,1),(19,'','','','\0','','\0','','',NULL,NULL,NULL,NULL,NULL,NULL,1),(20,'','','','\0','','\0','','',NULL,NULL,NULL,NULL,NULL,NULL,1),(21,'','','','\0','','\0','','',NULL,NULL,NULL,NULL,NULL,NULL,1),(22,'','','','','','','',NULL,'','','','',NULL,NULL,1),(23,'','','','','','','',NULL,'','','','',NULL,NULL,1),(24,'','','','','','','',NULL,'','','','',NULL,NULL,1),(25,'','','','','','','',NULL,'','','','',NULL,NULL,1),(26,'','','','','','','',NULL,'','','','',NULL,NULL,1),(27,'','','','','','','',NULL,'','','','',NULL,NULL,1),(28,'','','','','','','',NULL,'','','','',NULL,NULL,1),(29,'','','','','','','',NULL,'','','','',NULL,NULL,1),(30,'','','','','','','',NULL,'','','','',NULL,NULL,1),(31,'','','','','','','',NULL,'','','','',NULL,NULL,1);
/*!40000 ALTER TABLE `config_notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conversation`
--

DROP TABLE IF EXISTS `conversation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conversation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` int(11) DEFAULT NULL,
  `receiver` bigint(20) DEFAULT NULL,
  `sender` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conversation`
--

LOCK TABLES `conversation` WRITE;
/*!40000 ALTER TABLE `conversation` DISABLE KEYS */;
INSERT INTO `conversation` VALUES (1,1,6,3),(2,0,5,3),(3,0,3,5),(4,0,5,3),(5,0,5,3),(6,0,3,5),(7,0,24,4),(8,0,4,24);
/*!40000 ALTER TABLE `conversation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(10) DEFAULT NULL,
  `name` varchar(65) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=247 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` VALUES (1,'AF','Afghanistan',1),(2,'AL','Albania',1),(3,'DZ','Algeria',1),(4,'DS','American Samoa',1),(5,'AD','Andorra',1),(6,'AO','Angola',1),(7,'AI','Anguilla',1),(8,'AQ','Antarctica',1),(9,'AG','Antigua and/or Barbuda',1),(10,'AR','Argentina',1),(11,'AM','Armenia',1),(12,'AW','Aruba',1),(13,'AU','Australia',1),(14,'AT','Austria',1),(15,'AZ','Azerbaijan',1),(16,'BS','Bahamas',1),(17,'BH','Bahrain',1),(18,'BD','Bangladesh',1),(19,'BB','Barbados',1),(20,'BY','Belarus',1),(21,'BE','Belgium',1),(22,'BZ','Belize',1),(23,'BJ','Benin',1),(24,'BM','Bermuda',1),(25,'BT','Bhutan',1),(26,'BO','Bolivia',1),(27,'BA','Bosnia and Herzegovina',1),(28,'BW','Botswana',1),(29,'BV','Bouvet Island',1),(30,'BR','Brazil',1),(31,'IO','British Indian Ocean Territory',1),(32,'BN','Brunei Darussalam',1),(33,'BG','Bulgaria',1),(34,'BF','Burkina Faso',1),(35,'BI','Burundi',1),(36,'KH','Cambodia',1),(37,'CM','Cameroon',1),(38,'CA','Canada',1),(39,'CV','Cape Verde',1),(40,'KY','Cayman Islands',1),(41,'CF','Central African Republic',1),(42,'TD','Chad',1),(43,'CL','Chile',1),(44,'CN','China',1),(45,'CX','Christmas Island',1),(46,'CC','Cocos (null, Keeling) Islands',1),(47,'CO','Colombia',1),(48,'KM','Comoros',1),(49,'CG','Congo',1),(50,'CK','Cook Islands',1),(51,'CR','Costa Rica',1),(52,'HR','Croatia (null, Hrvatska)',1),(53,'CU','Cuba',1),(54,'CY','Cyprus',1),(55,'CZ','Czech Republic',1),(56,'DK','Denmark',1),(57,'DJ','Djibouti',1),(58,'DM','Dominica',1),(59,'DO','Dominican Republic',1),(60,'TP','East Timor',1),(61,'EC','Ecuador',1),(62,'EG','Egypt',1),(63,'SV','El Salvador',1),(64,'GQ','Equatorial Guinea',1),(65,'ER','Eritrea',1),(66,'EE','Estonia',1),(67,'ET','Ethiopia',1),(68,'FK','Falkland Islands (null, Malvinas)',1),(69,'FO','Faroe Islands',1),(70,'FJ','Fiji',1),(71,'FI','Finland',1),(72,'FR','France',1),(73,'FX','France, Metropolitan',1),(74,'GF','French Guiana',1),(75,'PF','French Polynesia',1),(76,'TF','French Southern Territories',1),(77,'GA','Gabon',1),(78,'GM','Gambia',1),(79,'GE','Georgia',1),(80,'DE','Germany',1),(81,'GH','Ghana',1),(82,'GI','Gibraltar',1),(83,'GK','Guernsey',1),(84,'GR','Greece',1),(85,'GL','Greenland',1),(86,'GD','Grenada',1),(87,'GP','Guadeloupe',1),(88,'GU','Guam',1),(89,'GT','Guatemala',1),(90,'GN','Guinea',1),(91,'GW','Guinea-Bissau',1),(92,'GY','Guyana',1),(93,'HT','Haiti',1),(94,'HM','Heard and Mc Donald Islands',1),(95,'HN','Honduras',1),(96,'HK','Hong Kong',1),(97,'HU','Hungary',1),(98,'IS','Iceland',1),(99,'IN','India',1),(100,'IM','Isle of Man',1),(101,'ID','Indonesia',1),(102,'IR','Iran (null, Islamic Republic of)',1),(103,'IQ','Iraq',1),(104,'IE','Ireland',1),(105,'IL','Israel',1),(106,'IT','Italy',1),(107,'CI','Ivory Coast',1),(108,'JE','Jersey',1),(109,'JM','Jamaica',1),(110,'JP','Japan',1),(111,'JO','Jordan',1),(112,'KZ','Kazakhstan',1),(113,'KE','Kenya',1),(114,'KI','Kiribati',1),(115,'KP','Korea, Democratic People\'s Republic of',1),(116,'KR','Korea, Republic of',1),(117,'XK','Kosovo',1),(118,'KW','Kuwait',1),(119,'KG','Kyrgyzstan',1),(120,'LA','Lao People\'s Democratic Republic',1),(121,'LV','Latvia',1),(122,'LB','Lebanon',1),(123,'LS','Lesotho',1),(124,'LR','Liberia',1),(125,'LY','Libyan Arab Jamahiriya',1),(126,'LI','Liechtenstein',1),(127,'LT','Lithuania',1),(128,'LU','Luxembourg',1),(129,'MO','Macau',1),(130,'MK','Macedonia',1),(131,'MG','Madagascar',1),(132,'MW','Malawi',1),(133,'MY','Malaysia',1),(134,'MV','Maldives',1),(135,'ML','Mali',1),(136,'MT','Malta',1),(137,'MH','Marshall Islands',1),(138,'MQ','Martinique',1),(139,'MR','Mauritania',1),(140,'MU','Mauritius',1),(141,'TY','Mayotte',1),(142,'MX','Mexico',1),(143,'FM','Micronesia, Federated States of',1),(144,'MD','Moldova, Republic of',1),(145,'MC','Monaco',1),(146,'MN','Mongolia',1),(147,'ME','Montenegro',1),(148,'MS','Montserrat',1),(149,'MA','Morocco',1),(150,'MZ','Mozambique',1),(151,'MM','Myanmar',1),(152,'NA','Namibia',1),(153,'NR','Nauru',1),(154,'NP','Nepal',1),(155,'NL','Netherlands',1),(156,'AN','Netherlands Antilles',1),(157,'NC','New Caledonia',1),(158,'NZ','New Zealand',1),(159,'NI','Nicaragua',1),(160,'NE','Niger',1),(161,'NG','Nigeria',1),(162,'NU','Niue',1),(163,'NF','Norfolk Island',1),(164,'MP','Northern Mariana Islands',1),(165,'NO','Norway',1),(166,'OM','Oman',1),(167,'PK','Pakistan',1),(168,'PW','Palau',1),(169,'PS','Palestine',1),(170,'PA','Panama',1),(171,'PG','Papua New Guinea',1),(172,'PY','Paraguay',1),(173,'PE','Peru',1),(174,'PH','Philippines',1),(175,'PN','Pitcairn',1),(176,'PL','Poland',1),(177,'PT','Portugal',1),(178,'PR','Puerto Rico',1),(179,'QA','Qatar',1),(180,'RE','Reunion',1),(181,'RO','Romania',1),(182,'RU','Russian Federation',1),(183,'RW','Rwanda',1),(184,'KN','Saint Kitts and Nevis',1),(185,'LC','Saint Lucia',1),(186,'VC','Saint Vincent and the Grenadines',1),(187,'WS','Samoa',1),(188,'SM','San Marino',1),(189,'ST','Sao Tome and Principe',1),(190,'SA','Saudi Arabia',1),(191,'SN','Senegal',1),(192,'RS','Serbia',1),(193,'SC','Seychelles',1),(194,'SL','Sierra Leone',1),(195,'SG','Singapore',1),(196,'SK','Slovakia',1),(197,'SI','Slovenia',1),(198,'SB','Solomon Islands',1),(199,'SO','Somalia',1),(200,'ZA','South Africa',1),(201,'GS','South Georgia South Sandwich Islands',1),(202,'ES','Spain',1),(203,'LK','Sri Lanka',1),(204,'SH','St. Helena',1),(205,'PM','St. Pierre and Miquelon',1),(206,'SD','Sudan',1),(207,'SR','Suriname',1),(208,'SJ','Svalbard and Jan Mayen Islands',1),(209,'SZ','Swaziland',1),(210,'SE','Sweden',1),(211,'CH','Switzerland',1),(212,'SY','Syrian Arab Republic',1),(213,'TW','Taiwan',1),(214,'TJ','Tajikistan',1),(215,'TZ','Tanzania, United Republic of',1),(216,'TH','Thailand',1),(217,'TG','Togo',1),(218,'TK','Tokelau',1),(219,'TO','Tonga',1),(220,'TT','Trinidad and Tobago',1),(221,'TN','Tunisia',1),(222,'TR','Turkey',1),(223,'TM','Turkmenistan',1),(224,'TC','Turks and Caicos Islands',1),(225,'TV','Tuvalu',1),(226,'UG','Uganda',1),(227,'UA','Ukraine',1),(228,'AE','United Arab Emirates',1),(229,'GB','United Kingdom',1),(230,'US','United States',1),(231,'UM','United States minor outlying islands',1),(232,'UY','Uruguay',1),(233,'UZ','Uzbekistan',1),(234,'VU','Vanuatu',1),(235,'VA','Vatican City State',1),(236,'VE','Venezuela',1),(237,'VN','Vietnam',1),(238,'VG','Virgin Islands (null, British)',1),(239,'VI','Virgin Islands (null, U.S.)',1),(240,'WF','Wallis and Futuna Islands',1),(241,'EH','Western Sahara',1),(242,'YE','Yemen',1),(243,'YU','Yugoslavia',1),(244,'ZR','Zaire',1),(245,'ZM','Zambia',1),(246,'ZW','Zimbabwe',1);
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `denouncement`
--

DROP TABLE IF EXISTS `denouncement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `denouncement` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `reason` varchar(200) NOT NULL,
  `status_type` varchar(255) NOT NULL,
  `version` int(11) DEFAULT NULL,
  `admin_resolution` bigint(20) DEFAULT NULL,
  `complainant` bigint(20) DEFAULT NULL,
  `denouced` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `denouncement`
--

LOCK TABLES `denouncement` WRITE;
/*!40000 ALTER TABLE `denouncement` DISABLE KEYS */;
INSERT INTO `denouncement` VALUES (1,'2016-03-21 20:53:58','perfil sospechozo','Create',1,NULL,1,6);
/*!40000 ALTER TABLE `denouncement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `donation`
--

DROP TABLE IF EXISTS `donation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `donation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `donor` bigint(20) DEFAULT NULL,
  `receiver` bigint(20) DEFAULT NULL,
  `donation_status_type` varchar(255) NOT NULL,
  `publication` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `donation`
--

LOCK TABLES `donation` WRITE;
/*!40000 ALTER TABLE `donation` DISABLE KEYS */;
INSERT INTO `donation` VALUES (1,'2016-03-24 15:03:43','tengo 5 almohadas',1,5,3,'Received',5),(2,'2016-03-28 20:26:28','tengo 1 cama',1,6,3,'Received',6),(3,'2016-04-06 23:06:35','1 cacelora',1,5,2,'Received',2),(8,'2016-05-17 23:20:10','tengo 1 holla!',0,1,2,'Opened',2),(9,'2016-05-31 21:08:44','tengo 1 cama!!',5,1,3,'Accept',5),(10,'2016-06-06 18:20:47','tengo una mesa de luz',0,1,3,'Opened',5),(11,'2016-06-07 22:50:06','Tengo 1 colchon!',3,24,4,'Received',4),(12,'2016-06-10 22:50:06','Tengo 2 juegos sábanas y 1 colchon si les sirve.',2,22,4,'Accept',4),(13,'2016-06-11 22:50:06','Hola tengo 10 páquetes de ázucar!',1,29,4,'Opened',4),(14,'2016-06-12 22:50:06','Tengo 1 colchon de dos plazas.',1,26,4,'Opened',4),(15,'2016-06-13 22:50:06','Tengo 2 colchones de plaza 1/2.',1,28,4,'Opened',4),(16,'2016-06-20 23:37:05','Hola tengo 1 colchon y almohada. Saludos!',1,26,4,'Opened',4),(17,'2016-06-22 23:37:05','Hola tengo 1 colchon, 1 almohada y 2 juegos de sábanas.',1,31,4,'Opened',4),(18,'2016-06-27 23:37:05','Hola tengo 3 colchones de 2 plaza.',1,24,4,'Opened',4);
/*!40000 ALTER TABLE `donation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `donation_request`
--

DROP TABLE IF EXISTS `donation_request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `donation_request` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `description` varchar(100) NOT NULL,
  `status_type` varchar(10) NOT NULL DEFAULT 'Create',
  `version` int(11) DEFAULT NULL,
  `donation_good` bigint(20) DEFAULT NULL,
  `applicant` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `donation_request`
--

LOCK TABLES `donation_request` WRITE;
/*!40000 ALTER TABLE `donation_request` DISABLE KEYS */;
INSERT INTO `donation_request` VALUES (1,'2016-05-30 21:17:45','me seria util para dar clases!','Create',0,1,6),(2,'2016-06-29 11:53:12','me seria muy util para nuestro comedor','Create',1,27,11),(3,'2016-06-29 11:53:12','Nos vendria muy bien para nuestro asilo','Create',1,27,4),(4,'2016-06-29 11:53:12','seria de gran ayuda para el comedor','Create',1,27,2),(5,'2016-06-29 11:53:12','me seria muy util','Create',1,27,15),(6,'2016-06-29 11:53:12','nos vendria muy bien','Create',1,27,20);
/*!40000 ALTER TABLE `donation_request` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `message` varchar(250) NOT NULL,
  `status` bit(1) NOT NULL,
  `version` int(11) DEFAULT NULL,
  `conversation` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message`
--

LOCK TABLES `message` WRITE;
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
INSERT INTO `message` VALUES (1,'2016-04-28 20:28:55','Hola, Si tenes otra cama para donar nos vendria muy bien','',1,1),(2,'2016-04-29 20:30:09','Si posee alimentos tambien nos serian de gran ayuda gracias','',2,1),(3,'2016-04-21 23:04:15','muchas gracias!!!','',1,2),(4,'2016-06-20 13:14:20','necesita algo mas?','',1,3),(12,'2016-06-11 21:53:49','si necesita alguna otra cosa aviseme!','',1,3),(14,'2016-06-28 09:17:23','Hola me interesa su donación puede enviarme cuando desee.','',1,7),(22,'2016-06-28 10:26:15','Mañana por la mañana lo acercó al lugar. saludos!','',1,8),(23,'2016-06-29 22:58:56','Muchas gracias por su donación!','',1,7);
/*!40000 ALTER TABLE `message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `multimedia`
--

DROP TABLE IF EXISTS `multimedia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `multimedia` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(250) NOT NULL,
  `multimedia_type` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `multimedia`
--

LOCK TABLES `multimedia` WRITE;
/*!40000 ALTER TABLE `multimedia` DISABLE KEYS */;
INSERT INTO `multimedia` VALUES (1,'niño comiendo','Image','images/gallery/2/2-1454028850637.jpg',0),(2,'comedor vista','Image','images/gallery/2/2-1454030878735.jpg',0),(3,'comedor historia','Video','http://www.youtube.com/embed/T8n8Pqu8IbM',0),(4,'comedor vista','Video','http://www.youtube.com/embed/k7AttHngMIM',NULL),(6,'otra imagen del comedor','Image','images/gallery/2/2-1463855066652.jpg',0);
/*!40000 ALTER TABLE `multimedia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notification`
--

DROP TABLE IF EXISTS `notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `status` bit(1) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `notification_type` varchar(255) NOT NULL,
  `user` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notification`
--

LOCK TABLES `notification` WRITE;
/*!40000 ALTER TABLE `notification` DISABLE KEYS */;
INSERT INTO `notification` VALUES (1,'2016-03-24 15:03:43','recibiste una donación de 5 almohadas del participante Juan Perez','',1,'DonationReceived',3),(2,'2016-03-28 20:26:28','recibiste una donación 1 cama del participante Analia Gomez','',3,'DonationReceived',3),(3,'2016-04-21 23:04:15','Ha recibido un agradecimiento de muchas gracias!!!del participante Sala de parto despertar','\0',0,'ThanksToDonation',5),(8,'2016-05-14 19:49:26','Ha recibido un agradecimiento: esto es otra prueba de la donación 1 cacelora del participante Comedor Buenos dias','\0',0,'ThanksToDonation',5),(9,'2016-05-17 23:20:10','el participante Darío Emmanuel Gonzalez Banó quiere donarle tengo 1 holla!','',1,'DonationReceived',2),(10,'2016-05-31 21:08:44','el participante Darío Emmanuel Gonzalez Banó quiere donarle tengo 1 cama!!','\0',0,'DonationReceived',3),(11,'2016-06-06 18:20:47','el participante Darío Emmanuel Gonzalez Banó quiere donarle tengo una mesa de luz','',1,'DonationReceived',3),(12,'2016-06-07 22:50:06','el participante  Janet Patterson quiere donarle Tengo 1 colchon!','',2,'DonationReceived',4),(13,'2016-06-10 22:50:06','el participante Gary Richardson quiere donarle Tengo 2 juegos sábanas y 1 colchon si les sirve.\n\n','\0',2,'DonationReceived',4),(14,'2016-06-11 22:50:06','el participante \nHelen Ward quiere donarle Hola tengo 10 páquetes de ázucar!\n\n','\0',2,'DonationReceived',4),(15,'2016-06-12 22:50:06','el participante Mark Flores quiere donarle \nTengo 1 colchon de dos plazas.','\0',2,'DonationReceived',4),(16,'2016-06-13 22:50:06','el participante Debra Wright quiere donarle Tengo 2 colchones de plaza 1/2.','\0',2,'DonationReceived',4),(17,'2016-06-20 23:37:05','el participante Mark Flores quiere donarle Hola tengo 1 colchon y almohada. Saludos!\n\n','\0',2,'DonationReceived',4),(18,'2016-06-22 23:37:05','el participante \nRonald Scott quiere donarle Hola tengo 1 colchon, 1 almohada y 2 juegos de sábanas.\n\n','\0',2,'DonationReceived',4),(19,'2016-06-27 23:37:05','el participante Janet Patterson quiere donarle Hola tengo 3 colchones de 2 plaza.\n\n','\0',2,'DonationReceived',4),(20,'2016-06-28 22:58:56','Ha recibido un agradecimiento: Muchas gracias por su donación! de la donación Tengo 1 colchon! del participante Asilo nuevo horizonte','\0',0,'ThanksToDonation',24);
/*!40000 ALTER TABLE `notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `participant`
--

DROP TABLE IF EXISTS `participant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `participant` (
  `dtype` varchar(31) NOT NULL,
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cell_phone` int(11) DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `profile_picture` varchar(255) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `genre_type` varchar(255) DEFAULT NULL,
  `identification` int(11) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `cbu` int(11) DEFAULT NULL,
  `long_description` varchar(1000) DEFAULT NULL,
  `short_description` varchar(100) DEFAULT NULL,
  `social_reason` varchar(255) DEFAULT NULL,
  `address` bigint(20) DEFAULT NULL,
  `user` bigint(20) DEFAULT NULL,
  `category` bigint(20) DEFAULT NULL,
  `personal_description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `participant`
--

LOCK TABLES `participant` WRITE;
/*!40000 ALTER TABLE `participant` DISABLE KEYS */;
INSERT INTO `participant` VALUES ('Person',1,NULL,NULL,'images/profileUser/1-1453248340692.jpg',13,'1987-12-12 00:00:00','Darío Emmanuel','Male',33321165,'Gonzalez Banó',NULL,NULL,NULL,NULL,1,1,NULL,'Este es un usuario de prueba nueva test'),('Institution',2,NULL,NULL,'images/profileUser/institution.png',5,'2014-01-18 23:00:00',NULL,NULL,NULL,NULL,1245678901,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse nec cursus odio, semper commodo magna.','Este es un usuario de prueba nueva test','Comedor Buenos dias',2,2,8,NULL),('Institution',3,NULL,261423786,'images/profileUser/institution.png',1,'2000-11-14 09:00:00',NULL,NULL,NULL,NULL,1222222221,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse nec cursus odio, semper commodo magna.','Este es un usuario de prueba nueva test','Sala de parto despertar',3,3,NULL,NULL),('Institution',4,NULL,NULL,'images/profileUser/institution.png',1,'2010-03-10 13:00:00',NULL,NULL,NULL,NULL,12765431,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse nec cursus odio, semper commodo magna.','Este es un usuario de prueba nueva test','Asilo nuevo horizonte',4,4,NULL,NULL),('Person',5,NULL,NULL,'images/profileUser/person.png',4,'1976-09-20 13:00:00','Juan','Male',20232647,'Perez',NULL,NULL,'',NULL,5,5,NULL,'Este es un usuario de prueba nueva test'),('Person',6,NULL,NULL,'images/profileUser/person.png',1,'1958-07-10 13:00:00','Analia','Female',15667879,'Gomez',NULL,NULL,NULL,NULL,6,6,NULL,'Este es un usuario de prueba nueva test'),('Person',8,NULL,NULL,'images/profileUser/person.png',1,'1980-06-20 21:41:46','Federico','Male',NULL,'Rodriguez',NULL,NULL,NULL,NULL,7,8,NULL,'Este es un usuario de prueba nueva test'),('Person',9,NULL,NULL,'images/profileUser/person.png',1,'1966-06-20 21:42:25','Analia','Female',NULL,'Juarez',NULL,NULL,NULL,NULL,8,9,NULL,'Este es un usuario de prueba nueva test'),('Institution',10,NULL,NULL,'images/profileUser/institution.png',1,'1998-06-20 21:42:07',NULL,NULL,NULL,NULL,NULL,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse nec cursus odio, semper commodo magna.','Este es un usuario de prueba nueva test','Sala primeros auxuilios manos a la obra',9,10,NULL,NULL),('Institution',11,NULL,NULL,'images/profileUser/institution.png',1,'2010-06-20 21:42:19',NULL,NULL,NULL,NULL,NULL,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse nec cursus odio, semper commodo magna.','Este es un usuario de prueba nueva test','Comedor comunitario 101',10,11,NULL,NULL),('Institution',12,NULL,NULL,'images/profileUser/institution.png',1,'2016-06-26 13:07:26',NULL,NULL,NULL,NULL,1420741324,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse nec cursus odio, semper commodo magna.','Este es un usuario de prueba nueva test','institucion6',13,12,2,NULL),('Institution',13,NULL,NULL,'images/profileUser/institution.png',1,'2016-06-26 13:07:26',NULL,NULL,NULL,NULL,1946307619,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse nec cursus odio, semper commodo magna.','Este es un usuario de prueba nueva test','institucion1',14,13,8,NULL),('Institution',14,NULL,NULL,'images/profileUser/institution.png',1,'2016-06-26 13:07:26',NULL,NULL,NULL,NULL,1790154068,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse nec cursus odio, semper commodo magna.','Este es un usuario de prueba nueva test','institucion5',15,14,7,NULL),('Institution',15,NULL,NULL,'images/profileUser/institution.png',1,'2016-06-26 13:07:26',NULL,NULL,NULL,NULL,1062602804,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse nec cursus odio, semper commodo magna.','Este es un usuario de prueba nueva test','institucion7',16,15,8,NULL),('Institution',16,NULL,NULL,'images/profileUser/institution.png',1,'2016-06-26 13:07:26',NULL,NULL,NULL,NULL,1457204866,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse nec cursus odio, semper commodo magna.','Este es un usuario de prueba nueva test','institucion6',17,16,5,NULL),('Institution',17,NULL,NULL,'images/profileUser/institution.png',1,'2016-06-26 13:07:26',NULL,NULL,NULL,NULL,1472989936,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse nec cursus odio, semper commodo magna.','Este es un usuario de prueba nueva test','institucion5',18,17,6,NULL),('Institution',18,NULL,NULL,'images/profileUser/institution.png',1,'2016-06-26 13:07:26',NULL,NULL,NULL,NULL,1288502930,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse nec cursus odio, semper commodo magna.','Este es un usuario de prueba nueva test','institucion8',19,18,8,NULL),('Institution',19,NULL,NULL,'images/profileUser/institution.png',1,'2016-06-26 13:07:26',NULL,NULL,NULL,NULL,1438702051,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse nec cursus odio, semper commodo magna.','Este es un usuario de prueba nueva test','institucion7',20,19,1,NULL),('Institution',20,NULL,NULL,'images/profileUser/institution.png',1,'2016-06-26 13:07:26',NULL,NULL,NULL,NULL,1496487284,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse nec cursus odio, semper commodo magna.','Este es un usuario de prueba nueva test','institucion1',21,20,1,NULL),('Institution',21,NULL,NULL,'images/profileUser/institution.png',1,'2016-06-26 13:07:26',NULL,NULL,NULL,NULL,1389539230,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse nec cursus odio, semper commodo magna.','Este es un usuario de prueba nueva test','institucion1',22,21,8,NULL),('Person',22,NULL,NULL,'images/profileUser/person.png',1,'2016-06-26 13:20:50','Gary','Male',NULL,'Richardson',NULL,NULL,NULL,NULL,13,22,NULL,'Etiam magnis non nec aptent nascetur odio mattis nostra. Viverra.'),('Person',23,NULL,NULL,'images/profileUser/person.png',1,'2016-06-26 13:20:50','Karen','Male',NULL,'Hernandez',NULL,NULL,NULL,NULL,14,23,NULL,'Nostra volutpat conubia pellentesque vehicula quam tortor eros nonummy. Morbi.'),('Person',24,NULL,NULL,'images/profileUser/person.png',1,'2016-06-26 13:20:50','Janet','Male',NULL,'Patterson',NULL,NULL,NULL,NULL,15,24,NULL,'Odio enim fringilla ullamcorper nisi eget gravida aliquet dapibus. Enim.'),('Person',25,NULL,NULL,'images/profileUser/person.png',1,'2016-06-26 13:20:50','Roger','Male',NULL,'Lewis',NULL,NULL,NULL,NULL,16,25,NULL,'Vehicula in bibendum erat aenean integer tellus dictum facilisi. Semper.'),('Person',26,NULL,NULL,'images/profileUser/person.png',1,'2016-06-26 13:20:50','Mark','Male',NULL,'Flores',NULL,NULL,NULL,NULL,17,26,NULL,'Odio dolor bibendum sociis ullamcorper blandit placerat duis viverra. Nibh.'),('Person',27,NULL,NULL,'images/profileUser/person.png',1,'2016-06-26 13:21:02','Mary','Female',NULL,'Wright',NULL,NULL,NULL,NULL,13,27,NULL,'Maecenas velit iaculis velit porta nostra vivamus augue leo. Montes.'),('Person',28,NULL,NULL,'images/profileUser/person.png',1,'2016-06-26 13:21:02','Debra','Female',NULL,'Wright',NULL,NULL,NULL,NULL,14,28,NULL,'Ut dis cum dapibus lacus non purus turpis feugiat. Nonummy.'),('Person',29,NULL,NULL,'images/profileUser/person.png',1,'2016-06-26 13:21:02','Helen','Female',NULL,'Ward',NULL,NULL,NULL,NULL,15,29,NULL,'Class egestas sociosqu mollis cubilia consequat diam metus at. Quam.'),('Person',30,NULL,NULL,'images/profileUser/person.png',1,'2016-06-26 13:21:02','Ruth','Female',NULL,'Cooper',NULL,NULL,NULL,NULL,16,30,NULL,'A class maecenas class arcu ad tristique magnis pretium. Natoque.'),('Person',31,NULL,NULL,'images/profileUser/person.png',1,'2016-06-26 13:21:02','Ronald','Female',NULL,'Scott',NULL,NULL,NULL,NULL,17,31,NULL,'Euismod auctor magnis elementum primis ullamcorper lacinia fringilla lacus. Enim.');
/*!40000 ALTER TABLE `participant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `participant_array_mutimedia`
--

DROP TABLE IF EXISTS `participant_array_mutimedia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `participant_array_mutimedia` (
  `participant` bigint(20) NOT NULL,
  `array_mutimedia` bigint(20) NOT NULL,
  PRIMARY KEY (`participant`,`array_mutimedia`),
  UNIQUE KEY `UK_m16qaq4ngfpqqdh1wqx0nidc4` (`array_mutimedia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `participant_array_mutimedia`
--

LOCK TABLES `participant_array_mutimedia` WRITE;
/*!40000 ALTER TABLE `participant_array_mutimedia` DISABLE KEYS */;
INSERT INTO `participant_array_mutimedia` VALUES (2,1),(2,2),(2,3),(2,4),(2,6);
/*!40000 ALTER TABLE `participant_array_mutimedia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `participant_array_score`
--

DROP TABLE IF EXISTS `participant_array_score`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `participant_array_score` (
  `participant` bigint(20) NOT NULL,
  `array_score` bigint(20) NOT NULL,
  PRIMARY KEY (`participant`,`array_score`),
  UNIQUE KEY `UK_ga6yaftot5296441fvbdv2r4d` (`array_score`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `participant_array_score`
--

LOCK TABLES `participant_array_score` WRITE;
/*!40000 ALTER TABLE `participant_array_score` DISABLE KEYS */;
INSERT INTO `participant_array_score` VALUES (5,1),(6,2);
/*!40000 ALTER TABLE `participant_array_score` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `participant_donation_goods`
--

DROP TABLE IF EXISTS `participant_donation_goods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `participant_donation_goods` (
  `participant` bigint(20) NOT NULL,
  `donation_goods` bigint(20) NOT NULL,
  PRIMARY KEY (`participant`,`donation_goods`),
  UNIQUE KEY `UK_do0yfu7vq054qhh2abfna8olq` (`donation_goods`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `participant_donation_goods`
--

LOCK TABLES `participant_donation_goods` WRITE;
/*!40000 ALTER TABLE `participant_donation_goods` DISABLE KEYS */;
INSERT INTO `participant_donation_goods` VALUES (1,1),(24,27),(26,28),(28,29),(22,30),(26,31),(30,32),(23,33),(24,34),(28,35),(28,36),(23,37),(29,38),(24,39),(24,40),(27,41),(30,42),(27,43),(30,44),(27,45),(29,46),(2,47),(2,48),(2,49),(2,50);
/*!40000 ALTER TABLE `participant_donation_goods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `participant_necessitys`
--

DROP TABLE IF EXISTS `participant_necessitys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `participant_necessitys` (
  `participant` bigint(20) NOT NULL,
  `necessitys` bigint(20) NOT NULL,
  PRIMARY KEY (`participant`,`necessitys`),
  UNIQUE KEY `UK_5dj8109f2pyu8h1tflx9b4rd4` (`necessitys`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `participant_necessitys`
--

LOCK TABLES `participant_necessitys` WRITE;
/*!40000 ALTER TABLE `participant_necessitys` DISABLE KEYS */;
INSERT INTO `participant_necessitys` VALUES (2,2),(1,3),(4,4),(3,5),(3,6),(26,7),(24,8),(25,9),(14,10),(11,11),(31,12),(26,13),(17,14),(18,15),(12,16),(16,17),(20,18),(21,19),(23,20),(30,21),(18,22),(30,23),(27,24),(17,25),(29,26);
/*!40000 ALTER TABLE `participant_necessitys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `province`
--

DROP TABLE IF EXISTS `province`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `province` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(65) DEFAULT NULL,
  `country` bigint(20) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `province`
--

LOCK TABLES `province` WRITE;
/*!40000 ALTER TABLE `province` DISABLE KEYS */;
INSERT INTO `province` VALUES (1,'Buenos Aires',10,1),(2,'Catamarca',10,1),(3,'Chaco',10,1),(4,'Chubut',10,1),(5,'Córdoba',10,1),(6,'Corrientes',10,1),(7,'Entre Ríos',10,1),(8,'Jujuy',10,1),(9,'La Pampa',10,1),(10,'La Rioja',10,1),(11,'Mendoza',10,1),(12,'Misiones',10,1),(13,'Neuquén',10,1),(14,'Río Negro',10,1),(15,'Salta',10,1),(16,'San Juan',10,1),(17,'San Luis',10,1),(18,'Santa Cruz',10,1),(19,'Santa Fe',10,1),(20,'Santiago del Estero',10,1),(21,'Tierra del Fuego, Antártida e Isla del Atlántico Sur',10,1),(22,'Tucumán',10,1);
/*!40000 ALTER TABLE `province` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `publication`
--

DROP TABLE IF EXISTS `publication`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publication` (
  `dtype` varchar(31) NOT NULL,
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `description` varchar(100) NOT NULL,
  `donation_type` varchar(255) NOT NULL,
  `link` varchar(250) DEFAULT NULL,
  `status_type` varchar(255) NOT NULL DEFAULT '0',
  `version` int(11) DEFAULT NULL,
  `photo_path` varchar(255) DEFAULT NULL,
  `priority_type` varchar(255) DEFAULT NULL,
  `history` bigint(20) DEFAULT NULL,
  `detailed_description` varchar(300) DEFAULT NULL,
  `creator` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `publication`
--

LOCK TABLES `publication` WRITE;
/*!40000 ALTER TABLE `publication` DISABLE KEYS */;
INSERT INTO `publication` VALUES ('DonationGood',1,NULL,'Bajo electrico','Electronics',NULL,'Accept',1,'images/donationGood/1/1-1456451567627.png','Hight',NULL,NULL,1),('Necessity',2,NULL,'2 hollas','Other',NULL,'Finish',3,NULL,'Medium',NULL,NULL,2),('Necessity',3,NULL,'1 docena de vasos ','Other',NULL,'Create',1,NULL,'Hight',NULL,NULL,1),('Necessity',4,NULL,'2 colchones de 1 plaza','Other',NULL,'Accept',1,NULL,'Medium',NULL,NULL,4),('Necessity',5,NULL,'5 almohadas','Other',NULL,'Accept',1,NULL,'Medium',NULL,NULL,3),('Necessity',6,NULL,'1 cama','Other',NULL,'Finish',0,NULL,'Hight',NULL,NULL,3),('Necessity',7,'2016-06-26 15:13:04','Suscipit sem malesuada.','Other',NULL,'Create',1,NULL,'Medium',NULL,NULL,26),('Necessity',8,'2016-06-26 15:13:04','Neque pede nec.','Other',NULL,'Accept',1,NULL,'Medium',NULL,NULL,24),('Necessity',9,'2016-06-26 15:13:04','Phasellus lorem erat.','Other',NULL,'Create',1,NULL,'Medium',NULL,NULL,25),('Necessity',10,'2016-06-26 15:13:04','Sollicitudin cras cum.','Other',NULL,'Accept',1,NULL,'Medium',NULL,NULL,14),('Necessity',11,'2016-06-26 15:13:04','Blandit sagittis per.','Other',NULL,'Accept',1,NULL,'Medium',NULL,NULL,11),('Necessity',12,'2016-06-26 15:13:04','Leo tempus commodo.','Other',NULL,'Accept',1,NULL,'Medium',NULL,NULL,31),('Necessity',13,'2016-06-26 15:13:04','Ipsum aliquam venenatis.','Other',NULL,'Accept',1,NULL,'Medium',NULL,NULL,26),('Necessity',14,'2016-06-26 15:13:04','Montes luctus quam.','Other',NULL,'Accept',1,NULL,'Medium',NULL,NULL,17),('Necessity',15,'2016-06-26 15:13:04','Turpis laoreet interdum.','Other',NULL,'Accept',1,NULL,'Medium',NULL,NULL,18),('Necessity',16,'2016-06-26 15:13:04','Venenatis nam tempus.','Other',NULL,'Accept',1,NULL,'Medium',NULL,NULL,12),('Necessity',17,'2016-06-26 15:13:04','Maecenas varius facilisi.','Other',NULL,'Accept',1,NULL,'Medium',NULL,NULL,16),('Necessity',18,'2016-06-26 15:13:04','Quis quisque montes.','Other',NULL,'Accept',1,NULL,'Medium',NULL,NULL,20),('Necessity',19,'2016-06-26 15:13:04','Fames vestibulum velit.','Other',NULL,'Accept',1,NULL,'Medium',NULL,NULL,21),('Necessity',20,'2016-06-26 15:13:04','Aenean molestie nec.','Other',NULL,'Accept',1,NULL,'Medium',NULL,NULL,23),('Necessity',21,'2016-06-26 15:13:04','Hendrerit tempor fermentum.','Other',NULL,'Accept',1,NULL,'Medium',NULL,NULL,30),('Necessity',22,'2016-06-26 15:13:04','Porta pretium mauris.','Other',NULL,'Accept',1,NULL,'Medium',NULL,NULL,18),('Necessity',23,'2016-06-26 15:13:04','Sociosqu ad mus.','Other',NULL,'Accept',1,NULL,'Medium',NULL,NULL,30),('Necessity',24,'2016-06-26 15:13:04','Mi sem massa.','Other',NULL,'Accept',1,NULL,'Medium',NULL,NULL,27),('Necessity',25,'2016-06-26 15:13:04','Sed ornare senectus.','Other',NULL,'Accept',1,NULL,'Medium',NULL,NULL,17),('Necessity',26,'2016-06-26 15:13:04','Integer sit venenatis.','Other',NULL,'Accept',1,NULL,'Medium',NULL,NULL,29),('DonationGood',27,'2016-06-26 15:14:58','10 litros de leche larga vida','Other',NULL,'Accept',1,'images/donationGood/donationGood.png','Medium',NULL,NULL,24),('DonationGood',28,'2016-06-26 15:14:58','Parturient nascetur suscipit.','Other',NULL,'Accept',1,'images/donationGood/donationGood.png','Medium',NULL,NULL,26),('DonationGood',29,'2016-06-26 15:14:58','Fames eu malesuada.','Other',NULL,'Accept',1,'images/donationGood/donationGood.png','Medium',NULL,NULL,28),('DonationGood',30,'2016-06-26 15:14:58','Morbi pede per.','Other',NULL,'Accept',1,'images/donationGood/donationGood.png','Medium',NULL,NULL,22),('DonationGood',31,'2016-06-26 15:14:58','Ut nisi cum.','Other',NULL,'Accept',1,'images/donationGood/donationGood.png','Medium',NULL,NULL,26),('DonationGood',32,'2016-06-26 15:14:58','Hymenaeos lacinia ac.','Other',NULL,'Accept',1,'images/donationGood/donationGood.png','Medium',NULL,NULL,30),('DonationGood',33,'2016-06-26 15:14:58','Vestibulum euismod mi.','Other',NULL,'Accept',1,'images/donationGood/donationGood.png','Medium',NULL,NULL,23),('DonationGood',34,'2016-06-26 15:14:58','1 bicicleta playera recien realizado service','Other',NULL,'Accept',1,'images/donationGood/donationGood.png','Medium',NULL,NULL,24),('DonationGood',35,'2016-06-26 15:14:58','Netus amet lorem.','Other',NULL,'Accept',1,'images/donationGood/donationGood.png','Medium',NULL,NULL,28),('DonationGood',36,'2016-06-26 15:14:58','Mauris per suscipit.','Other',NULL,'Accept',1,'images/donationGood/donationGood.png','Medium',NULL,NULL,28),('DonationGood',37,'2016-06-26 15:14:58','Odio dapibus ut.','Other',NULL,'Accept',1,'images/donationGood/donationGood.png','Medium',NULL,NULL,23),('DonationGood',38,'2016-06-26 15:14:58','Vehicula litora lectus.','Other',NULL,'Accept',1,'images/donationGood/donationGood.png','Medium',NULL,NULL,29),('DonationGood',39,'2016-06-26 15:14:58','1 comedor completo muy buen estado','Other',NULL,'Accept',1,'images/donationGood/donationGood.png','Medium',NULL,NULL,24),('DonationGood',40,'2016-06-26 15:14:58','5 kilos de ázucar','Other',NULL,'Accept',1,'images/donationGood/donationGood.png','Medium',NULL,NULL,24),('DonationGood',41,'2016-06-26 15:14:58','Vel arcu laoreet.','Other',NULL,'Accept',1,'images/donationGood/donationGood.png','Medium',NULL,NULL,27),('DonationGood',42,'2016-06-26 15:14:58','Scelerisque in molestie.','Other',NULL,'Accept',1,'images/donationGood/donationGood.png','Medium',NULL,NULL,30),('DonationGood',43,'2016-06-26 15:14:58','Vel congue phasellus.','Other',NULL,'Accept',1,'images/donationGood/donationGood.png','Medium',NULL,NULL,27),('DonationGood',44,'2016-06-26 15:14:58','Inceptos euismod etiam.','Other',NULL,'Accept',1,'images/donationGood/donationGood.png','Medium',NULL,NULL,30),('DonationGood',45,'2016-06-26 15:14:58','Mus eros parturient.','Other',NULL,'Accept',1,'images/donationGood/donationGood.png','Medium',NULL,NULL,27),('DonationGood',46,'2016-06-26 15:14:58','Integer eget velit.','Other',NULL,'Accept',1,'images/donationGood/donationGood.png','Medium',NULL,NULL,29),('Necessity',47,'2016-06-29 12:20:02','50 litros de leche','Other',NULL,'Accept',1,'images/donationGood/donationGood.png','Medium',NULL,NULL,2),('Necessity',48,'2016-06-29 12:20:02','1 mesa para 8 personas','Other',NULL,'Accept',1,'images/donationGood/donationGood.png','Medium',NULL,NULL,2),('Necessity',49,'2016-06-29 12:20:02','cocinera para sabado por la mañana','Other',NULL,'Accept',1,'images/donationGood/donationGood.png','Medium',NULL,NULL,2),('Necessity',50,'2016-06-29 12:20:02','10 tazas','Other',NULL,'Accept',1,'images/donationGood/donationGood.png','Medium',NULL,NULL,2),('DonationGood',51,'2016-06-30 09:53:00','Taciti elementum nisl.','Other',NULL,'Accept',1,NULL,'Medium',NULL,NULL,2),('DonationGood',52,'2016-06-30 09:53:00','Pede lacus cubilia.','Other',NULL,'Accept',1,NULL,'Medium',NULL,NULL,2),('DonationGood',53,'2016-06-30 09:53:00','Tellus vitae etiam.','Other',NULL,'Accept',1,NULL,'Medium',NULL,NULL,2),('DonationGood',54,'2016-06-30 09:53:00','Ligula tempor ante.','Other',NULL,'Accept',1,NULL,'Medium',NULL,NULL,2);
/*!40000 ALTER TABLE `publication` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `publication_history`
--

DROP TABLE IF EXISTS `publication_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publication_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` int(11) DEFAULT NULL,
  `array_status` tinyblob,
  `admin_name` varchar(60) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `publication_history`
--

LOCK TABLES `publication_history` WRITE;
/*!40000 ALTER TABLE `publication_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `publication_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `puntuation`
--

DROP TABLE IF EXISTS `puntuation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `puntuation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `comment` varchar(250) NOT NULL,
  `date` datetime DEFAULT NULL,
  `score_type` varchar(255) NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `puntuation`
--

LOCK TABLES `puntuation` WRITE;
/*!40000 ALTER TABLE `puntuation` DISABLE KEYS */;
INSERT INTO `puntuation` VALUES (1,'excelente usuario muchas gracias!','2016-04-21 20:55:12','Excellent',0),(2,'muy buen usuario!','2016-04-29 10:02:10','Good',NULL);
/*!40000 ALTER TABLE `puntuation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resource`
--

DROP TABLE IF EXISTS `resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resource` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_resource` int(11) DEFAULT NULL,
  `resource_type` varchar(255) NOT NULL,
  `version` int(11) DEFAULT NULL,
  `participant_owner` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resource`
--

LOCK TABLES `resource` WRITE;
/*!40000 ALTER TABLE `resource` DISABLE KEYS */;
INSERT INTO `resource` VALUES (1,1,'Profile',1,6);
/*!40000 ALTER TABLE `resource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resource_denouncements`
--

DROP TABLE IF EXISTS `resource_denouncements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resource_denouncements` (
  `resource` bigint(20) NOT NULL,
  `denouncements` bigint(20) NOT NULL,
  PRIMARY KEY (`resource`,`denouncements`),
  UNIQUE KEY `UK_qt9dvf6ya4riuncs83letlmo` (`denouncements`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resource_denouncements`
--

LOCK TABLES `resource_denouncements` WRITE;
/*!40000 ALTER TABLE `resource_denouncements` DISABLE KEYS */;
INSERT INTO `resource_denouncements` VALUES (1,1);
/*!40000 ALTER TABLE `resource_denouncements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_type` varchar(255) NOT NULL,
  `role_description` varchar(200) NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'ROLE_ADMIN','administrador del sitio',1),(2,'ROLE_PERSON','participante persona del sitio',1),(3,'ROLE_INSTITUTION','participante institucion del sitio',1);
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suscription`
--

DROP TABLE IF EXISTS `suscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `suscription` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `amount` float NOT NULL,
  `date` datetime DEFAULT NULL,
  `state` bit(1) NOT NULL,
  `type` varchar(255) NOT NULL,
  `version` int(11) DEFAULT NULL,
  `receiver` bigint(20) DEFAULT NULL,
  `suscriptor` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suscription`
--

LOCK TABLES `suscription` WRITE;
/*!40000 ALTER TABLE `suscription` DISABLE KEYS */;
/*!40000 ALTER TABLE `suscription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `activation_date` datetime DEFAULT NULL,
  `activation_key` varchar(255) DEFAULT NULL,
  `email_address` varchar(255) NOT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `locked` bit(1) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `version` int(11) DEFAULT NULL,
  `rol` bigint(20) DEFAULT NULL,
  `config_notification` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_d0ar1h7wcp7ldy6qg5859sol6` (`email_address`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'2016-01-17 10:44:38','activationKey:1555819280','darioegb@gmail.com','','\0','96cae35ce8a9b0244178bf28e4966c2ce1b8385723a96a6b838858cdd6ca0a1e',9,2,2),(2,'2016-01-19 22:22:35','activationKey:1555833289','comedor_buenos_dias@yopmail.com','','\0','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3',1,3,1),(3,'2016-03-11 08:46:58','activationKey:1555823290','despertar@gmail.com','','\0','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3',3,3,3),(4,'2016-02-05 22:10:00','activationKey:1555883291','asilounnuevohorizonte@yahoo.com.ar','','\0','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3',1,3,4),(5,'2016-02-05 23:42:00','activationKey:1555873292','juanperez@gmail.com','','\0','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3',1,2,5),(6,'2016-02-08 12:42:00','activationKey:1555863293','analiagomez@yopmail.com','','\0','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3',2,2,6),(7,'2015-11-17 11:40:30','activationKey:1555819279','adminesperanza@gmail.com','','\0','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3',1,1,7),(8,'2016-06-13 21:33:01','activationKey:1555825158','federicorodriguez232@hotmail.com','','\0','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3',1,2,8),(9,'2016-06-16 21:35:19','activationKey:1555819234','mariajuarez@gmail.com','','\0','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3',1,2,9),(10,'2016-06-17 21:36:25','activationKey:15558195654','salaprimerosauxilios@gmail.com','','\0','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3',1,3,10),(11,'2016-06-19 21:37:47','activationKey:1555819259','comedorcomunitariolavino@yahoo.com.ar','','\0','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3',1,3,11),(12,'2016-06-17 22:07:03','activationKey:1555819232','ayudacatastrofes@gov.com.ar','','\0','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3',1,3,12),(13,'2016-06-26 12:30:19','activationKey:5303973658','institucion@gmail.com','','\0','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3',1,3,13),(14,'2016-06-26 12:30:19','activationKey:3120353635','institucion2@gmail.com','','\0','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3',1,3,14),(15,'2016-06-26 12:30:19','activationKey:2880262073','institucion3@gmail.com','','\0','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3',1,3,15),(16,'2016-06-26 12:30:19','activationKey:5961007813','institucion4@gmail.com','','\0','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3',1,3,16),(17,'2016-06-26 12:30:19','activationKey:7240261588','institucion5@gmail.com','','\0','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3',1,3,17),(18,'2016-06-26 12:30:19','activationKey:4193400396','institucion6@gmail.com','','\0','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3',1,3,18),(19,'2016-06-26 12:30:19','activationKey:2985879862','institucion7@gmail.com','','\0','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3',1,3,19),(20,'2016-06-26 12:30:19','activationKey:2518543607','institucion8@gmail.com','','\0','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3',1,3,20),(21,'2016-06-26 12:30:19','activationKey:5854218939','institucion9@gmail.com','','\0','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3',1,3,21),(22,'2016-06-26 12:30:19','activationKey:8728168652','persona@gmail.com','','\0','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3',1,2,22),(23,'2016-06-26 12:30:19','activationKey:3513789793','persona2@gmail.com','','\0','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3',1,2,23),(24,'2016-06-26 12:30:19','activationKey:4875539500','persona3@gmail.com','','\0','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3',1,2,24),(25,'2016-06-26 12:30:19','activationKey:7590226422','persona4@gmail.com','','\0','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3',1,2,25),(26,'2016-06-26 12:30:19','activationKey:4721439174','persona5@gmail.com','','\0','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3',1,2,26),(27,'2016-06-26 12:30:19','activationKey:5144606161','persona6@gmail.com','','\0','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3',1,2,27),(28,'2016-06-26 12:30:19','activationKey:8050251990','persona7@gmail.com','','\0','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3',1,2,28),(29,'2016-06-26 12:30:19','activationKey:3975642147','persona8@gmail.com','','\0','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3',1,2,29),(30,'2016-06-26 12:30:19','activationKey:7017267776','persona9@gmail.com','','\0','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3',1,2,30),(31,'2016-06-29 12:46:47','activationKey:4574941477','persona10@gmail.com','','\0','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3',3,2,31);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` int(11) DEFAULT NULL,
  `role_entry` bigint(20) NOT NULL,
  `user_entry` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-06-30 10:35:05
